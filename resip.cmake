
#project(resip)

include_directories(resiprocate-1.10.2)
include_directories(resiprocate-1.10.2/rutil)
include_directories(resiprocate-1.10.2/rutil/dns/ares)
include_directories(resiprocate-1.10.2/resip)

add_definitions(-DUSE_ARES)

aux_source_directory(resiprocate-1.10.2/rutil RUTIL_SOURCE)
if (NOT WIN32)
    list(REMOVE_ITEM RUTIL_SOURCE resiprocate-1.10.2/rutil/WinCompat.cxx)
endif()
aux_source_directory(resiprocate-1.10.2/rutil/dns RUTIL_SOURCE)
aux_source_directory(resiprocate-1.10.2/rutil/stun RUTIL_SOURCE)

aux_source_directory(resiprocate-1.10.2/rutil/dns/ares ARES_SOURCE)
list(REMOVE_ITEM ARES_SOURCE resiprocate-1.10.2/rutil/dns/ares/adig.c)

aux_source_directory(resiprocate-1.10.2/resip/stack STACK_SOURCE)
aux_source_directory(resiprocate-1.10.2/resip/stack/gen STACK_SOURCE)

aux_source_directory(resiprocate-1.10.2/resip/dum DUM_SOURCE)

option(NOCRT "link crt static" ON)

if (WIN32)
    if (NOCRT)
		set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /DWIN32 /MTd")
		set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /DWIN32 /MT")
		set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} /DWIN32 /MTd")
		set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} /DWIN32 /MT")
	endif()
	
	add_library(rutil STATIC ${RUTIL_SOURCE})
	add_library(ares STATIC ${ARES_SOURCE})
	add_library(resiprocate STATIC ${STACK_SOURCE})
	add_library(dum STATIC ${DUM_SOURCE})
	
	set_target_properties(rutil PROPERTIES DEBUG_POSTFIX "d")
	set_target_properties(ares PROPERTIES DEBUG_POSTFIX "d")
	set_target_properties(resiprocate PROPERTIES DEBUG_POSTFIX "d")
	set_target_properties(dum PROPERTIES DEBUG_POSTFIX "d")
else()
	add_library(rutil SHARED ${RUTIL_SOURCE})
	add_library(ares SHARED ${ARES_SOURCE})
	add_library(resiprocate SHARED ${STACK_SOURCE})
	add_library(dum SHARED ${DUM_SOURCE})
	
	install(TARGETS rutil ares resiprocate dum 
		RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)
endif()
