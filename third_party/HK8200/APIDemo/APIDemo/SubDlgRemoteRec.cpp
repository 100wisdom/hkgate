// SubDlgRemoteRec.cpp : implementation file
//

#include "stdafx.h"
#include "APIDemo.h"
#include "SubDlgRemoteRec.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubDlgRemoteRec dialog


CSubDlgRemoteRec::CSubDlgRemoteRec(CWnd* pParent /*=NULL*/)
	: CDialog(CSubDlgRemoteRec::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSubDlgRemoteRec)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSubDlgRemoteRec::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubDlgRemoteRec)
	DDX_Control(pDX, IDC_STATIC_RPWND, m_wndRP);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_COMBO2, m_combox);
	DDX_Control(pDX, IDC_COMBO4, m_combox_s);
}


BEGIN_MESSAGE_MAP(CSubDlgRemoteRec, CDialog)
	//{{AFX_MSG_MAP(CSubDlgRemoteRec)
	ON_BN_CLICKED(IDC_BUTTON_RECFIND, OnButtonRecfind)
	ON_BN_CLICKED(IDC_BUTTON_SRPLAYBYT, OnButtonSrplaybyt)
	ON_BN_CLICKED(IDC_BUTTON_SRPBYF, OnButtonSrpbyf)
	ON_BN_CLICKED(IDC_BUTTON_STOPSRP, OnButtonStopsrp)
	ON_BN_CLICKED(IDC_BUTTON_STARTDLBYT, OnButtonStartdlbyt)
	ON_BN_CLICKED(IDC_BUTTON_STARTDLBYF, OnButtonStartdlbyf)
	ON_BN_CLICKED(IDC_BUTTON_STOPDL, OnButtonStopdl)
	ON_BN_CLICKED(IDC_BUTTON_GETDLPOS, OnButtonGetdlpos)
	ON_BN_CLICKED(IDC_BUTTON_RPPAUSE, OnButtonRppause)
	ON_BN_CLICKED(IDC_BUTTON_RPSTOP, OnButtonRpstop)
	ON_BN_CLICKED(IDC_BUTTON_RPFRAMEF, OnButtonRpframef)
	ON_BN_CLICKED(IDC_BUTTON_RPFRAMEB, OnButtonRpframeb)
	ON_BN_CLICKED(IDC_BUTTON_FORWARDP, OnButtonForwardp)
	ON_BN_CLICKED(IDC_BUTTON_BACKP, OnButtonBackp)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CSubDlgRemoteRec::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_SPOS, &CSubDlgRemoteRec::OnBnClickedButtonSpos)
	ON_BN_CLICKED(IDC_BUTTON_GPOS, &CSubDlgRemoteRec::OnBnClickedButtonGpos)
	ON_BN_CLICKED(IDC_BUTTON_PICCAPTURE2, &CSubDlgRemoteRec::OnBnClickedButtonPiccapture2)
	ON_BN_CLICKED(IDC_BUTTON_FORWARDP3, &CSubDlgRemoteRec::OnBnClickedButtonForwardp3)
	ON_BN_CLICKED(IDC_BUTTON_FORWARDP5, &CSubDlgRemoteRec::OnBnClickedButtonForwardp5)
	ON_BN_CLICKED(IDC_BUTTON_FORWARDP6, &CSubDlgRemoteRec::OnBnClickedButtonForwardp6)
	ON_BN_CLICKED(IDC_BUTTON_FORWARDP7, &CSubDlgRemoteRec::OnBnClickedButtonForwardp7)
	ON_BN_CLICKED(IDC_BUTTON_BACKP3, &CSubDlgRemoteRec::OnBnClickedButtonBackp3)
	ON_BN_CLICKED(IDC_BUTTON_BACKP5, &CSubDlgRemoteRec::OnBnClickedButtonBackp5)
	ON_BN_CLICKED(IDC_BUTTON_BACKP6, &CSubDlgRemoteRec::OnBnClickedButtonBackp6)
	ON_BN_CLICKED(IDC_BUTTON_BACKP7, &CSubDlgRemoteRec::OnBnClickedButtonBackp7)
	ON_BN_CLICKED(IDC_BUTTON_RPPAUSE2, &CSubDlgRemoteRec::OnBnClickedButtonRppause2)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubDlgRemoteRec message handlers

BOOL CSubDlgRemoteRec::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here

	GetDlgItem(IDC_EDIT_STARTTMP)->SetWindowText("YYYYMMDDTHHNNSSZ");
	GetDlgItem(IDC_EDIT_ENDTMP)->SetWindowText("YYYYMMDDTHHNNSSZ");

	GetDlgItem(IDC_EDIT_STARTTMT)->SetWindowText("YYYYMMDDTHHNNSSZ");
	GetDlgItem(IDC_EDIT_ENDTMT)->SetWindowText("YYYYMMDDTHHNNSSZ");
	GetDlgItem(IDC_EDIT_RECORDT)->SetWindowText("C:\\test_rec");
	GetDlgItem(IDC_EDIT_RECORDF)->SetWindowText("C:\\test_rec");
	GetDlgItem(IDC_EDIT_CAMIDP)->SetWindowText("");
	GetDlgItem(IDC_EDIT_STREAMPOS)->SetWindowText("0");
	GetDlgItem(IDC_EDIT2)->SetWindowText("8");

	m_combox.InsertString(0, "0:jpg");
	m_combox.InsertString(1, "1:bmp");

	m_combox.SetCurSel(0);


	m_combox_s.InsertString(0, "1表示从平台查");
	m_combox_s.InsertString(1, "2表示从设备查");
	m_combox_s.InsertString(2, "3表示从本地查");
	
	m_combox_s.SetCurSel(0);


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSubDlgRemoteRec::OnButtonRecfind()
{
	CString csCamID, csStartTM, csEndTM;

	GetDlgItemText(IDC_EDIT_CAMIDP, csCamID);
	GetDlgItemText(IDC_EDIT_STARTTMP, csStartTM);
	GetDlgItemText(IDC_EDIT_ENDTMP, csEndTM);

	long lRet = -1;

	long StorageDev = -1;
	GetDlgItemInt(IDC_EDIT3);

	int index = m_combox_s.GetCurSel();
	if ( index == 0 )
	{
		StorageDev = 1;
	}
	else if ( index == 1 )
	{
		StorageDev = 2;
	}
	else if ( index == 2 )
	{
		StorageDev = 3;
	}

	lRet = Std_RecordFind(g_pMainDlg->m_lLoginID,
						  csCamID.GetBuffer(),
						  csStartTM.GetBuffer(),
						  csEndTM.GetBuffer(),
						  StorageDev, RecordFindCallBack, NULL);

	g_pMainDlg->PrintCallMsg("Std_RecordFind()", lRet);

}


void CSubDlgRemoteRec::OnButtonSrplaybyt()
{
	CString csCamID, csStartTM, csEndTM;

	GetDlgItemText(IDC_EDIT_CAMIDP, csCamID);
	GetDlgItemText(IDC_EDIT_STARTTMP, csStartTM);
	GetDlgItemText(IDC_EDIT_ENDTMP, csEndTM);


	long lRet = -1;
	long StorageDev = GetDlgItemInt(IDC_EDIT3);


	CButton *m_handle = (CButton *)GetDlgItem(IDC_CHECK1);
	if ( m_handle->GetCheck() )
	{
		lRet = Std_StreamReplayByTime(g_pMainDlg->m_lLoginID,
									  csCamID.GetBuffer(),
									  csStartTM.GetBuffer(),
									  csEndTM.GetBuffer(),
									  StorageDev, m_wndRP,
									  StreamCallBack, NULL);
	}
	else
	{
		lRet = Std_StreamReplayByTime(g_pMainDlg->m_lLoginID,
									  csCamID.GetBuffer(),
									  csStartTM.GetBuffer(),
									  csEndTM.GetBuffer(),
									  StorageDev, NULL,
									  StreamCallBack, NULL);
	}

	g_pMainDlg->PrintCallMsg("Std_StreamReplayByTime()", lRet);

	if ( lRet >= 0 )
	{
		g_pMainDlg->m_lRePlayHandleR = lRet;
	}
}


void CSubDlgRemoteRec::OnButtonSrpbyf()
{
	CString csFileUrl;
	GetDlgItemText(IDC_EDIT_STREAMURL, csFileUrl);

	long lRet = -1;

	string url = AnsiToUtf8(csFileUrl.GetBuffer());
	CButton *m_handle = (CButton *)GetDlgItem(IDC_CHECK1);
	if ( m_handle->GetCheck() )
	{
		lRet = Std_StreamReplayByFile(g_pMainDlg->m_lLoginID, url.c_str(), m_wndRP, NULL, NULL);
	}
	else
	{
		lRet = Std_StreamReplayByFile(g_pMainDlg->m_lLoginID, url.c_str(), NULL, NULL, NULL);
	}

	g_pMainDlg->PrintCallMsg("Std_StreamReplayByFile()", lRet);

	if ( lRet >= 0 )
	{
		g_pMainDlg->m_lRePlayHandleR = lRet;
	}
}

void CSubDlgRemoteRec::OnButtonStopsrp()
{
	//
	long lRet = -1;
	lRet = Std_StopStreamReplay(g_pMainDlg->m_lRePlayHandleR);

	g_pMainDlg->PrintCallMsg("Std_StopStreamReplay()", lRet);

	if ( lRet >= 0 )
	{
		g_pMainDlg->m_lRePlayHandleR = -1;
		m_wndRP.Invalidate();
	}
}

void CSubDlgRemoteRec::OnButtonStartdlbyt()
{
	CString csCamID, csRecPath, csStartTM, csEndTM;

	char czFileExt[64] = { 0 };
	long lFileExtLen = 64;

	GetDlgItemText(IDC_EDIT_CAMIDT, csCamID);
	GetDlgItemText(IDC_EDIT_RECORDT, csRecPath);
	GetDlgItemText(IDC_EDIT_STARTTMT, csStartTM);
	GetDlgItemText(IDC_EDIT_ENDTMT, csEndTM);

	//
	long lRet = -1;
	//long StorageDev = 1;
	//long DownloadSpeed = 8;
	long StorageDev = GetDlgItemInt(IDC_EDIT1);
	long DownloadSpeed = GetDlgItemInt(IDC_EDIT2);

	string cspath = AnsiToUtf8(csRecPath.GetBuffer());

	lRet = Std_StartDownloadByTime(g_pMainDlg->m_lLoginID,
								   csCamID.GetBuffer(),
								   csStartTM.GetBuffer(),
								   csEndTM.GetBuffer(),
								   StorageDev, &DownloadSpeed,
								   cspath.c_str(),
								   czFileExt,
								   lFileExtLen);

	g_pMainDlg->PrintCallMsg("Std_StartDownloadByTime()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		string tmp;
		tmp = Utf8toAnsi(czFileExt);

		csMsg.Format("录像路径:%s%s", csRecPath.GetBuffer(), tmp.c_str());

		g_pMainDlg->m_lDownloadHandle = lRet;

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgRemoteRec::OnButtonStartdlbyf()
{
	CString csUrl, csFilePath;

	char czFileExt[64] = { 0 };
	long lFileExtLen = 64;

	GetDlgItemText(IDC_EDIT_RECORDURL, csUrl);
	GetDlgItemText(IDC_EDIT_RECORDF, csFilePath);

	long lRet = -1;

	string url = AnsiToUtf8(csUrl.GetBuffer());
	string cspath = AnsiToUtf8(csFilePath.GetBuffer());

	//char szBuf2[1204] = { 0 };
	//strcpy_s(szBuf2, AnsiToUtf8(csFilePath.GetBuffer()));

	long DownloadSpeed = GetDlgItemInt(IDC_EDIT2);

	lRet = Std_StartDownloadByFile(g_pMainDlg->m_lLoginID,
								   &DownloadSpeed,
								   url.c_str(),
								   cspath.c_str(),
								   czFileExt,
								   lFileExtLen);

	g_pMainDlg->PrintCallMsg("Std_StartDownloadByFile()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		string tmp;
		tmp = Utf8toAnsi(czFileExt);

		g_pMainDlg->m_lDownloadHandle = lRet;

		csMsg.Format("录像路径:%s%s", csFilePath.GetBuffer(), tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgRemoteRec::OnButtonStopdl()
{
	long lRet = -1;

	lRet = Std_StopDownload(g_pMainDlg->m_lDownloadHandle);

	g_pMainDlg->PrintCallMsg("Std_StopDownload()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		g_pMainDlg->m_lDownloadHandle = -1;

		csMsg.Format("总共下载了%d个录像文件", lRet);

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgRemoteRec::OnButtonGetdlpos()
{
	long lRet = -1;

	lRet = Std_GetDownloadPos(g_pMainDlg->m_lDownloadHandle);

	g_pMainDlg->PrintCallMsg("Std_GetDownloadPos()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		csMsg.Format("目前下载进度:%d", lRet);

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgRemoteRec::OnButtonRppause()
{
	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;


	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_PAUSE);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|Pause", lRet);

	return;
	/*
		CString csTxt;

		((CButton*)GetDlgItem(IDC_BUTTON_RPPAUSE))->GetWindowText(csTxt);

		if ( csTxt.Compare("暂停") >= 0 )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_RPPAUSE))->SetWindowText("恢复");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_PAUSE);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|Pause", lRet);
		}
		else
		{
		((CButton*)GetDlgItem(IDC_BUTTON_RPPAUSE))->SetWindowText("暂停");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_RESUME);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|Resume", lRet);
		}*/
}

void CSubDlgRemoteRec::OnButtonRpstop()
{
	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_STOP);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|Stop", lRet);
}

void CSubDlgRemoteRec::OnButtonRpframef()
{
	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_ONEBYONE);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|OneFrameByone", lRet);
}

void CSubDlgRemoteRec::OnButtonRpframeb()
{
	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_ONEBYONEBACK);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|OneFrameBack", lRet);
}

void CSubDlgRemoteRec::OnButtonForwardp()
{
	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;


	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_2_FORWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|2倍速播放", lRet);

	return;
	/*
		CString csTxt;

		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->GetWindowText(csTxt);

		if ( csTxt == "2倍进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("4倍进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_2_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|2倍速播放", lRet);
		}
		else if ( csTxt == "4倍进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("8倍进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_4_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|4倍速播放", lRet);
		}
		else if ( csTxt == "8倍进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("16倍进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_8_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|8倍速播放", lRet);
		}
		else if ( csTxt == "16倍进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("正常进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_16_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|16倍速播放", lRet);
		}
		else if ( csTxt == "正常进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("1/2进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_1_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|正常倍速播放", lRet);
		}
		else if ( csTxt == "1/2进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("1/4进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_HALF_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|1/2倍速播放", lRet);
		}
		else if ( csTxt == "1/4进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("1/8进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_QUARTER_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|1/4倍速播放", lRet);
		}
		else if ( csTxt == "1/8进" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_FORWARDP))->SetWindowText("2倍进");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_Eighth_FORWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|1/8倍速播放", lRet);
		}*/
}

void CSubDlgRemoteRec::OnButtonBackp()
{
	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_2_BACKWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|2倍速回退", lRet);

	return;
	/*
		CString csTxt;

		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->GetWindowText(csTxt);

		if ( csTxt == "2倍退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("4倍退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_2_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|2倍速回退", lRet);
		}
		else if ( csTxt == "4倍退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("8倍退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_4_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|4倍速回退", lRet);
		}
		else if ( csTxt == "8倍退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("16倍退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_8_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|8倍速回退", lRet);
		}
		else if ( csTxt == "16倍退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("正常退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_16_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|16倍速回退", lRet);
		}
		else if ( csTxt == "正常退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("1/2退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_1_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|正常倍速回退", lRet);
		}
		else if ( csTxt == "1/2退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("1/4退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_HALF_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|1/2倍速回退", lRet);
		}
		else if ( csTxt == "1/4退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("1/8退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_QUARTER_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|1/4倍速回退", lRet);
		}
		else if ( csTxt == "1/8退" )
		{
		((CButton*)GetDlgItem(IDC_BUTTON_BACKP))->SetWindowText("2倍退");

		lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_Eighth_BACKWARD);

		g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|1/8倍速回退", lRet);
		}*/
}


void CSubDlgRemoteRec::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码

	long lRet = -1;
	long Playtime = GetDlgItemInt(IDC_EDIT_STREAMPOS);

	lRet = Std_SetStreamReplayPos(g_pMainDlg->m_lRePlayHandleR, Playtime);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayByFile()", lRet);

}


void CSubDlgRemoteRec::OnBnClickedButtonSpos()
{
	// TODO:  在此添加控件通知处理程序代码

	long lRet = -1;
	long Playtime = GetDlgItemInt(IDC_EDIT_STREAMPOS);

	lRet = Std_SetStreamReplayPos(g_pMainDlg->m_lRePlayHandleR, Playtime);

	g_pMainDlg->PrintCallMsg("Std_SetStreamReplayPos()", lRet);
}


void CSubDlgRemoteRec::OnBnClickedButtonGpos()
{
	// TODO:  在此添加控件通知处理程序代码

	long lRet = -1;
	long Playtime = -1;

	lRet = Std_GetFileReplayPos(g_pMainDlg->m_lRePlayHandleR, &Playtime);

	SetDlgItemInt(IDC_EDIT_STREAMPOS, Playtime);

	g_pMainDlg->PrintCallMsg("Std_GetFileReplayPos()", lRet);
}


void CSubDlgRemoteRec::OnBnClickedButtonPiccapture2()
{
	// TODO:  在此添加控件通知处理程序代码
	CString csPic;
	CString csMsg;
	int nPicFormat;

	GetDlgItemText(IDC_EDIT_PICPATH3, csPic);
	//nPicFormat = GetDlgItemInt(IDC_EDIT_PICFORMAT2);

	int index = m_combox.GetCurSel();

	long lRet = -1;

	string	szBuf = AnsiToUtf8(csPic.GetBuffer());

	lRet = Std_Capture(g_pMainDlg->m_lRePlayHandleR, szBuf.c_str(), index);

	g_pMainDlg->m_lRecHandle[0] = lRet;

	g_pMainDlg->PrintCallMsg("Std_Capture()", lRet);

}


void CSubDlgRemoteRec::OnBnClickedButtonForwardp3()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;


	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_4_FORWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|4倍速播放", lRet);

}


void CSubDlgRemoteRec::OnBnClickedButtonForwardp5()
{
	// TODO:  在此添加控件通知处理程序代码
	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_8_FORWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|8倍速播放", lRet);

}


void CSubDlgRemoteRec::OnBnClickedButtonForwardp6()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_16_FORWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|16倍速播放", lRet);

}


void CSubDlgRemoteRec::OnBnClickedButtonForwardp7()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_1_FORWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|正常倍速播放", lRet);

}


void CSubDlgRemoteRec::OnBnClickedButtonBackp3()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_4_BACKWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|4倍速回退", lRet);
}


void CSubDlgRemoteRec::OnBnClickedButtonBackp5()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_8_BACKWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|8倍速回退", lRet);
}


void CSubDlgRemoteRec::OnBnClickedButtonBackp6()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_16_BACKWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|16倍速回退", lRet);
}


void CSubDlgRemoteRec::OnBnClickedButtonBackp7()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;

	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_1_BACKWARD);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|正常倍速回退", lRet);
}


void CSubDlgRemoteRec::OnBnClickedButtonRppause2()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( g_pMainDlg->m_lRePlayHandleR < 0 )
	{
		g_pMainDlg->ShowMsg("call Std_StreamReplayControl(),未开始远程播放");

		return;
	}

	long lRet = -1;


	lRet = Std_StreamReplayControl(g_pMainDlg->m_lRePlayHandleR, PLAYMODE_RESUME);

	g_pMainDlg->PrintCallMsg("Std_StreamReplayControl()|Resume", lRet);

	return;
}
