// SubDlgTalk.cpp : 实现文件
//

#include "stdafx.h"
#include "APIDemo.h"
#include "SubDlgTalk.h"


// SubDlgTalk 对话框

IMPLEMENT_DYNAMIC(SubDlgTalk, CDialog)

SubDlgTalk::SubDlgTalk(CWnd* pParent /*=NULL*/)
	: CDialog(SubDlgTalk::IDD, pParent)
{
    m_TalkHandle = 0;
}

SubDlgTalk::~SubDlgTalk()
{
}

void SubDlgTalk::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(SubDlgTalk, CDialog)
    ON_BN_CLICKED(IDC_BUTTON_STARTTALK, &SubDlgTalk::OnBnClickedButtonStarttalk)
    ON_BN_CLICKED(IDC_BUTTON_STOPTALK, &SubDlgTalk::OnBnClickedButtonStoptalk)
END_MESSAGE_MAP()


// SubDlgTalk 消息处理程序

void SubDlgTalk::OnBnClickedButtonStarttalk()
{
    CString csID;

    GetDlgItemText(IDC_EDIT_CAMID_TALK, csID);

    long lRet = -1;

    lRet = Std_StartTalk(g_pMainDlg->m_lLoginID, csID.GetBuffer(), TalkCallBack, NULL);
    m_TalkHandle = lRet;

    g_pMainDlg->PrintCallMsg("Std_StartTalk()", lRet);
}

void SubDlgTalk::OnBnClickedButtonStoptalk()
{
    long lRet = -1;

    lRet = Std_StopTalk(m_TalkHandle);

    g_pMainDlg->PrintCallMsg("Std_StopTalk()", lRet);
}
