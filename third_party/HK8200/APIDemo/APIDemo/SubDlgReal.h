#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "MyButton.h"

class CSubDlgReal : public CDialog
{
	// Construction
public:
	CSubDlgReal(CWnd* pParent = NULL);   // standard constructor

	int		m_nWndSel;
	enum
	{
		IDD = IDD_DLG_SUB_REAL
	};

	CStatic	m_wnd3;
	CStatic	m_wnd2;
	CStatic	m_wnd1;
	CStatic	m_wnd0;

	CStatic *sta[4];

 	BOOL b_3d_set;
	BOOL b_goto_preset;
	BOOL b_set_preset;
	BOOL b_del_preset;
	BOOL b_cle_preset;
 
	static const char *PtzCmd[];

	long PtzCtrl(BOOL  btype, int PtzCmdIndex);

	long lRet;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonStartreal();
	afx_msg void OnButtonStopreal();
	afx_msg void OnStaticWnd0();
	afx_msg void OnStaticWnd1();
	afx_msg void OnStaticWnd2();
	afx_msg void OnStaticWnd3();
	afx_msg void OnButtonStartlrec();
	afx_msg void OnButtonStoplrec();
	afx_msg void OnButtonPiccapture();
	afx_msg void OnButtonPresetset();
	afx_msg void OnButtonPresetgoto();
	afx_msg void OnButtonPresetcle();
	afx_msg void OnButtonPresetdel();
	DECLARE_MESSAGE_MAP()
public:
	CMyButton b_left_up;
	CMyButton b_up;
	CMyButton b_right_up;
	CMyButton b_left;
	CMyButton b_right;
	CMyButton b_left_down;
	CMyButton b_down;
	CMyButton b_right_down;
	CMyButton b_zoomin;
	CMyButton b_zoomout;

	BOOL m_startRect;   //绘制矩形框标志 
	CPoint m_startPoint; //矩形框开始点 
	CPoint m_OldPoint;   //矩形框终点（但是它是上一次的点，所以这里用了Old标识）

	int bCounter;

	void CheckLastError();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonPreset3d();
	CComboBox m_combox;
};
