#pragma once
#include "afxwin.h"

class CSubDlgRemoteRec : public CDialog
{
public:
	CSubDlgRemoteRec(CWnd* pParent = NULL);   // standard constructor

	enum { IDD = IDD_DLG_SUB_REMOTEREC };
	CStatic	m_wndRP;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:

	virtual BOOL OnInitDialog();
	afx_msg void OnButtonRecfind();
	afx_msg void OnButtonSrplaybyt();
	afx_msg void OnButtonSrpbyf();
	afx_msg void OnButtonStopsrp();
	afx_msg void OnButtonStartdlbyt();
	afx_msg void OnButtonStartdlbyf();
	afx_msg void OnButtonStopdl();
	afx_msg void OnButtonGetdlpos();
	afx_msg void OnButtonRppause();
	afx_msg void OnButtonRpstop();
	afx_msg void OnButtonRpframef();
	afx_msg void OnButtonRpframeb();
	afx_msg void OnButtonForwardp();
	afx_msg void OnButtonBackp();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButtonSpos();
	afx_msg void OnBnClickedButtonGpos();
	afx_msg void OnBnClickedButtonPiccapture2();
	afx_msg void OnBnClickedButtonForwardp3();
	afx_msg void OnBnClickedButtonForwardp5();
	afx_msg void OnBnClickedButtonForwardp6();
	afx_msg void OnBnClickedButtonForwardp7();
	afx_msg void OnBnClickedButtonBackp3();
	afx_msg void OnBnClickedButtonBackp5();
	afx_msg void OnBnClickedButtonBackp6();
	afx_msg void OnBnClickedButtonBackp7();
	afx_msg void OnBnClickedButtonRppause2();
	CComboBox m_combox;
	CComboBox m_combox_s;
};
