// APIDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "APIDemo.h"
#include "APIDemoDlg.h"
#include "MyShowMsg.h"


char startpath[MAX_PATH] = { 0 };

double YUV2RGB_CONVERT_MATRIX[3][3] =
{
	{ 1, 0, 1.4022 },
	{ 1, -0.3456, -0.7145 },
	{ 1, 1.771, 0 }
};


static void ConvertYUV2RGB(byte* yuvFrame, byte* rgbFrame, int width, int height);
static void WriteBMP(byte* rgbFrame, int width, int height, const char* bmpFile);


//语音对讲回调
void STDCALL TalkCallBack(int iDataType, void* pData, int iDataSize, void* pUser)
{
    char *DecoderTag = NULL;
    CString path = startpath;

    CStdioFile file;
    file.Open(path + "Talk." +
        ((DecoderTag != NULL) ? DecoderTag : "hikvision"),
        CFile::modeCreate |
        CFile::modeNoTruncate |
        CFile::modeReadWrite |
        CFile::shareDenyWrite |
        CFile::typeText);

    file.SeekToEnd();
    //file.Write(msg.GetBuffer(), msg.GetLength());
    //if ( DataLen > 0 )
    {
        file.Write(pData, iDataSize);
    }

    file.Close();
   

}

//实时播放回调函数定义
void STDCALL StreamCallBack(long RealHandle, int StreamType,
							const char *Data, int DataLen,
							const char *DecoderTag, void *UserData)
{
	char *n = "1";

	CString msg;
	msg.Format("%d", DataLen);
	char *a = new char[10];

	_itoa_s(DataLen, a, 10, 10);

	PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, (WPARAM)n, (LPARAM)a);

	CallType ctype = (CallType)UnDef;
	//CallType ctype = (CallType)RealPlay;
	if ( UserData != NULL )
	{
		ctype = ((PUSERDATA)UserData)->ctype;
	}

	CString path = startpath;


	switch ( ctype )
	{
	case	RealPlay:
	{
		OutputDebugString("5\n");
		CStdioFile file;
		file.Open(path + "RealPlay." +
				  ((DecoderTag != NULL) ? DecoderTag : "hikvision"),
				  CFile::modeCreate |
				  CFile::modeNoTruncate |
				  CFile::modeReadWrite |
				  CFile::shareDenyWrite |
				  CFile::typeBinary);

		file.SeekToEnd();
		OutputDebugString("6\n");
		//file.Write(msg.GetBuffer(), msg.GetLength());
		//if ( DataLen > 0 )
		{
			file.Write(Data, DataLen);
		}

		OutputDebugString("7\n");
		file.Close();
		OutputDebugString("8\n");
	}

	break;
	case	LocalPlay:
	{
		CStdioFile file;
		file.Open(path + "LocalPlay." +
				  ((DecoderTag != NULL) ? DecoderTag : "hikvision"),
				  CFile::modeCreate |
				  CFile::modeNoTruncate |
				  CFile::modeReadWrite |
				  CFile::shareDenyWrite |
				  CFile::typeText);

		file.SeekToEnd();
		//file.Write(msg.GetBuffer(), msg.GetLength());
		//if ( DataLen > 0 )
		{
			file.Write(Data, DataLen);
		}

		file.Close();
	}

	break;
	case	RemotePlay:
	{
		CStdioFile file;
		file.Open(path + "RemotePlay." +
				  ((DecoderTag != NULL) ? DecoderTag : "hikvision"),
				  CFile::modeCreate |
				  CFile::modeNoTruncate |
				  CFile::modeReadWrite |
				  CFile::shareDenyWrite |
				  CFile::typeText);

		file.SeekToEnd();
		//file.Write(msg.GetBuffer(), msg.GetLength());
		//if ( DataLen > 0 )
		{
			file.Write(Data, DataLen);
		}

		file.Close();
	}
	break;

	default:
	break;
	}

	//g_pMainDlg->ShowMsg(msg);

}


//录像文件检索回调函数定义
void STDCALL RecordFindCallBack(long FindHandle, const char *CameraId,
								int *iContinue, int iFinish,
								const char *FileListXML, void *UserData)
{
	string msg;

	try
	{
		msg = Utf8toAnsi(FileListXML);

		if ( msg.length() <= 0 )
		{
			return;
		}

		char *a = new char[msg.length() + 1];

		strcpy_s(a, msg.length() + 1, msg.c_str());

		PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, NULL, (LPARAM)a);

		g_pMainDlg->ShowMsg(msg.c_str());

		*iContinue = 1;
	}
	catch ( ... )
	{
		g_pMainDlg->ShowMsg("Catch error");
	}
}

//获取资源列表回调函数定义
void STDCALL OrgInfoCallBack(long getResHandle, int *iContinue,
							 int iFinish, const char *FileListXML, void *UserData)
{
	CString msg;
	//if ( iFinish == 1 )
	{
		string szbuff;

		szbuff = Utf8toAnsi(FileListXML);

		g_pMainDlg->ShowMsg(szbuff.c_str());
	}

	*iContinue = 1;

}


int filter(unsigned int code, struct _EXCEPTION_POINTERS *ep)
{

	//puts("in filter.");
	CString msg;
	msg.Format("in filter: code=%d", code);

	g_pMainDlg->ShowMsg(msg.GetBuffer());

	if ( code == EXCEPTION_ACCESS_VIOLATION )
	{

		puts("caught AV as expected.");

		return EXCEPTION_EXECUTE_HANDLER;

	}

	else
	{

		puts("didn't catch AV, unexpected.");

		return EXCEPTION_CONTINUE_SEARCH;

	};

}


//YUV数据回调回调函数定义
void STDCALL VideoDataStreamCallBack(long RealHandle, LPPICTURE_DATA_S
									 pPictureData, void *pUserData)
{
	static int cnt = 0;
	char* dtmp = "2";
	CString msg;
	if ( pPictureData == NULL )
	{
		g_pMainDlg->ShowMsg("pPictureData is null");
		return;
	}

	msg.Format("图片高度:%d 图片宽度:%d",
			   pPictureData->ulPicHeight,
			   pPictureData->ulPicWidth);


	char *a = new char[msg.GetLength() + 1];

	strcpy_s(a, msg.GetLength() + 1, msg.GetBuffer());

	::PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, (WPARAM)dtmp, (LPARAM)a);


	if ( !g_pMainDlg->IsSaveBmp )
	{
		return;
	}
	else
	{
		g_pMainDlg->IsSaveBmp = FALSE;
		msg.Format("w:%d h:%d LineSize[0]:%d LineSize[1]:%d LineSize[2]:%d",
				   pPictureData->ulPicWidth,
				   pPictureData->ulPicHeight,
				   pPictureData->ulLineSize[0],
				   pPictureData->ulLineSize[1],
				   pPictureData->ulLineSize[2]);

		g_pMainDlg->ShowMsg(msg);

	}

	int width = pPictureData->ulPicWidth;
	int height = pPictureData->ulPicHeight;
	int n = width * height;

	//if ( n != pPictureData->ulLineSize[0] )
	//{
	//	g_pMainDlg->ShowMsg("width * height != pPictureData->ulLineSize[0]");
	//	return;
	//}

	int framesize = n + n / 2;

	byte *yuv = new byte[framesize];
	byte *rgb = new byte[3 * n];

	CFile YUVFile;

	char path[MAX_PATH] = { 0 };

	GetModuleFileName(NULL, path, MAX_PATH);

	CString tmp = path;

	int pos = tmp.ReverseFind('\\') + 1;
	tmp = tmp.Left(pos);
	CString strPathName;
	strPathName.Format("%s%s.bmp", tmp.GetBuffer(),
					   CTime::GetTickCount().Format("%Y%m%d_%H%M%S"));

	try
	{
		memcpy(yuv, pPictureData->pucData[0], n);
		g_pMainDlg->ShowMsg("pucData[0]!");

		memcpy(yuv + n, pPictureData->pucData[1], n / 4);
		g_pMainDlg->ShowMsg("pucData[1]!");

		memcpy(yuv + n + n / 4, pPictureData->pucData[2], n / 4);
		g_pMainDlg->ShowMsg("pucData[2]!");

		ConvertYUV2RGB(yuv, rgb, width, height);
		g_pMainDlg->ShowMsg("ConvertYUV2RGB");

		WriteBMP(rgb, width, height, strPathName.GetBuffer());
		g_pMainDlg->ShowMsg("WriteBMP");

		try
		{
			delete[] yuv;
			delete[] rgb;
		}
		catch ( ... )
		{
			g_pMainDlg->ShowMsg("delete yuv or rgb");
		}

	}
	//__except ( filter(GetExceptionCode(), GetExceptionInformation()) )
	//{
	//	g_pMainDlg->ShowMsg("memcpy or ConvertYUV2RGB or WriteBMP error!");
	//}
	catch ( ... )
	{
		g_pMainDlg->ShowMsg("memcpy or ConvertYUV2RGB or WriteBMP error!");
	}

	/*try
	{
		delete[] yuv;
		delete[] rgb;
	}
	catch ( ... )
	{
		g_pMainDlg->ShowMsg("delete yuv or rgb");
	}*/

}

//订阅报警消息回调
void STDCALL AlarmCallback(const char* csAlarmDetail, void *pUser)
{
	CString msg;

	try
	{
		string szBuf;
		szBuf = Utf8toAnsi(csAlarmDetail);
		msg.Format("%s", szBuf.c_str());

		if ( msg.GetLength() <= 0 )
		{
			return;
		}

		char *a = new char[msg.GetLength() + 1];

		strcpy_s(a, msg.GetLength() + 1, msg.GetBuffer());

		PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, NULL, (LPARAM)a);

		g_pMainDlg->ShowMsg(msg.GetBuffer());
	}
	catch ( ... )
	{
		g_pMainDlg->ShowMsg("Catch error");
	}
}


static void ConvertYUV2RGB(byte* yuvFrame, byte* rgbFrame, int width, int height)
{
	int uIndex = width * height;
	int vIndex = uIndex + ((width * height) >> 2);
	int gIndex = width * height;
	int bIndex = gIndex * 2;

	int temp = 0;

	for ( int y = 0; y < height; y++ )
	{
		for ( int x = 0; x < width; x++ )
		{
			// R分量
			temp = (int)(yuvFrame[y * width + x] + (yuvFrame[vIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[0][2]);
			rgbFrame[y * width + x] = (byte)(temp < 0 ? 0 : (temp > 255 ? 255 : temp));
			// G分量
			temp = (int)(yuvFrame[y * width + x] + (yuvFrame[uIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[1][1] +
						 (yuvFrame[vIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[1][2]);
			rgbFrame[gIndex + y * width + x] = (byte)(temp < 0 ? 0 : (temp > 255 ? 255 : temp));
			// B分量
			temp = (int)(yuvFrame[y * width + x] + (yuvFrame[uIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[2][1]);
			rgbFrame[bIndex + y * width + x] = (byte)(temp < 0 ? 0 : (temp > 255 ? 255 : temp));
			//Color c = Color.FromArgb(rgbFrame[y * width + x], rgbFrame[gIndex + y * width + x], rgbFrame[bIndex + y * width + x]);
			//bm.SetPixel(x, y, c);
		}
	}
	//return bm;
}


static void WriteBMP(byte* rgbFrame, int width, int height, const char* bmpFile)
{
	// 写 BMP 图像文件。

	int yu = width * 3 % 4;
	yu = yu != 0 ? 4 - yu : yu;

	int bytePerLine = width * 3 + yu;
	int rgb_size = width * 3 * height;

	CFile fs;
	fs.Open(bmpFile, CFile::modeCreate | CFile::typeBinary | CFile::modeWrite);

	BITMAPFILEHEADER bmpHeader;
	BITMAPINFOHEADER bmpInfo;

	memset(&bmpHeader, 0, sizeof(BITMAPFILEHEADER));
	memset(&bmpInfo, 0, sizeof(BITMAPINFOHEADER));
	bmpHeader.bfType = 0x4D42; //"MB"
	bmpHeader.bfSize = rgb_size + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	bmpHeader.bfReserved1 = 0;
	bmpHeader.bfReserved2 = 0;
	bmpHeader.bfOffBits = 54; //sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFODEADER);

	bmpInfo.biSize = sizeof(BITMAPINFOHEADER);
	bmpInfo.biWidth = width;
	bmpInfo.biHeight = height;
	bmpInfo.biPlanes = 1;
	bmpInfo.biBitCount = 24;
	bmpInfo.biCompression = BI_RGB;
	bmpInfo.biSizeImage = rgb_size;
	bmpInfo.biXPelsPerMeter = 0;
	bmpInfo.biYPelsPerMeter = 0;
	bmpInfo.biClrUsed = 0;
	bmpInfo.biClrImportant = 0;

	fs.Write(&bmpHeader, sizeof(BITMAPFILEHEADER));
	fs.Write(&bmpInfo, sizeof(BITMAPINFOHEADER));

	byte* data = new byte[bytePerLine * height];
	int gIndex = width * height;
	int bIndex = gIndex * 2;

	for ( int y = height - 1, j = 0; y >= 0; y--, j++ )
	{
		for ( int x = 0, i = 0; x < width; x++ )
		{
			data[y * bytePerLine + i++] = rgbFrame[bIndex + j * width + x]; // B
			data[y * bytePerLine + i++] = rgbFrame[gIndex + j * width + x]; // G
			data[y * bytePerLine + i++] = rgbFrame[j * width + x]; // R
		}
	}

	fs.Write(data, bytePerLine * height);
	fs.Flush();

	fs.Close();
}

//消息回调回调函数定义
void STDCALL MsgCallBack(int MsgType, const char *Data, long DataLen, void* UserData)
{
	CString msg;

	switch ( MsgType )
	{
	case 1:
	{
		g_pMainDlg->ShowMsg("消息类型:1解码异常");
		LPMSG_DECODE_EXCEPTION e = (LPMSG_DECODE_EXCEPTION)Data;

		string szBuf;
		szBuf = Utf8toAnsi(e->ErrorDesc);
		msg.Format("%d,%d,%s", e->PlayHandle, e->ErrorNo, szBuf.c_str());

	}
	break;

	case 2:
	{
		g_pMainDlg->ShowMsg("消息类型:2录像异常");
		LPMSG_RECORD_EXCEPTION e = (LPMSG_RECORD_EXCEPTION)Data;

		string szBuf;
		szBuf = Utf8toAnsi(e->ErrorDesc);
		msg.Format("%d,%d,%s", e->RecordHandle, e->ErrorNo, szBuf.c_str());
	}

	break;

	case 3:
	{
		g_pMainDlg->ShowMsg("消息类型:3下载进度");
		LPMSG_DOWNLOAD_Progress e = (LPMSG_DOWNLOAD_Progress)Data;

		msg.Format("%d,%d,%d", e->DownloadHandle, e->Progress, e->Status);

	}
	break;

	case 4:
	{

		LPMSG_REPLAY_Progress e = (LPMSG_REPLAY_Progress)Data;

		msg.Format("%d,%d,%d", e->PlayHandle, e->PlayTimed, e->Status);

		g_pMainDlg->ShowMsg("消息类型:4播放进度" + msg);
	}
	break;

	case 5:
	{
		g_pMainDlg->ShowMsg("消息类型:5用户离线");
		LPMSG_USER_OFFLINE e = (LPMSG_USER_OFFLINE)Data;

		string szBuf;
		szBuf = Utf8toAnsi(e->UserName);
		msg.Format("%d,%s", e->LoginHandle, szBuf.c_str());
	}

	break;

	default:

	break;
	}

	if ( !msg.IsEmpty() )
	{
		char *n = "3";

		char *a = new char[msg.GetLength() + 1];

		strcpy_s(a, msg.GetLength() + 1, msg.GetBuffer());

		//PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, (WPARAM)n, (LPARAM)a);

		g_pMainDlg->ShowMsg(msg.GetBuffer());
	}

}


//摄像机状态推送回调函数定义
void STDCALL StatusCallBack(const char *CameraId, int Status,
							void *UserData)
{
	//CString msg;
	//msg.Format("%d", Status);

	//g_pMainDlg->ShowMsg(msg);
}


//画图叠加回调函数定义
void STDCALL DrawCallBack(long LoginHandle, long PlayHandle,
						  long hDC, void *UserData)
{
	//HDC hdc = GetDC(NULL);
	HDC hdc = (HDC)hDC;
	if ( hdc == NULL )
	{
		g_pMainDlg->ShowMsg("绘图设备句柄无效");
	}

	SetTextColor(hdc, RGB(255, 0, 0));
	SetBkColor(hdc, RGB(0, 0, 255));
	//SetBkMode(hdc, TRANSPARENT);
	CString str = "画图叠加回调函数测试!";
	TextOut(hdc, 100, 100, str, str.GetLength());

	//g_pMainDlg->ShowMsg((char*)UserData);

}


/////////////////////////////////////////////////////////////////////////////
// CAPIDemoDlg dialog


CAPIDemoDlg::CAPIDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAPIDemoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAPIDemoDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_lLoginID = -1;
	m_lPlayHandle[0] = -1;
	m_lPlayHandle[1] = -1;
	m_lPlayHandle[2] = -1;
	m_lPlayHandle[3] = -1;
	m_lRePlayHandleR = -1;
	m_lDownloadHandle = -1;
	m_lRePlayHandleL = -1;
}

void CAPIDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAPIDemoDlg)
	DDX_Control(pDX, IDC_TAB_OPERATE, m_tabOperate);
	DDX_Control(pDX, IDC_EDIT_MSG, m_ctrlMsg);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_LIST1, m_listbox);
}

BEGIN_MESSAGE_MAP(CAPIDemoDlg, CDialog)
	//{{AFX_MSG_MAP(CAPIDemoDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_OPERATE, OnSelchangeTabOperate)
	//}}AFX_MSG_MAP
	ON_LBN_DBLCLK(IDC_LIST1, &CAPIDemoDlg::OnLbnDblclkList1)
	ON_MESSAGE(CALLBACKMSG, &CAPIDemoDlg::OnCallbackmsg)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////
//
void CAPIDemoDlg::CreateTab()
{
	char path[MAX_PATH] = { 0 };

	GetModuleFileName(NULL, path, MAX_PATH);

	CString tmp = path;

	int pos = tmp.ReverseFind('\\') + 1;
	tmp = tmp.Left(pos);

	memcpy(startpath, tmp.GetBuffer(), tmp.GetLength());

	m_tabOperate.InsertItem(0, "初始化及登录");
	m_tabOperate.InsertItem(1, "平台资源");
	m_tabOperate.InsertItem(2, "辅助接口");
	m_tabOperate.InsertItem(3, "实况及云台");
	m_tabOperate.InsertItem(4, "远程录像");
	m_tabOperate.InsertItem(5, "本地录像");
	m_tabOperate.InsertItem(6, "信息回调");
    m_tabOperate.InsertItem(7, "语音对讲");

	m_dlgInit.Create(IDD_DLG_SUB_INIT, &m_tabOperate);
	m_dlgRes.Create(IDD_DLG_SUB_RES, &m_tabOperate);
	m_dlgAux.Create(IDD_DLG_SUB_AUX, &m_tabOperate);
	m_dlgReal.Create(IDD_DLG_SUB_REAL, &m_tabOperate);
	m_dlgRemoteRec.Create(IDD_DLG_SUB_REMOTEREC, &m_tabOperate);
	m_dlgLocalRec.Create(IDD_DLG_SUB_LOCALREC, &m_tabOperate);
	m_dlgCallback.Create(IDD_DLG_SUB_CALLBACK, &m_tabOperate);
    m_dlgTalk.Create(IDD_DLG_SUB_TALK, &m_tabOperate);

	CRect tabRect;
	m_tabOperate.GetClientRect(&tabRect);

	tabRect.top += 20;
	tabRect.bottom -= 5;
	tabRect.left += 5;
	tabRect.right -= 5;

	m_dlgInit.MoveWindow(tabRect);
	m_dlgRes.MoveWindow(tabRect);
	m_dlgAux.MoveWindow(tabRect);
	m_dlgReal.MoveWindow(tabRect);
	m_dlgRemoteRec.MoveWindow(tabRect);
	m_dlgLocalRec.MoveWindow(tabRect);
	m_dlgCallback.MoveWindow(tabRect);
    m_dlgTalk.MoveWindow(tabRect);

	m_dlgInit.ShowWindow(SW_SHOW);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);

	m_tabOperate.SetCurSel(0);

}

void CAPIDemoDlg::ShowMsg(CString csMsg)
{
	//CString csTM = "", csInfo;

	//SYSTEMTIME st;

	//GetLocalTime(&st);

	int cnt = m_listbox.GetCount();
	if ( cnt >= 100 )
	{
		m_listbox.ResetContent();
	}

	m_listbox.InsertString(cnt, csMsg.GetBuffer());
	m_listbox.SetCurSel(cnt);

	//csInfo = csTM + "\r\n" + csMsg + "\r\n";

	//nOldLen = m_ctrlMsg.GetWindowTextLength();

	//nNewLen = csInfo.GetLength() + nOldLen;

	//m_ctrlMsg.SetSel(nOldLen, nNewLen);

	//m_ctrlMsg.ReplaceSel(csInfo);

}



void CAPIDemoDlg::PrintCallMsg(char* csFunc, long lVal)
{
	CString msg;

	switch ( lVal )
	{
	case -1:	msg = "调用失败";							break;
	case -2:	msg = "密码不正确";						break;
	case -3:	msg = "用户名不存在";						break;
	case -4:	msg = "登录超时";							break;
	case -5:	msg = "帐号已被锁定";						break;
	case -6:	msg = "摄像机不存在";						break;
	case -7:	msg = "摄像机离线";						break;
	case -8:	msg = "解码库加载失败";					break;
	case -9:	msg = "点播超时";							break;
	case -10:	msg = "响应超时";							break;
	case -11:	msg = "没有找到任何录像文件";				break;
	case -12:	msg = "文件不存在";						break;
	case -13:	msg = "不支持的控制命令";					break;
	case -14:	msg = "文件打开失败(文件被占用或文件损坏)";	break;
	case -15:	msg = "(根据后缀名)找不到对应的解码器";		break;
	case -16:	msg = "读取文件失败";						break;
	case -17:	msg = "写入文件失败";						break;
	case -18:	msg = "没有找到作任何预置点信息";			break;
	case -19:	msg = "该摄像机不支持预置点查询";			break;
	case -20:	msg = "点播时未启动解码";					break;
	case -90:	msg = "测试错误信息，错误代号里未定义";		break;
	case -97:	msg = "接口不支持";						break;
	case -98:	msg = "输入参数非法";						break;
	case -99:	msg = "其它错误";							break;
	default:
	if ( lVal >= 0 )
	{
		msg = "调用成功";
	}
	break;
	}

	CString csMsg;
	csMsg.Format("%s : %s", csFunc, msg);

	ShowMsg(csMsg);
}


//void CAPIDemoDlg::PrintCallMsg(CString csFunc, long lVal)
//{
//	CString csMsg, csResult;
//
//	csMsg.Format("-\r\ncall %s:返回值%d,", csFunc, lVal);
//
//	if ( lVal >= 0 )
//	{
//		csResult = "调用成功";
//	}
//	else if ( -1 == lVal )
//	{
//		if ( csFunc.Find("Login") >= 0 )
//		{
//			csResult = "用户名不存在";
//		}
//		else
//		{
//			csResult = "调用失败";
//		}
//	}
//	else if ( -2 == lVal )
//	{
//		if ( csFunc.Find("Login") >= 0 )
//		{
//			csResult = "用户名不存在";
//		}
//		if ( csFunc.Find("StartRealPlay") >= 0 )
//		{
//			csResult = "摄像机不存在";
//		}
//		if ( csFunc.Find("RecordFind") >= 0 )
//		{
//			csResult = "响应超时";
//		}
//		if ( csFunc.Find("StreamReplayByTime") >= 0 )
//		{
//			csResult = "摄像机不存在";
//		}
//		if ( csFunc.Find("StreamReplayByFile") >= 0 )
//		{
//			csResult = "文件不存在";
//		}
//		if ( csFunc.Find("StartDownloadByTime") >= 0 )
//		{
//			csResult = "摄像机不存在";
//		}
//		if ( csFunc.Find("StartDownloadByFile") >= 0 )
//		{
//			csResult = "文件不存在";
//		}
//		if ( csFunc.Find("StartFileReplay") >= 0 )
//		{
//			csResult = "文件不存在";
//		}
//		if ( csFunc.Find("FileCut") >= 0 )
//		{
//			csResult = "读取文件失败";
//		}
//		if ( csFunc.Find("PresetQuery") >= 0 )
//		{
//			csResult = "摄像机不存在";
//		}
//		if ( csFunc.Find("SetVideoDataCBF") >= 0 )
//		{
//			csResult = "点播时未启动解码";
//		}
//		if ( csFunc.Find("SetDrawCBF") >= 0 )
//		{
//			csResult = "点播时未启动解码";
//		}
//	}
//	else if ( -3 == lVal )
//	{
//		if ( csFunc.Find("Login") >= 0 )
//		{
//			csResult = "登录超时";
//		}
//		if ( csFunc.Find("StartRealPlay") >= 0 )
//		{
//			csResult = "摄像机离线";
//		}
//		if ( csFunc.Find("RecordFind") >= 0 )
//		{
//			csResult = "没有找到任何录像文件";
//		}
//		if ( csFunc.Find("StreamReplayByTime") >= 0 )
//		{
//			csResult = "摄像机离线";
//		}
//		if ( csFunc.Find("StartDownloadByTime") >= 0 )
//		{
//			csResult = "摄像机离线";
//		}
//		if ( csFunc.Find("FileCut") >= 0 )
//		{
//			csResult = "写入文件失败";
//		}
//		if ( csFunc.Find("PresetQuery") >= 0 )
//		{
//			csResult = "没有找到作任何预置点信息";
//		}
//	}
//	else if ( -4 == lVal )
//	{
//		if ( csFunc.Find("Login") >= 0 )
//		{
//			csResult = "帐号已被锁定";
//		}
//		if ( csFunc.Find("StartRealPlay") >= 0 )
//		{
//			csResult = "解码库加载失败";
//		}
//		if ( csFunc.Find("StreamReplayByTime") >= 0 )
//		{
//			csResult = "响应超时";
//		}
//		if ( csFunc.Find("StreamReplayByFile") >= 0 )
//		{
//			csResult = "响应超时";
//		}
//		if ( csFunc.Find("StartFileReplay") >= 0 )
//		{
//			csResult = "文件打开失败";
//		}
//	}
//	else if ( -5 == lVal )
//	{
//		if ( csFunc.Find("StartRealPlay") >= 0 )
//		{
//			csResult = "点播超时";
//		}
//		if ( csFunc.Find("StartFileReplay") >= 0 )
//		{
//			csResult = "找不到对应的解码器";
//		}
//	}
//	if ( (lVal >= -96) && (lVal <= -6) )
//	{
//		csResult = "未定义的返回值";
//	}
//	else if ( -97 == lVal )
//	{
//		if ( csFunc.Find("PtzCtrl") >= 0 )
//		{
//			csResult = "接口不支持";
//		}
//		if ( csFunc.Find("PtzCtrl3D") >= 0 )
//		{
//			csResult = "接口不支持";
//		}
//		if ( csFunc.Find("StreamReplayControl") >= 0 )
//		{
//			csResult = "不支持的控制命令";
//		}
//		if ( csFunc.Find("FileReplayControl") >= 0 )
//		{
//			csResult = "不支持的控制命令";
//		}
//		if ( csFunc.Find("PresetQuery") >= 0 )
//		{
//			csResult = "该摄像机不支持预置点查询";
//		}
//		if ( csFunc.Find("SetStatusCBF") >= 0 )
//		{
//			csResult = "接口不支持";
//		}
//		if ( csFunc.Find("SetDrawCBF") >= 0 )
//		{
//			csResult = "接口不支持";
//		}
//	}
//	else if ( -98 == lVal )
//	{
//		csResult = "输入参数非法";
//	}
//
//	else if ( -99 == lVal )
//	{
//		int nErr;
//		char czErr[512] = { 0 };
//
//		Std_GetLastError(&nErr, czErr);
//
//		char  error[1024] = { 0 };
//		strcpy_s(czErr, Utf8toAnsi(error));
//
//
//		csResult.Format("其它错误,errno:%d,%s", nErr, error);
//	}
//
//	csMsg += csResult;
//
//	ShowMsg(csMsg);
//}

/////////////////////////////////////////////////////////////////////////////
// CAPIDemoDlg message handlers

BOOL CAPIDemoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();


	//CMenu* pSysMenu = GetSystemMenu(FALSE);
	//if ( pSysMenu != NULL )
	//{
	//	CString strAboutMenu;
	//	strAboutMenu.LoadString(IDS_ABOUTBOX);
	//	if ( !strAboutMenu.IsEmpty() )
	//	{
	//		pSysMenu->AppendMenu(MF_SEPARATOR);
	//		pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
	//	}

	//}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	IsSaveBmp = FALSE;
	// TODO: Add extra initialization here
	g_pMainDlg = this;

	CreateTab();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAPIDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAPIDemoDlg::OnPaint()
{
	if ( IsIconic() )
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM)dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAPIDemoDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}

//
void CAPIDemoDlg::OnSelchangeTabOperate(NMHDR* pNMHDR, LRESULT* pResult)
{
	int nTab = m_tabOperate.GetCurSel();

	switch ( nTab )
	{
	case 0:
	m_dlgInit.ShowWindow(SW_SHOW);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
	case 1:
	m_dlgInit.ShowWindow(SW_HIDE);
	m_dlgRes.ShowWindow(SW_SHOW);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
	case 2:
	m_dlgInit.ShowWindow(SW_HIDE);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_SHOW);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
	case 3:
	m_dlgInit.ShowWindow(SW_HIDE);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_SHOW);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
	case 4:
	m_dlgInit.ShowWindow(SW_HIDE);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_SHOW);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
	case 5:
	m_dlgInit.ShowWindow(SW_HIDE);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_SHOW);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
	case 6:
	m_dlgInit.ShowWindow(SW_HIDE);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_SHOW);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
    case 7:
    m_dlgInit.ShowWindow(SW_HIDE);
    m_dlgRes.ShowWindow(SW_HIDE);
    m_dlgAux.ShowWindow(SW_HIDE);
    m_dlgReal.ShowWindow(SW_HIDE);
    m_dlgRemoteRec.ShowWindow(SW_HIDE);
    m_dlgLocalRec.ShowWindow(SW_HIDE);
    m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_SHOW);
    break;
	default:
	m_dlgInit.ShowWindow(SW_SHOW);
	m_dlgRes.ShowWindow(SW_HIDE);
	m_dlgAux.ShowWindow(SW_HIDE);
	m_dlgReal.ShowWindow(SW_HIDE);
	m_dlgRemoteRec.ShowWindow(SW_HIDE);
	m_dlgLocalRec.ShowWindow(SW_HIDE);
	m_dlgCallback.ShowWindow(SW_HIDE);
    m_dlgTalk.ShowWindow(SW_HIDE);
	break;
	}//switch

	*pResult = 0;
}



string AnsiToUtf8(LPCSTR Ansi)
{
	int WLength = MultiByteToWideChar(CP_ACP, 0, Ansi, -1, NULL, 0);
	LPWSTR pszW = (LPWSTR)_alloca((WLength + 1) * sizeof(WCHAR));
	MultiByteToWideChar(CP_ACP, 0, Ansi, -1, pszW, WLength);

	int ALength = WideCharToMultiByte(CP_UTF8, 0, pszW, -1, NULL, 0, NULL, NULL);
	LPSTR pszA = (LPSTR)_alloca(ALength + 1);
	WideCharToMultiByte(CP_UTF8, 0, pszW, -1, pszA, ALength, NULL, NULL);
	pszA[ALength] = '\0';

	string retStr(pszA);

	return retStr;
}

string WcharToUtf8(LPCWSTR szUnicode)
{
	int ALength = WideCharToMultiByte(CP_UTF8, 0, szUnicode, -1, NULL, 0, NULL, NULL);
	LPSTR pszA = (LPSTR)_alloca(ALength + 1);
	WideCharToMultiByte(CP_UTF8, 0, szUnicode, -1, pszA, ALength, NULL, NULL);
	pszA[ALength] = '\0';

	string retStr(pszA);
	return retStr;
}

string Utf8toAnsi(LPCSTR utf8)
{
	int WLength = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, NULL);
	LPWSTR pszW = (LPWSTR)_alloca((WLength + 1) * sizeof(WCHAR));
	MultiByteToWideChar(CP_UTF8, 0, utf8, -1, pszW, WLength);
	pszW[WLength] = '\0';

	int ALength = WideCharToMultiByte(CP_ACP, 0, pszW, -1, NULL, 0, NULL, NULL);
	LPSTR pszA = (LPSTR)_alloca(ALength + 1);
	WideCharToMultiByte(CP_ACP, 0, pszW, -1, pszA, ALength, NULL, NULL);
	pszA[ALength] = '\0';

	string retStr = pszA;

	return retStr;

	//return pszA;
}

LPCWSTR Utf8toWchar(LPCSTR utf8)
{
	int WLength = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, NULL);
	LPWSTR pszW = (LPWSTR)_alloca((WLength + 1) *sizeof(WCHAR));
	MultiByteToWideChar(CP_UTF8, 0, utf8, -1, pszW, WLength);
	pszW[WLength] = '\0';
	return pszW;
}




void CAPIDemoDlg::OnLbnDblclkList1()
{
	// TODO:  在此添加控件通知处理程序代码

	int index = m_listbox.GetCurSel();
	if ( index == -1 )
	{
		return;
	}

	CString msg;
	m_listbox.GetText(index, msg);

	CMyShowMsg myshow(msg.GetBuffer());
	myshow.DoModal();

}


afx_msg LRESULT CAPIDemoDlg::OnCallbackmsg(WPARAM wParam, LPARAM lParam)
{
	CString msg;

	char *n = (char*)wParam;
	char *a = (char*)lParam;

	msg.Format("%s\n", a);

	//char *b = NULL;
	//a = (char*)b;

	if ( a != NULL )
	{
		delete[]a;
	}
	int switch_on = 0;
	if ( n != NULL )
	{
		switch_on = atoi(n);
	}


	switch ( switch_on )
	{
	case 1:
	::SetDlgItemText(g_pMainDlg->GetSafeHwnd(), IDC_STATIC_MSG, "实时");
	break;

	case 2:
	::SetDlgItemText(g_pMainDlg->GetSafeHwnd(), IDC_STATIC_MSG, "VideoDataStreamCallBack");
	break;

	case 3:
	::SetDlgItemText(g_pMainDlg->GetSafeHwnd(), IDC_STATIC_MSG, "MsgCallBack");
	break;

	case 4:
	::SetDlgItemText(g_pMainDlg->GetSafeHwnd(), IDC_STATIC_MSG, "StatusCallBack");
	break;

	case 5:
	::SetDlgItemText(g_pMainDlg->GetSafeHwnd(), IDC_STATIC_MSG, "DrawCallBack");
	break;

	case 6:
	::SetDlgItemText(g_pMainDlg->GetSafeHwnd(), IDC_STATIC_MSG, "DownloadCallBack");
	break;

	default:
	break;
	}

	//AfxMessageBox(msg);

	::SetDlgItemText(g_pMainDlg->GetSafeHwnd(), IDC_STATIC_MSG, msg.GetBuffer());

	return 0;
}

void CAPIDemoDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_lLoginID != -1)
	{
		Std_Logout(g_pMainDlg->m_lLoginID);
		m_lLoginID = -1;
	}

	Std_UnInitialize();

	CDialog::OnClose();
}
