// SubDlgInit.cpp : implementation file
//

#include "stdafx.h"
#include "APIDemo.h"
#include "SubDlgInit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubDlgInit dialog


CSubDlgInit::CSubDlgInit(CWnd* pParent /*=NULL*/)
: CDialog(CSubDlgInit::IDD, pParent)
, isInitialize(FALSE)
{
	//{{AFX_DATA_INIT(CSubDlgInit)
	//}}AFX_DATA_INIT
}


void CSubDlgInit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubDlgInit)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSubDlgInit, CDialog)
	//{{AFX_MSG_MAP(CSubDlgInit)
	ON_BN_CLICKED(IDC_BUTTON_INIT, OnButtonInit)
	ON_BN_CLICKED(IDC_BUTTON_UNINIT, OnButtonUninit)
	ON_BN_CLICKED(IDC_BUTTON_LOGIN, OnButtonLogin)
	ON_BN_CLICKED(IDC_BUTTON_LOGOUT, OnButtonLogout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubDlgInit message handlers

BOOL CSubDlgInit::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetModuleFileName(NULL, strFilePath.GetBuffer(MAX_PATH), MAX_PATH);
	strFilePath.ReleaseBuffer();

	int nPos = strFilePath.ReverseFind(_T('\\')) + 1;
	strFilePath = strFilePath.Left(nPos) + "config.ini";

	CString ip;
	CString port;
	CString username;
	CString password;

	GetPrivateProfileString("main", "ip", "127.0.0.1",
							ip.GetBuffer(MAX_PATH), MAX_PATH, strFilePath);

	GetPrivateProfileString("main", "port", "8000",
							port.GetBuffer(MAX_PATH), MAX_PATH, strFilePath);

	GetPrivateProfileString("main", "username", "admin",
							username.GetBuffer(MAX_PATH), MAX_PATH, strFilePath);

	GetPrivateProfileString("main", "password", "",
							password.GetBuffer(MAX_PATH), MAX_PATH, strFilePath);

	GetDlgItem(IDC_EDIT_IPADDR)->SetWindowText(ip.GetBuffer());
	GetDlgItem(IDC_EDIT_PORT)->SetWindowText(port.GetBuffer());
	GetDlgItem(IDC_EDIT_USER)->SetWindowText(username.GetBuffer());
	GetDlgItem(IDC_EDIT_PWD)->SetWindowText(password.GetBuffer());

	//GetPrivateProfileSection

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSubDlgInit::OnButtonInit()
{
	long lRet = -1;

	lRet = Std_Initialize();
	if ( lRet >= 0 )
	{
		isInitialize = TRUE;
	}
	else
	{
		isInitialize = FALSE;
	}

	g_pMainDlg->PrintCallMsg("Std_Initialize()", lRet);

	//char *n = "6";
	//char *msg = "Std_SetDownloadDataCBF";
	//::PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, (WPARAM)n, (LPARAM)msg);
}

void CSubDlgInit::OnButtonUninit()
{
	long lRet = -1;

	lRet = Std_UnInitialize();
	if ( lRet >= 0 )
	{
		isInitialize = FALSE;
	}

	g_pMainDlg->PrintCallMsg("Std_UnInitialize()", lRet);
}

void CSubDlgInit::OnButtonLogin()
{
	CString csIP, csPort, csUser, csPwd;
	long lPort;

	GetDlgItemText(IDC_EDIT_IPADDR, csIP);
	GetDlgItemText(IDC_EDIT_PORT, csPort);
	GetDlgItemText(IDC_EDIT_USER, csUser);
	GetDlgItemText(IDC_EDIT_PWD, csPwd);
	lPort = GetDlgItemInt(IDC_EDIT_PORT);

	WritePrivateProfileString("main", "ip", csIP, strFilePath.GetBuffer());
	WritePrivateProfileString("main", "port", csPort, strFilePath.GetBuffer());
	WritePrivateProfileString("main", "username", csUser, strFilePath.GetBuffer());
	WritePrivateProfileString("main", "password", csPwd, strFilePath.GetBuffer());


	//
	long lRet = -1;

	if ( !isInitialize )
	{
		g_pMainDlg->ShowMsg("δ���г�ʼ��");
		return;
	}

	lRet = Std_Login(csIP, lPort, csUser, csPwd);

	g_pMainDlg->m_lLoginID = lRet;

	g_pMainDlg->PrintCallMsg("Std_Login()", lRet);
}

void CSubDlgInit::OnButtonLogout()
{
	long lRet = -1;

	lRet = Std_Logout(g_pMainDlg->m_lLoginID);

	if ( lRet >= 0 )
	{
		g_pMainDlg->m_lLoginID = -1;
	}

	g_pMainDlg->PrintCallMsg("Std_Logout()", lRet);
}
