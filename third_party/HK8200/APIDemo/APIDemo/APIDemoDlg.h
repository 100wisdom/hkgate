#pragma once


#include <string>
#include "stdafx.h"
#include "SubDlgAux.h"
#include "SubDlgCallback.h"
#include "SubDlgInit.h"
#include "SubDlgLocalRec.h"
#include "SubDlgReal.h"
#include "SubDlgRemoteRec.h"
#include "SubDlgRes.h"
#include "SubDlgTalk.h"

using namespace std;

extern char startpath[MAX_PATH];

string AnsiToUtf8(LPCSTR Ansi);
string Utf8toAnsi(LPCSTR utf8);


//实时播放回调函数定义
void STDCALL StreamCallBack(long RealHandle, int StreamType, const char *Data, int DataLen, const char *DecoderTag, void *UserData);
//录像文件检索回调函数定义
void STDCALL RecordFindCallBack(long FindHandle, const char *CameraId, int *iContinue, int iFinish, const char *FileListXML, void *UserData);
//获取资源列表回调函数定义
void STDCALL OrgInfoCallBack(long getResHandle, int *iContinue, int iFinish, const char *FileListXML, void *UserData);
//YUV数据回调回调函数定义
void STDCALL VideoDataStreamCallBack(long RealHandle, LPPICTURE_DATA_S pPictureData, void *pUserData);
//消息回调回调函数定义
void STDCALL MsgCallBack(int MsgType, const char *Data, long DataLen, void* UserData);
//摄像机状态推送回调函数定义
void STDCALL StatusCallBack(const char *CameraId, int Status, void *UserData);
//画图叠加回调函数定义
void STDCALL DrawCallBack(long LoginHandle, long PlayHandle, long hDC, void *UserData);
//报警回调函数定义
void STDCALL AlarmCallback(const char* csAlarmDetail, void *pUser);
int filter(unsigned int code, struct _EXCEPTION_POINTERS *ep);
//语音对讲回调
void STDCALL TalkCallBack(int iDataType, void* pData, int iDataSize, void* pUser);

class CAPIDemoDlg : public CDialog
{
	// Construction
public:
	CAPIDemoDlg(CWnd* pParent = NULL);	// standard constructor

	void	ShowMsg(CString csMsg);
	void    PrintCallMsg(char* csFunc, long lVal);
	void	CreateTab(void);

	LONG	m_lLoginID;
	LONG	m_lPlayHandle[4];			//实时预览实例号
	LONG	m_lRecHandle[4];			//实时视频录像实例号
	LONG	m_lRePlayHandleR;			//远程回放实例号			
	LONG    m_lDownloadHandle;			//远程下载实例号
	LONG	m_lRePlayHandleL;			//本地回放实例号

	CSubDlgAux	m_dlgAux;
	CSubDlgCallback m_dlgCallback;
	CSubDlgInit m_dlgInit;
	CSubDlgLocalRec m_dlgLocalRec;
	CSubDlgReal m_dlgReal;
	CSubDlgRemoteRec m_dlgRemoteRec;
	CSubDlgRes m_dlgRes;
    SubDlgTalk m_dlgTalk;

	enum
	{
		IDD = IDD_APIDEMO_DIALOG
	};

	CTabCtrl	m_tabOperate;
	CEdit	m_ctrlMsg;
	BOOL IsSaveBmp;
	CListBox m_listbox;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

protected:
	HICON m_hIcon;

	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeTabOperate(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLbnDblclkList1();
	afx_msg LRESULT OnCallbackmsg(WPARAM wParam, LPARAM lParam);
	afx_msg void OnClose();
};
