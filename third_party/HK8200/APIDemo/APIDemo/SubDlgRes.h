#pragma once

class CSubDlgRes : public CDialog
{
// Construction
public:
	CSubDlgRes(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSubDlgRes)
	enum { IDD = IDD_DLG_SUB_RES };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSubDlgRes)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSubDlgRes)
	afx_msg void OnButtonGetres();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonGetcaminfo();
	afx_msg void OnButtonPresetq();
	afx_msg void OnButtonGetcamstate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
