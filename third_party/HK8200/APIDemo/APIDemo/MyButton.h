#pragma once
#include "afxwin.h"

#ifdef DLLEXPORT
#define MY_MFC_MyBUTTON __declspec( dllexport )
#else
#define MY_MFC_MyBUTTON __declspec( dllimport )
#endif



class /*MY_MFC_MyBUTTON*/ CMyButton : public CButton
{
public:
	CMyButton();
	void Set(CComboBox *cb);

	virtual ~CMyButton();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnNcLButtonUp(UINT nHitTest, CPoint point);

public:
	CComboBox *m_cb;
	RECT rect;
};

