// SubDlgRes.cpp : implementation file
//

#include "stdafx.h"
#include "APIDemo.h"
#include "SubDlgRes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubDlgRes dialog


CSubDlgRes::CSubDlgRes(CWnd* pParent /*=NULL*/)
	: CDialog(CSubDlgRes::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSubDlgRes)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSubDlgRes::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubDlgRes)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSubDlgRes, CDialog)
	//{{AFX_MSG_MAP(CSubDlgRes)
	ON_BN_CLICKED(IDC_BUTTON_GETRES, OnButtonGetres)
	ON_BN_CLICKED(IDC_BUTTON_GETCAMINFO, OnButtonGetcaminfo)
	ON_BN_CLICKED(IDC_BUTTON_PRESETQ, OnButtonPresetq)
	ON_BN_CLICKED(IDC_BUTTON_GETCAMSTATE, OnButtonGetcamstate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubDlgRes message handlers

BOOL CSubDlgRes::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_EDIT_RESTYPE)->SetWindowText("0");
	GetDlgItem(IDC_EDIT_CAMIDS)->SetWindowText("<?xml version=\"1.0\"?>\r\n<list> \r\n<item id=\"\" />\r\n<item id=\"\" />\r\n<item id=\"\" />\r\n</list>");

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CSubDlgRes::OnButtonGetres()
{
	CString csOrgCode;
	int nRes;

	nRes = GetDlgItemInt(IDC_EDIT_RESTYPE);
	GetDlgItemText(IDC_EDIT_ORGCODE, csOrgCode);

	//
	long lRet = -1;

	//char szBuf[1204] = { 0 };
	//strcpy_s(szBuf, AnsiToUtf8(csOrgCode.GetBuffer()));

	lRet = Std_GetResList(g_pMainDlg->m_lLoginID,
						  nRes,
						  csOrgCode.GetBuffer(),
						  OrgInfoCallBack,this);

	g_pMainDlg->PrintCallMsg("Std_GetResList()", lRet);
}


void CSubDlgRes::OnButtonGetcaminfo()
{
	CString csCamID;
	char czCamInfo[1024 * 4] = { 0 };

	//string czCamInfo;

	GetDlgItemText(IDC_EDIT_CAMID, csCamID);

	//
	long lRet = -1;

	//char szBuf[1204] = { 0 };
	//strcpy_s(szBuf, AnsiToUtf8(csCamID.GetBuffer()));


	lRet = Std_GetCameraInfo(g_pMainDlg->m_lLoginID,
							 csCamID.GetBuffer(),
							 czCamInfo);

	g_pMainDlg->PrintCallMsg("Std_GetCameraInfo()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		string tmp;
		tmp = Utf8toAnsi(czCamInfo);


		csMsg.Format("摄像机信息:%s", tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgRes::OnButtonPresetq()
{
	CString csCamID;
	char czPreset[1024] = { 0 };

	GetDlgItemText(IDC_EDIT_CAMID, csCamID);

	//
	long lRet = -1;

	//char szBuf[1204] = { 0 };
	//strcpy_s(szBuf, AnsiToUtf8(csCamID.GetBuffer()));

	lRet = Std_PresetQuery(g_pMainDlg->m_lLoginID,
						   csCamID.GetBuffer(),
						   czPreset);

	g_pMainDlg->PrintCallMsg("Std_PresetQuery()", lRet);

	if ( lRet >= 0 )
	{
		string tmp;
		tmp = Utf8toAnsi(czPreset);

		CString csMsg;
		csMsg.Format("预置点信息:%s", tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgRes::OnButtonGetcamstate()
{
	CString csCamIDs;
	TCHAR czCamState[1024] = { 0 };

	GetDlgItemText(IDC_EDIT_CAMIDS, csCamIDs);

	csCamIDs.Replace("\r\n", "");
	csCamIDs.Replace("> ", ">");
	csCamIDs.Replace(" <", "<");

	long lRet = -1;


	//char szBuf[1204] = { 0 };
	//strcpy_s(szBuf, AnsiToUtf8(csCamIDs.GetBuffer()));

	lRet = Std_GetCameraStatus(g_pMainDlg->m_lLoginID,
							   csCamIDs.GetBuffer(),
							   czCamState);

	g_pMainDlg->PrintCallMsg("Std_GetCameraStatus()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		string tmp;
		tmp = Utf8toAnsi(czCamState);

		csMsg.Format("摄像机状态:%s", tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}
}
