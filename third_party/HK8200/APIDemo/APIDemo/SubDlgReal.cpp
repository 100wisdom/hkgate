// SubDlgReal.cpp : implementation file
//

#include "stdafx.h"
#include "APIDemo.h"
#include "SubDlgReal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubDlgReal dialog



const char * CSubDlgReal::PtzCmd[] = {
	"left",					//00
	"right",				//01
	"up",					//02
	"down",					//03
	"zoomin",				//04
	"zoomout",				//05
	"left_up",				//06
	"left_down",			//07
	"right_up",				//08
	"right_down",			//09
	"left_stop",			//10
	"right_stop",			//11
	"up_stop",				//12
	"down_stop",			//13
	"zoomin_stop",			//14
	"zoomout_stop",			//15
	"left_up_stop",			//16
	"left_down_stop",		//17
	"right_up_stop",		//18
	"right_down_stop",		//19
	"3d_set",				//20
	"goto_preset",			//21
	"set_preset",			//22
	"del_preset",			//23
	"cle_preset"			//24
};

CSubDlgReal::CSubDlgReal(CWnd* pParent /*=NULL*/)
: CDialog(CSubDlgReal::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSubDlgReal)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_nWndSel = 0;
}


void CSubDlgReal::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubDlgReal)
	DDX_Control(pDX, IDC_STATIC_WND3, m_wnd3);
	DDX_Control(pDX, IDC_STATIC_WND2, m_wnd2);
	DDX_Control(pDX, IDC_STATIC_WND1, m_wnd1);
	DDX_Control(pDX, IDC_STATIC_WND0, m_wnd0);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_BUTTON_PRESETU, b_up);
	DDX_Control(pDX, IDC_BUTTON_PRESETRU, b_right_up);
	DDX_Control(pDX, IDC_BUTTON_PRESETL, b_left);
	DDX_Control(pDX, IDC_BUTTON_PRESETLU, b_left_up);
	DDX_Control(pDX, IDC_BUTTON_PRESETR, b_right);
	DDX_Control(pDX, IDC_BUTTON_PRESETLD, b_left_down);
	DDX_Control(pDX, IDC_BUTTON_PRESETD, b_down);
	DDX_Control(pDX, IDC_BUTTON_RD, b_right_down);
	DDX_Control(pDX, IDC_BUTTON_ZOOMIN, b_zoomin);
	DDX_Control(pDX, IDC_BUTTON_ZOOMOUT, b_zoomout);
	DDX_Control(pDX, IDC_COMBO3, m_combox);
}


BEGIN_MESSAGE_MAP(CSubDlgReal, CDialog)
	//{{AFX_MSG_MAP(CSubDlgReal)
	ON_BN_CLICKED(IDC_BUTTON_STARTREAL, OnButtonStartreal)
	ON_BN_CLICKED(IDC_BUTTON_STOPREAL, OnButtonStopreal)
	ON_BN_CLICKED(IDC_STATIC_WND0, OnStaticWnd0)
	ON_BN_CLICKED(IDC_STATIC_WND1, OnStaticWnd1)
	ON_BN_CLICKED(IDC_STATIC_WND2, OnStaticWnd2)
	ON_BN_CLICKED(IDC_STATIC_WND3, OnStaticWnd3)
	ON_BN_CLICKED(IDC_BUTTON_STARTLREC, OnButtonStartlrec)
	ON_BN_CLICKED(IDC_BUTTON_STOPLREC, OnButtonStoplrec)
	ON_BN_CLICKED(IDC_BUTTON_PICCAPTURE, OnButtonPiccapture)
	ON_BN_CLICKED(IDC_BUTTON_PRESETSET, OnButtonPresetset)
	ON_BN_CLICKED(IDC_BUTTON_PRESETGOTO, OnButtonPresetgoto)
	ON_BN_CLICKED(IDC_BUTTON_PRESETCLE, OnButtonPresetcle)
	ON_BN_CLICKED(IDC_BUTTON_PRESETDEL, OnButtonPresetdel)
	//}}AFX_MSG_MAP
	//ON_BN_CLICKED(IDC_BUTTON_PRESETLU, &CSubDlgReal::OnBnClickedButtonPresetlu)
	//ON_BN_CLICKED(IDC_BUTTON_PRESETU, &CSubDlgReal::OnBnClickedButtonPresetu)
	//ON_BN_CLICKED(IDC_BUTTON_PRESETRU, &CSubDlgReal::OnBnClickedButtonPresetru)
	//ON_BN_CLICKED(IDC_BUTTON_PRESETL, &CSubDlgReal::OnBnClickedButtonPresetl)
	//ON_BN_CLICKED(IDC_BUTTON_PRESETR, &CSubDlgReal::OnBnClickedButtonPresetr)
	//ON_BN_CLICKED(IDC_BUTTON_PRESETLD, &CSubDlgReal::OnBnClickedButtonPresetld)
	//ON_BN_CLICKED(IDC_BUTTON_PRESETD, &CSubDlgReal::OnBnClickedButtonPresetd)
	//ON_BN_CLICKED(IDC_BUTTON_RD, &CSubDlgReal::OnBnClickedButtonRd)
	ON_BN_CLICKED(IDC_BUTTON_PRESET3D, &CSubDlgReal::OnBnClickedButtonPreset3d)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubDlgReal message handlers

BOOL CSubDlgReal::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_EDIT_STREAMTYPE)->SetWindowText("0");
	GetDlgItem(IDC_EDIT_PRESETNO)->SetWindowText("0");
	GetDlgItem(IDC_EDIT_RECPATH)->SetWindowText("C:\\test_rec");
	GetDlgItem(IDC_EDIT_PICPATH)->SetWindowText("C:\\test_pic");
	GetDlgItem(IDC_EDIT_CAMID)->SetWindowText("");
	GetDlgItem(IDC_EDIT_SPEED)->SetWindowText("10");

	lRet = -1;

	sta[3] = &m_wnd3;
	sta[2] = &m_wnd2;
	sta[1] = &m_wnd1;
	sta[0] = &m_wnd0;

	m_startRect = FALSE;
	bCounter = 0;

	m_combox.InsertString(0, "0:jpg");
	m_combox.InsertString(1, "1:bmp");


	m_combox.SetCurSel(0);
	return TRUE;
	// return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSubDlgReal::OnStaticWnd0()
{
	m_nWndSel = 0;
}

void CSubDlgReal::OnStaticWnd1()
{
	m_nWndSel = 1;
}

void CSubDlgReal::OnStaticWnd2()
{
	m_nWndSel = 2;
}

void CSubDlgReal::OnStaticWnd3()
{
	m_nWndSel = 3;
}

void CSubDlgReal::OnButtonStartreal()
{
	CString csID;
	int nStreamType;

	GetDlgItemText(IDC_EDIT_CAMID, csID);
	nStreamType = GetDlgItemInt(IDC_EDIT_STREAMTYPE);

	//
	long lRet = -1;

	//for ( int i = 0; i < 3; i++ )
	{
		if ( g_pMainDlg->m_lPlayHandle[0] < 0 )
		{
			CButton *m_handle = (CButton *)GetDlgItem(IDC_CHECK1);
			if ( m_handle->GetCheck() )
			{
				lRet = Std_StartRealPlay(g_pMainDlg->m_lLoginID,
										 csID.GetBuffer(),
										 *sta[0], nStreamType,
										 StreamCallBack, NULL);
			}
			else
			{
				lRet = Std_StartRealPlay(g_pMainDlg->m_lLoginID,
										 csID.GetBuffer(),
										 NULL, nStreamType,
										 StreamCallBack, NULL);
			}

			g_pMainDlg->m_lPlayHandle[0] = lRet;

			//break;
		}
	}

	g_pMainDlg->PrintCallMsg("Std_StartRealPlay()", lRet);
}

void CSubDlgReal::OnButtonStopreal()
{
	long lRet = -1;
	//for ( int i = 0; i < 4; i++ )
	//{
	//	if ( g_pMainDlg->m_lPlayHandle[i] != -1 )
	//	{
	//		if ( g_pMainDlg->m_lPlayHandle[i] < 0 )
	//		{
	//			continue;
	//		}

	//		lRet = Std_StopRealPlay(g_pMainDlg->m_lPlayHandle[i]);

	//		if ( lRet >= 0 )
	//		{
	//			g_pMainDlg->m_lPlayHandle[i] = -1;

	//			sta[i]->Invalidate();
	//		}
	//	}
	//	else
	//	{
	//		lRet = -1;
	//	}

	//	g_pMainDlg->PrintCallMsg("Std_StopRealPlay()", lRet);
	//}

	lRet = Std_StopRealPlay(g_pMainDlg->m_lPlayHandle[0]);

	g_pMainDlg->PrintCallMsg("Std_StopRealPlay()", lRet);

	if ( lRet >= 0 )
	{
		g_pMainDlg->m_lPlayHandle[0] = -1;

		sta[0]->Invalidate();
	}

}


void CSubDlgReal::OnButtonStartlrec()
{
	CString csRec;
	CString csMsg;
	char czFileExt[64] = { 0 };
	long lFileExt = 64;

	GetDlgItemText(IDC_EDIT_RECPATH, csRec);

	long lRet = -1;

	string szBuf = AnsiToUtf8(csRec.GetBuffer());


	lRet = Std_StartLocalRecord(g_pMainDlg->m_lPlayHandle[0], szBuf.c_str(), czFileExt, lFileExt);

	g_pMainDlg->m_lRecHandle[0] = lRet;

	g_pMainDlg->PrintCallMsg("Std_StartLocalRecord()", lRet);

	csMsg.Format("录像路径:%s%s", csRec, czFileExt);

	g_pMainDlg->ShowMsg(csMsg);
}

void CSubDlgReal::OnButtonStoplrec()
{
	long lRet = -1;

	lRet = Std_StopLocalRecord(g_pMainDlg->m_lPlayHandle[0]);

	if ( lRet >= 0 )
	{
		g_pMainDlg->m_lRecHandle[0] = -1;
		(HANDLE)g_pMainDlg->m_lRecHandle[0];

	}

	g_pMainDlg->PrintCallMsg("Std_StopLocalRecord()", lRet);
}

void CSubDlgReal::OnButtonPiccapture()
{
	CString csPic;
	CString csMsg;
	int nPicFormat;

	GetDlgItemText(IDC_EDIT_PICPATH, csPic);

	//nPicFormat = GetDlgItemInt(IDC_EDIT_PICFORMAT);

	int index = m_combox.GetCurSel();

	long lRet = -1;

	string	szBuf = AnsiToUtf8(csPic.GetBuffer());

	lRet = Std_Capture(g_pMainDlg->m_lPlayHandle[0], szBuf.c_str(), index);

	g_pMainDlg->m_lRecHandle[0] = lRet;

	g_pMainDlg->PrintCallMsg("Std_Capture()", lRet);

}

void CSubDlgReal::CheckLastError()
{
	if ( lRet == -99 )
	{
		int nErrNo = -1;
		char czErrDesc[512] = { 0 };

		long lRet = -1;

		lRet = Std_GetLastError(czErrDesc);

		g_pMainDlg->PrintCallMsg("Std_GetLastError()", lRet);

		if ( lRet >= 0 )
		{
			CString csMsg;

			string tmp;
			tmp = Utf8toAnsi(czErrDesc);

			csMsg.Format("错误描述:%s", tmp.c_str());

			g_pMainDlg->ShowMsg(csMsg);
		}
	}
}

void CSubDlgReal::OnButtonPresetset()
{
	// TODO: Add your control notification handler code here

	try
	{
		CString CsId;
		GetDlgItemText(IDC_EDIT_CAMID, CsId);

		CString CsName;
		GetDlgItemText(IDC_EDIT_NAME, CsName);

		int CsNo = GetDlgItemInt(IDC_EDIT_PRESETNO);


		char *name = CsName.GetBuffer();

		int param1 = *(int*)&name;

		char *msg = (char*)param1;

		//int param1 = GetDlgItemInt(IDC_EDIT_PRESETNO);

		long lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID,
								CsId.GetBuffer(), "SET_PRESET", CsNo, param1, 0);

		g_pMainDlg->PrintCallMsg("Std_PtzCtrl()", lRet);

		//throw new exception();

	}
	catch ( ... )
	{
		g_pMainDlg->PrintCallMsg("Std_PtzCtrl exception", 1);
	}
}

void CSubDlgReal::OnButtonPresetgoto()
{
	// TODO: Add your control notification handler code here

	CString CsId;
	GetDlgItemText(IDC_EDIT_CAMID, CsId);

	int param1 = GetDlgItemInt(IDC_EDIT_PRESETNO);

	long lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, CsId.GetBuffer(), "GOTO_PRESET", param1, 0, 0);

	g_pMainDlg->PrintCallMsg("Std_PtzCtrl()", lRet);
}

void CSubDlgReal::OnButtonPresetcle()
{
	// TODO: Add your control notification handler code here

	CString CsId;
	GetDlgItemText(IDC_EDIT_CAMID, CsId);

	int param1 = GetDlgItemInt(IDC_EDIT_PRESETNO);

	long lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, CsId.GetBuffer(), "CLE_PRESET", 0, 0, 0);

	g_pMainDlg->PrintCallMsg("Std_PtzCtrl()", lRet);
}

void CSubDlgReal::OnButtonPresetdel()
{
	// TODO: Add your control notification handler code here

	CString CsId;
	GetDlgItemText(IDC_EDIT_CAMID, CsId);

	int param1 = GetDlgItemInt(IDC_EDIT_PRESETNO);

	long lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, CsId.GetBuffer(), "DEL_PRESET", param1, 0, 0);

	g_pMainDlg->PrintCallMsg("Std_PtzCtrl()", lRet);
}


long CSubDlgReal::PtzCtrl(BOOL  btype, int PtzCmdIndex)
{
	CString csID;
	GetDlgItemText(IDC_EDIT_CAMID, csID);

	if ( !btype )
	{
		PtzCmdIndex += 10;
	}

	return Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID.GetBuffer(), PtzCmd[PtzCmdIndex], 0, 0, 0);
}



void CSubDlgReal::OnBnClickedButtonPreset3d()
{
	// TODO:  在此添加控件通知处理程序代码
	CString csID;
	GetDlgItemText(IDC_EDIT_CAMID, csID);

	//lRet = Std_PtzCtrl3D(g_pMainDlg->m_lLoginID, csID.GetBuffer(), 20, 20, 20, 20, 1);

	g_pMainDlg->PrintCallMsg("Std_PtzCtrl()", lRet);

}

void CSubDlgReal::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	CClientDC dc(this);
	dc.SetROP2(R2_NOT);
	dc.SelectStockObject(NULL_BRUSH);
	dc.Rectangle(CRect(m_startPoint, m_OldPoint));

	ZeroMemory(&m_startPoint, sizeof(CPoint));
	ZeroMemory(&m_OldPoint, sizeof(CPoint));

	CRect wndrc;
	CString msg;

	::GetWindowRect(sta[0]->GetSafeHwnd(), &wndrc);

	ClientToScreen(&point);

	if ( PtInRect(&wndrc, point) )
	{
		ClipCursor(&wndrc);
		msg.Format("OnLButtonDown In x= %d, y= %d", point.x, point.y);
	}
	else
	{
		msg.Format("OnLButtonDown Out x= %d, y= %d", point.x, point.y);
		return;
	}

	ScreenToClient(&point);

	m_startRect = TRUE;   //鼠标左键单击，设置可以开始绘制矩形框 
	m_startPoint = point; //记录开始点 
	m_OldPoint = point;   //设置老点也为开始点

	CDialog::OnLButtonDown(nFlags, point);
}


void CSubDlgReal::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	if ( !m_startRect )
	{
		return;
	}

	CPoint m_s = m_startPoint;
	CPoint m_o = m_OldPoint;

	ClientToScreen(&m_s);
	ClientToScreen(&m_o);

	sta[0]->ScreenToClient(&m_s);
	sta[0]->ScreenToClient(&m_o);

	int left2right = m_o.x - m_s.x;
	int top2bottom = m_o.y - m_s.y;

	bCounter = 0;

	if ( left2right > 2 && top2bottom > 2 ) //右下
	{
		bCounter = 4;
	}
	else if ( left2right < 2 && top2bottom > 2 )//左下
	{
		bCounter = 3;
	}
	else if ( left2right > 2 && top2bottom < 2 )//右上
	{
		bCounter = 2;
	}
	else if ( left2right < 2 && top2bottom < 2 )//左上
	{
		bCounter = 1;
	}

	CString csID;
	GetDlgItemText(IDC_EDIT_CAMID, csID);

	CRect wndrc;
	::GetWindowRect(sta[0]->GetSafeHwnd(), &wndrc);


	int w = wndrc.Width();
	int h = wndrc.Height();

	int xTop = 352 * m_s.x / wndrc.Width();
	int yTop = 288 * m_s.y / wndrc.Height();
	int xBottom = 352 * m_o.x / wndrc.Width();
	int yBottom = 288 * m_o.y / wndrc.Height();


	if ( (abs(left2right) > 2) && (abs(top2bottom) > 2) )
	{
		lRet = Std_PtzCtrl3D(g_pMainDlg->m_lLoginID,
							 csID.GetBuffer(),
							 xTop, yTop, xBottom, yBottom,
							 bCounter);

		g_pMainDlg->PrintCallMsg("Std_PtzCtrl()", lRet);
	}

	////消隐最后的一个矩形（其原理跟拖动时矩形框绘制原理相同）
	//CClientDC dc(this);
	//dc.SetROP2(R2_NOT);
	//dc.SelectStockObject(NULL_BRUSH);
	//dc.Rectangle(CRect(m_startPoint, m_OldPoint));

	m_startRect = FALSE; //重置绘制矩形框标志 
	ClipCursor(NULL);

	CDialog::OnLButtonUp(nFlags, point);

}


void CSubDlgReal::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	CClientDC dc(this);   //获取设备句柄 
	//SetRop2 Specifies the new drawing mode.(MSDN) 
	//R2_NOT   Pixel is the inverse of the screen color.(MSDN) 
	//即：该函数用来定义绘制的颜色，而该参数则将颜色设置为原屏幕颜色的反色 
	//这样，如果连续绘制两次的话，就可以恢复原来屏幕的颜色了（如下） 
	//但是，这里的连续两次绘制却不是在一次消息响应中完成的 
	//而是在第一次拖动响应的绘制可以显示（也就是看到的），第二次拖动绘制实现擦出（也就看不到了） 

	CRect wndrc;
	CString msg;

	::GetWindowRect(sta[0]->GetSafeHwnd(), &wndrc);

	ScreenToClient(&wndrc);

	if ( PtInRect(&wndrc, point) )
	{
		msg.Format("OnMouseMove In x= %d, y= %d", point.x, point.y);
	}
	else
	{
		msg.Format("OnMouseMove Out x= %d, y= %d", point.x, point.y);
	}

	SetWindowText(msg.GetBuffer());

	dc.SetROP2(R2_NOT);   //此为关键!!! 
	dc.SelectStockObject(NULL_BRUSH); //不使用画刷 

	if ( TRUE == m_startRect )   //根据是否有单击判断是否可以画矩形 
	{
		dc.Rectangle(CRect(m_startPoint, m_OldPoint));
		dc.Rectangle(CRect(m_startPoint, point));

		m_OldPoint = point;
	}

	CDialog::OnMouseMove(nFlags, point);
}
