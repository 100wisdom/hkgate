#pragma once


// SubDlgTalk 对话框

class SubDlgTalk : public CDialog
{
	DECLARE_DYNAMIC(SubDlgTalk)

public:
	SubDlgTalk(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~SubDlgTalk();

// 对话框数据
	enum { IDD = IDD_DLG_SUB_TALK };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedButtonStarttalk();
    afx_msg void OnBnClickedButtonStoptalk();

private:
    long m_TalkHandle;
};
