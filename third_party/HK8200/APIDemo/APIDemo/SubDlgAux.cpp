// SubDlgAux.cpp : implementation file
//

#include "stdafx.h"
#include "APIDemo.h"
#include "SubDlgAux.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubDlgAux dialog


CSubDlgAux::CSubDlgAux(CWnd* pParent /*=NULL*/)
	: CDialog(CSubDlgAux::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSubDlgAux)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSubDlgAux::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubDlgAux)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSubDlgAux, CDialog)
	//{{AFX_MSG_MAP(CSubDlgAux)
	ON_BN_CLICKED(IDC_BUTTON_GETERROR, OnButtonGeterror)
	ON_BN_CLICKED(IDC_BUTTON_GETSDKVER, OnButtonGetsdkver)
	ON_BN_CLICKED(IDC_BUTTON_GETPFVER, OnButtonGetpfver)
	ON_BN_CLICKED(IDC_BUTTON_GETPFABI, OnButtonGetpfabi)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubDlgAux message handlers

BOOL CSubDlgAux::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	GetDlgItem(IDC_EDIT_ABITYPE)->SetWindowText("1");

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSubDlgAux::OnButtonGeterror()
{
	int nErrNo = -1;
	char czErrDesc[512] = { 0 };

	long lRet = -1;

	lRet = Std_GetLastError(czErrDesc);

	g_pMainDlg->PrintCallMsg("Std_GetLastError()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		string tmp;
		tmp = Utf8toAnsi(czErrDesc);

		csMsg.Format("��������:%s", tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgAux::OnButtonGetsdkver()
{
	char czSDKVer[32] = { 0 };

	long lRet = -1;

	lRet = Std_GetSDKVersion(czSDKVer);

	g_pMainDlg->PrintCallMsg("Std_GetSDKVersion()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		string tmp;
		tmp = Utf8toAnsi(czSDKVer);

		csMsg.Format("SDK�汾��:%s", tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgAux::OnButtonGetpfver()
{
	char czPlatformVer[32] = { 0 };

	long lRet = -1;

	lRet = Std_GetPlatformVersion(czPlatformVer);

	g_pMainDlg->PrintCallMsg("Std_GetPlatformVersion()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;
		//char tmp[1024] = { 0 };

		string tmp;
		tmp = Utf8toAnsi(czPlatformVer);

		csMsg.Format("ƽ̨�汾��:%s", tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}
}

void CSubDlgAux::OnButtonGetpfabi()
{
	int nAbiType;
	char czAbility[1024] = { 0 };

	nAbiType = GetDlgItemInt(IDC_EDIT_ABITYPE);


	long lRet = -1;

	lRet = Std_GetPlatformAbility(g_pMainDlg->m_lLoginID, nAbiType, czAbility);

	g_pMainDlg->PrintCallMsg("Std_GetPlatformAbility()", lRet);

	if ( lRet >= 0 )
	{
		CString csMsg;

		string tmp;
		tmp = Utf8toAnsi(czAbility);

		csMsg.Format("����������:%s", tmp.c_str());

		g_pMainDlg->ShowMsg(csMsg);
	}

}
