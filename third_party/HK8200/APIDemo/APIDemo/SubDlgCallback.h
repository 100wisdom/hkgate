
#pragma once

class CSubDlgCallback : public CDialog
{

public:
	CSubDlgCallback(CWnd* pParent = NULL);   // standard constructor

	enum
	{
		IDD = IDD_DLG_SUB_CALLBACK
	};
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:

	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSetrdcbf();
	afx_msg void OnButtonSetvdcbf();
	afx_msg void OnButtonSetmsgcbf();
	afx_msg void OnButtonSetstatuscbf();
	afx_msg void OnButtonSetdrawcbf();
	afx_msg void OnButtonSetddcbf();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton10();


public:
	USERDATA userdata;

	afx_msg void OnBnClickedButtonSetalarmcbf();
	afx_msg void OnBnClickedButton11();
};

