#include "stdafx.h"
#include "MyButton.h"
#include "APIDemo.h"
#include "../include/StdClient.h"
#include "SubDlgLocalRec.h"
#include <Windows.h>

CMyButton::CMyButton()
{

}


void CMyButton::Set(CComboBox *cb)
{
	m_cb = cb;
	GetClientRect(&rect);
	//ScreenToClient(&rect);
}


CMyButton::~CMyButton()
{

}


BEGIN_MESSAGE_MAP(CMyButton, CButton)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_NCLBUTTONUP()
END_MESSAGE_MAP()


void CMyButton::OnLButtonDown(UINT nFlags, CPoint point)
{
	//AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO:  在此添加消息处理程序代码和/或调用默认值


	int CtrlID = this->GetDlgCtrlID();

	char csID[100] = { 0 };
	HWND hp = ::GetParent(this->GetSafeHwnd());

	::GetDlgItemText(hp, IDC_EDIT_CAMID, csID, 100);

	BOOL pbt = TRUE;
	int param1 = ::GetDlgItemInt(hp, IDC_EDIT_SPEED, &pbt, TRUE);

	long lRet = -1;

	switch ( CtrlID )
	{
	case IDC_BUTTON_PRESETLU:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "left_up", param1, 0, 0);
	break;

	case IDC_BUTTON_PRESETU:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "up", param1, 0, 0);
	break;

	case IDC_BUTTON_PRESETRU:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "right_up", param1, 0, 0);
	break;

	case IDC_BUTTON_PRESETL:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "left", param1, 0, 0);
	break;

	case IDC_BUTTON_PRESETR:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "right", param1, 0, 0);
	break;

	case IDC_BUTTON_PRESETLD:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "left_down", param1, 0, 0);
	break;

	case IDC_BUTTON_PRESETD:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "down", param1, 0, 0);
	break;

	case IDC_BUTTON_RD:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "right_down", 0, 0, 0);
	break;


	case IDC_BUTTON_ZOOMIN:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "zoomin", 0, 0, 0);
	break;

	case IDC_BUTTON_ZOOMOUT:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "zoomout", 0, 0, 0);
	break;

	default:
	lRet = 0;

	break;
	}

	CString msg;
	msg.Format("Std_PtzCtrl: %s", csID);

	g_pMainDlg->PrintCallMsg(msg.GetBuffer(), lRet);

	//AfxMessageBox(ss);

	//CRect rcWork;
	//CString strDateTime;

	//SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWork, 0);

	//CRect wndrc;
	//GetWindowRect(&wndrc);

	//CPoint pt;
	//GetCursorPos(&pt);

	//if ( PtInRect(&wndrc, pt) )
	//{
	//	ClipCursor(wndrc);
	//}

	//int cnt = m_cb->GetCount();
	//m_cb->InsertString(cnt, ss.GetBuffer());
	//m_cb->SetCurSel(cnt);

	CButton::OnLButtonDown(nFlags, point);
}


void CMyButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值


	int CtrlID = this->GetDlgCtrlID();

	char csID[100] = { 0 };
	HWND hp = ::GetParent(this->GetSafeHwnd());

	::GetDlgItemText(hp, IDC_EDIT_CAMID, csID, 100);

	BOOL pbt = TRUE;
	int param1 = ::GetDlgItemInt(hp, IDC_EDIT_SPEED, &pbt, TRUE);

	long lRet = -1;
	switch ( CtrlID )
	{
	case IDC_BUTTON_PRESETLU:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "left_up_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_PRESETU:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "up_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_PRESETRU:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "right_up_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_PRESETL:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "left_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_PRESETR:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "right_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_PRESETLD:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "left_down_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_PRESETD:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "down_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_RD:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "right_down_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_ZOOMIN:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "zoomin_stop", 0, 0, 0);
	break;

	case IDC_BUTTON_ZOOMOUT:
	lRet = Std_PtzCtrl(g_pMainDlg->m_lLoginID, csID, "zoomout_stop", 0, 0, 0);
	break;
	default:
	lRet = -1;
	break;
	}

	CString msg;
	msg.Format("Std_PtzCtrl: %s", csID);

	g_pMainDlg->PrintCallMsg(msg.GetBuffer(), lRet);

	//ClipCursor(NULL);

	CButton::OnLButtonUp(nFlags, point);
}


void CMyButton::OnNcLButtonUp(UINT nHitTest, CPoint point)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	//int cnt = m_cb->GetCount();
	//m_cb->InsertString(cnt, "OnNcLButtonUp");
	//m_cb->SetCurSel(cnt);

	CButton::OnNcLButtonUp(nHitTest, point);
}
