// SubDlgCallback.cpp : implementation file
//

#include "stdafx.h"
#include "APIDemo.h"
#include "SubDlgCallback.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubDlgCallback dialog


CSubDlgCallback::CSubDlgCallback(CWnd* pParent /*=NULL*/)
: CDialog(CSubDlgCallback::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSubDlgCallback)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSubDlgCallback::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubDlgCallback)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSubDlgCallback, CDialog)
	//{{AFX_MSG_MAP(CSubDlgCallback)
	ON_BN_CLICKED(IDC_BUTTON_SETRDCBF, OnButtonSetrdcbf)
	ON_BN_CLICKED(IDC_BUTTON_SETVDCBF, OnButtonSetvdcbf)
	ON_BN_CLICKED(IDC_BUTTON_SETMSGCBF, OnButtonSetmsgcbf)
	ON_BN_CLICKED(IDC_BUTTON_SETSTATUSCBF, OnButtonSetstatuscbf)
	ON_BN_CLICKED(IDC_BUTTON_SETDRAWCBF, OnButtonSetdrawcbf)
	ON_BN_CLICKED(IDC_BUTTON_SETDDCBF, OnButtonSetddcbf)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CSubDlgCallback::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSubDlgCallback::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CSubDlgCallback::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CSubDlgCallback::OnBnClickedButton4)
    ON_BN_CLICKED(IDC_BUTTON5, &CSubDlgCallback::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON9, &CSubDlgCallback::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, &CSubDlgCallback::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON_SETALARMCBF, &CSubDlgCallback::OnBnClickedButtonSetalarmcbf)
	ON_BN_CLICKED(IDC_BUTTON11, &CSubDlgCallback::OnBnClickedButton11)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubDlgCallback message handlers

BOOL CSubDlgCallback::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here

	::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON1), FALSE);
	::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON2), FALSE);
	::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON3), FALSE);
	::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON4), FALSE);
	::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON9), FALSE);
	::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON10), FALSE);
	::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON11), FALSE);
	return TRUE;

	// return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSubDlgCallback::OnButtonSetrdcbf()
{
	if ( (g_pMainDlg->m_lPlayHandle[0] < 0) &&
		(g_pMainDlg->m_lRePlayHandleL < 0) &&
		(g_pMainDlg->m_lRePlayHandleR < 0) )
	{
		g_pMainDlg->ShowMsg("call Std_SetSteamCBF(),未开始实时预览或录像播放!");
	}


	long lRet = -1;

	if ( g_pMainDlg->m_lPlayHandle[0] >= 0 )
	{
		userdata.ctype = RealPlay;
		strcpy_s(userdata.msg, AnsiToUtf8("实时").c_str());

		lRet = Std_SetSteamCBF(g_pMainDlg->m_lPlayHandle[0],
							   StreamCallBack, (void*)&userdata);

		g_pMainDlg->PrintCallMsg("Std_SetSteamCBF()|实时", lRet);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON1), TRUE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETRDCBF), FALSE);
		}

	}

	if ( g_pMainDlg->m_lRePlayHandleL >= 0 )
	{
		userdata.ctype = LocalPlay;
		strcpy_s(userdata.msg, AnsiToUtf8("本地回放").c_str());

		lRet = Std_SetSteamCBF(g_pMainDlg->m_lRePlayHandleL,
							   StreamCallBack, (void*)&userdata);

		g_pMainDlg->PrintCallMsg("Std_SetSteamCBF()|本地回放", lRet);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON1), TRUE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETRDCBF), FALSE);
		}

	}

	if ( g_pMainDlg->m_lRePlayHandleR >= 0 )
	{
		userdata.ctype = RemotePlay;
		strcpy_s(userdata.msg, AnsiToUtf8("远程回放").c_str());

		lRet = Std_SetSteamCBF(g_pMainDlg->m_lRePlayHandleR,
							   StreamCallBack, (void*)&userdata);

		g_pMainDlg->PrintCallMsg("Std_SetSteamCBF()|远程回放", lRet);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON1), TRUE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETRDCBF), FALSE);
		}
	}
}



void CSubDlgCallback::OnButtonSetvdcbf()
{

	if ( (g_pMainDlg->m_lPlayHandle[0] < 0) &&
		(g_pMainDlg->m_lRePlayHandleL < 0) &&
		(g_pMainDlg->m_lRePlayHandleR < 0) )
	{
		g_pMainDlg->ShowMsg("call Std_SetVideoDataCBF(),未开始实时预览或录像播放!");
	}


	long lRet = -1;

	if ( g_pMainDlg->m_lPlayHandle[0] >= 0 )
	{
		lRet = Std_SetVideoDataCBF(g_pMainDlg->m_lPlayHandle[0], VideoDataStreamCallBack, NULL);

		//g_pMainDlg->PrintCallMsg("Std_SetVideoDataCBF()|实时", lRet);

		char *n = "2";
		char *msg = "Std_SetVideoDataCBF()|实时";
		g_pMainDlg->ShowMsg(msg);
	}

	if ( g_pMainDlg->m_lRePlayHandleL >= 0 )
	{
		lRet = Std_SetVideoDataCBF(g_pMainDlg->m_lRePlayHandleL, VideoDataStreamCallBack, NULL);

		//g_pMainDlg->PrintCallMsg("Std_SetVideoDataCBF()|本地回放", lRet);

		char *n = "2";
		char *msg = "Std_SetVideoDataCBF()|本地回放";
		char *a = new char[512];
		strcpy_s(a, 512, msg);

		::PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, (WPARAM)n, (LPARAM)a);
	}

	if ( g_pMainDlg->m_lRePlayHandleR >= 0 )
	{
		lRet = Std_SetVideoDataCBF(g_pMainDlg->m_lRePlayHandleR, VideoDataStreamCallBack, NULL);

		//g_pMainDlg->PrintCallMsg("Std_SetVideoDataCBF()|远程回放", lRet);

		char *n = "2";
		char *msg = "Std_SetVideoDataCBF()|远程回放";

		char *a = new char[512];
		strcpy_s(a, 512, msg);

		::PostMessage(g_pMainDlg->GetSafeHwnd(), CALLBACKMSG, (WPARAM)n, (LPARAM)a);
	}


	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON2), TRUE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETVDCBF), FALSE);
	}
}

void CSubDlgCallback::OnButtonSetmsgcbf()
{
	long lRet = -1;

	if ( g_pMainDlg->m_lLoginID >= 0 )
	{
		lRet = Std_SetMessageCBF(g_pMainDlg->m_lLoginID, MsgCallBack, NULL);

 		char *msg = "Std_SetMessageCBF";
 
		g_pMainDlg->ShowMsg(msg);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON3), TRUE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETMSGCBF), FALSE);
		}
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetMessageCBF(),未登录平台!");
	}
}

void CSubDlgCallback::OnButtonSetstatuscbf()
{
	long lRet = -1;

	if ( g_pMainDlg->m_lLoginID >= 0 )
	{
		lRet = Std_SetStatusCBF(g_pMainDlg->m_lLoginID, StatusCallBack, NULL);

		char *msg = "Std_SetStatusCBF";

		g_pMainDlg->ShowMsg(msg);
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetStatusCBF(),未登录平台!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON4), TRUE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETSTATUSCBF), FALSE);
	}
}

void CSubDlgCallback::OnButtonSetdrawcbf()
{
	long lRet = -1;

	if ( g_pMainDlg->m_lPlayHandle[0] >= 0 )
	{
		lRet = Std_SetDrawCBF(g_pMainDlg->m_lPlayHandle[0], DrawCallBack, NULL);

		//g_pMainDlg->PrintCallMsg("Std_SetDrawCBF()", lRet);
 
		char *msg = "Std_SetDrawCBF";

		g_pMainDlg->ShowMsg(msg);

	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetDrawCBF(),未开始视频实时预览!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON9), TRUE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETDRAWCBF), FALSE);
	}
}

void CSubDlgCallback::OnButtonSetddcbf()
{
	long lRet = -1;

	if ( g_pMainDlg->m_lDownloadHandle >= 0 )
	{
		lRet = Std_SetDownloadDataCBF(g_pMainDlg->m_lDownloadHandle, StreamCallBack, NULL);

		char *msg = "Std_SetDownloadDataCBF";

		g_pMainDlg->ShowMsg(msg);
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetDownloadDataCBF(),未开始录像下载!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON10), TRUE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETDDCBF), FALSE);
	}
}



void CSubDlgCallback::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码

	if ( (g_pMainDlg->m_lPlayHandle[0] < 0) &&
		(g_pMainDlg->m_lRePlayHandleL < 0) &&
		(g_pMainDlg->m_lRePlayHandleR < 0) )
	{
		g_pMainDlg->ShowMsg("call Std_SetSteamCBF(),未开始实时预览或录像播放!");
	}


	long lRet = -1;

	if ( g_pMainDlg->m_lPlayHandle[0] >= 0 )
	{
		lRet = Std_SetSteamCBF(g_pMainDlg->m_lPlayHandle[0], NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetSteamCBF()|实时", lRet);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON1), FALSE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETRDCBF), TRUE);
		}

	}

	if ( g_pMainDlg->m_lRePlayHandleL >= 0 )
	{
		lRet = Std_SetSteamCBF(g_pMainDlg->m_lRePlayHandleL, NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetSteamCBF()|本地回放", lRet);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON1), FALSE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETRDCBF), TRUE);
		}

	}

	if ( g_pMainDlg->m_lRePlayHandleR >= 0 )
	{


		lRet = Std_SetSteamCBF(g_pMainDlg->m_lRePlayHandleR,
							   NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetSteamCBF()|远程回放", lRet);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON1), FALSE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETRDCBF), TRUE);
		}
	}


}



void CSubDlgCallback::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码


	if ( (g_pMainDlg->m_lPlayHandle[0] < 0) &&
		(g_pMainDlg->m_lRePlayHandleL < 0) &&
		(g_pMainDlg->m_lRePlayHandleR < 0) )
	{
		g_pMainDlg->ShowMsg("call Std_SetVideoDataCBF(),未开始实时预览或录像播放!");
	}


	long lRet = -1;

	if ( g_pMainDlg->m_lPlayHandle[0] >= 0 )
	{
		lRet = Std_SetVideoDataCBF(g_pMainDlg->m_lPlayHandle[0], NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetVideoDataCBF()|实时", lRet);
	}

	if ( g_pMainDlg->m_lRePlayHandleL >= 0 )
	{
		lRet = Std_SetVideoDataCBF(g_pMainDlg->m_lRePlayHandleL, NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetVideoDataCBF()|本地回放", lRet);
	}

	if ( g_pMainDlg->m_lRePlayHandleR >= 0 )
	{
		lRet = Std_SetVideoDataCBF(g_pMainDlg->m_lRePlayHandleR, NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetVideoDataCBF()|远程回放", lRet);
	}


	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON2), FALSE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETVDCBF), TRUE);
	}



}


void CSubDlgCallback::OnBnClickedButton3()
{
	// TODO:  在此添加控件通知处理程序代码

	long lRet = -1;

	if ( g_pMainDlg->m_lLoginID >= 0 )
	{
		lRet = Std_SetMessageCBF(g_pMainDlg->m_lLoginID, NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetMessageCBF()", lRet);

		if ( lRet >= 0 )
		{
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON3), FALSE);
			::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETMSGCBF), TRUE);
		}
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetMessageCBF(),未登录平台!");
	}

}


void CSubDlgCallback::OnBnClickedButton4()
{
	// TODO:  在此添加控件通知处理程序代码
	long lRet = -1;

	if ( g_pMainDlg->m_lLoginID >= 0 )
	{
		lRet = Std_SetStatusCBF(g_pMainDlg->m_lLoginID, NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetStatusCBF()", lRet);
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetStatusCBF(),未登录平台!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON4), FALSE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETSTATUSCBF), TRUE);
	}

}

void CSubDlgCallback::OnBnClickedButton5()
{
	g_pMainDlg->IsSaveBmp = TRUE;
}


void CSubDlgCallback::OnBnClickedButton9()
{
	// TODO:  在此添加控件通知处理程序代码
	long lRet = -1;

	if ( g_pMainDlg->m_lPlayHandle[0] >= 0 )
	{
		lRet = Std_SetDrawCBF(g_pMainDlg->m_lPlayHandle[0], NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetDrawCBF()", lRet);
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetDrawCBF(),未开始视频实时预览!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON9), FALSE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETDRAWCBF), TRUE);
	}

}


void CSubDlgCallback::OnBnClickedButton10()
{
	// TODO:  在此添加控件通知处理程序代码
	long lRet = -1;

	if ( g_pMainDlg->m_lDownloadHandle >= 0 )
	{
		lRet = Std_SetDownloadDataCBF(g_pMainDlg->m_lDownloadHandle, NULL, NULL);

		g_pMainDlg->PrintCallMsg("Std_SetDownloadDataCBF()", lRet);
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SetDownloadDataCBF(),未开始录像!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON10), FALSE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETDDCBF), TRUE);

	}

}

void CSubDlgCallback::OnBnClickedButtonSetalarmcbf()
{
	// TODO: 在此添加控件通知处理程序代码
	long lRet = -1;
	if ( g_pMainDlg->m_lLoginID >= 0 )
	{
		lRet = Std_SubscribeAlarm(g_pMainDlg->m_lLoginID, AlarmCallback, (void*)&userdata);

		g_pMainDlg->PrintCallMsg("Std_SubscribeAlarm()", lRet);
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_SubscribeAlarm(),未登录平台!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON11), TRUE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETALARMCBF), FALSE);
	}
}

void CSubDlgCallback::OnBnClickedButton11()
{
	// TODO: 在此添加控件通知处理程序代码
	long lRet = -1;
	if ( g_pMainDlg->m_lLoginID >= 0 )
	{
		lRet = Std_UnSubscribeAlarm(g_pMainDlg->m_lLoginID);

		g_pMainDlg->PrintCallMsg("Std_UnSubscribeAlarm()", lRet);
	}
	else
	{
		g_pMainDlg->ShowMsg("call Std_UnSubscribeAlarm(),未登录平台!");
	}

	if ( lRet >= 0 )
	{
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON11), FALSE);
		::EnableWindow(::GetDlgItem(this->GetSafeHwnd(), IDC_BUTTON_SETALARMCBF), TRUE);
	}
}
