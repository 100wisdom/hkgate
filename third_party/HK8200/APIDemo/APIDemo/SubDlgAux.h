#pragma once

class CSubDlgAux : public CDialog
{
public:
	CSubDlgAux(CWnd* pParent = NULL);   // standard constructor

	enum { IDD = IDD_DLG_SUB_AUX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:

	virtual BOOL OnInitDialog();
	afx_msg void OnButtonGeterror();
	afx_msg void OnButtonGetsdkver();
	afx_msg void OnButtonGetpfver();
	afx_msg void OnButtonGetpfabi();
	DECLARE_MESSAGE_MAP()
};
