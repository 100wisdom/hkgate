#ifndef _STD_CLIENT_H_
#define _STD_CLIENT_H_

//#define hwnd void*

#ifdef WINDOWS_DLL_BUILD
#define CLIENT_API _declspec(dllexport)
#elif WINDOWS_DLL_USE
#define CLIENT_API _declspec(dllimport)
#elif LINUX_DLL
#define CLIENT_API
#else
#define CLIENT_API
#endif

typedef unsigned char UCHAR;
typedef unsigned long ULONG;

#define STDCALL __stdcall

//PlayMode
#define PLAYMODE_STOP				1
#define PLAYMODE_PAUSE				2
#define PLAYMODE_RESUME				3
#define PLAYMODE_ONEBYONE			4
#define PLAYMODE_ONEBYONEBACK		5
#define PLAYMODE_16_BACKWARD		6
#define PLAYMODE_8_BACKWARD			7
#define PLAYMODE_4_BACKWARD			8
#define PLAYMODE_2_BACKWARD			9
#define PLAYMODE_1_BACKWARD			10
#define PLAYMODE_HALF_BACKWARD		11
#define PLAYMODE_QUARTER_BACKWARD	12
#define PLAYMODE_Eighth_BACKWARD	13
#define PLAYMODE_Eighth_FORWARD		14
#define PLAYMODE_QUARTER_FORWARD	15
#define PLAYMODE_HALF_FORWARD		16
#define PLAYMODE_1_FORWARD			17
#define PLAYMODE_2_FORWARD			18
#define PLAYMODE_4_FORWARD			19
#define PLAYMODE_8_FORWARD			20
#define PLAYMODE_16_FORWARD			21
//End PlayMode

//对讲音频数据类型定义
#define STD_AUDIO_TYPE_G722_S16K  0
#define STD_AUDIO_TYPE_G711U_S8K  1
#define STD_AUDIO_TYPE_G711A_S8K  2
#define STD_AUDIO_TYPE_MPEG2_S16K 5
#define STD_AUDIO_TYPE_G726_S8K   6
#define STD_AUDIO_TYPE_AAC_S32K   7

#define PACKETHEAD_DEFAULT		0												// 默认值，保留
#define PACKETHEAD_HEARTBEAT	1												// 心跳报文
#define PACKETHEAD_XML			2												// xml格式报文(除心跳),xml格式的语音数据
#define PACKETHEAD_STRUCT		3												// 结构体形式的语音数据
#define	PACKETHEAD_STREAM		4												// 通过SDK获取到的语音流数据 


extern "C"
{
	//YUV数据结构体
	typedef struct tagPictureData
	{
		UCHAR *pucData[4];			/**pucData[0]:Y 平面指针，
									   pucData[1]:U 平面指针，
									   pucData[2]:V 平面指针 */
		ULONG ulLineSize[4];		/**ulLineSize[0]: Y 平面指针，
									   ulLineSize[1]: U 平面指针,
									   ulLineSize[2]: V 平面指针 */
		ULONG ulPicHeight;			/**图片高度 */
		ULONG ulPicWidth;			/**图片宽度 */
		ULONG ulRenderTime;			/**用于渲染的时间数据（单位为毫秒）*/
		ULONG ulReserverParam1;		/**保留参数 */
		ULONG ulReserverParam2;		/**保留参数 */
	}PICTURE_DATA_S,*LPPICTURE_DATA_S;


	//消息类型与消息数据结构体对应表
	//解码异常
	typedef struct tagMsgDecodeException
	{
		long PlayHandle;			/*播放实例号*/
		long ErrorNo;				/*错误号*/
		char *ErrorDesc;			/*错误描述*/
	}MSG_DECODE_EXCEPTION,*LPMSG_DECODE_EXCEPTION;

	//录像异常
	typedef struct tagMsgRecordException
	{
		long RecordHandle;			/*录像实例号*/
		long ErrorNo;				/*错误号*/
		char *ErrorDesc;			/*错误描述*/
	}MSG_RECORD_EXCEPTION,*LPMSG_RECORD_EXCEPTION;

	//录像下载进度
	typedef struct tagMsgDownloadProgress
	{
		long  DownloadHandle;		/*文件下载实例号*/
		long  Progress;    /* 取值范围0--100表示表示进度值*/
		long  Status;     /* 0下载中 1正常结束，2异常结束*/
	}MSG_DOWNLOAD_Progress,*LPMSG_DOWNLOAD_Progress;

	//录像播放进度
	typedef struct tagMsgPlayProgress
	{
		long  PlayHandle;			/*远程回放实例号*/
		long  PlayTimed;  /*已播放的相对时间，单位为秒,用户可结合文件总秒数计算百分比*/
		long  Status ;     /* 0下载中 1正常结束，2异常结束*/
	}MSG_PLAY_Progress,*LPMSG_REPLAY_Progress;

	//用户离线
	typedef struct tagMsgUserOffline
	{
		long LoginHandle;			/*登录实例号*/
		char *UserName;				/*用户名*/
	}MSG_USER_OFFLINE,*LPMSG_USER_OFFLINE;
	//------------------------------------------------------------


	//实时播放回调函数定义
	typedef void (STDCALL *StreamCallbackPF)(long RealHandle, int StreamType, const char *Data, int DataLen, const char *DecoderTag, void *UserData);
	//录像文件检索回调函数定义
	typedef void (STDCALL *RecordFindCallBackPF)(long FindHandle,const char *CameraId , int *iContinue ,int iFinish ,const char *FileListXML,void* UserData);
	//获取资源列表回调函数定义
	typedef void (STDCALL *OrgInfoCallBackPF)(long getResHandle, int *iContinue, int iFinish, const char *FileListXML,void* UserData);
	//YUV数据回调回调函数定义
	typedef void (STDCALL *VideoData_Stream_PF)(long RealHandle, LPPICTURE_DATA_S pPictureData, void *pUserData);
	//消息回调回调函数定义
	typedef void (STDCALL *fMsgCallback)(int MsgType, const char *Data,long DataLen , void* UserData);
	//摄像机状态推送回调函数定义
	typedef void (STDCALL *fStatusCallback)(const char *CameraId, int Status, void *UserData);
	//画图叠加回调函数定义
	typedef void (STDCALL *fDrawCallBack)(long LoginHandle, long PlayHandle, long hDC, void *UserData);
	//报警订阅回调函数定义
	typedef void (STDCALL *fAlarmCallback)(const char* csAlarmDetail, void *pUser);
	//对讲数据回调函数定义
	typedef void(CALLBACK* fVoiceDataCallBack)(int iDataType, void* pData, int iDataSize, void* pUser);

	//初始化业务
	CLIENT_API long STDCALL Std_Initialize(void);
	CLIENT_API long STDCALL Std_UnInitialize(void);

	//登录业务
	CLIENT_API long STDCALL Std_Login( const char *Host, long Port,  const char *Username,const char *Password );
	CLIENT_API long STDCALL Std_Logout(long LoginHandle);

	//实时播放
	CLIENT_API long STDCALL Std_StartRealPlay(long LoginHandle,const  char *CameraId, void* PlayWnd, int StreamType, StreamCallbackPF CBF_Stream,  void *UserData );
	CLIENT_API long STDCALL Std_StopRealPlay(long RealHandle);

	//客户端录像
	CLIENT_API long STDCALL Std_StartLocalRecord(long RealHandle,const char *FileName, char *FileExt, long FileExtLen);
	CLIENT_API long STDCALL Std_StopLocalRecord(long RealHandle);

	//抓图
	CLIENT_API long STDCALL Std_Capture(long PlayHandle, const char *PicFile, int PicFormat);

	//云台控制
	CLIENT_API long STDCALL Std_PtzCtrl(long LoginHandle ,const char *CameraId, const char *PtzCmd, int Param1,int Param2,int Param3 );
	CLIENT_API long STDCALL Std_PtzCtrl3D(long LoginHandle,char *CameraId, int xTop,int yTop,int xBottom, int yBottom,int bCounter);

	//打开声音
	CLIENT_API long STDCALL Std_OpenSound(long PlayHandle);
	//关闭声音
	CLIENT_API long STDCALL Std_CloseSound(long PlayHandle);
	//设置系统音量
	CLIENT_API long STDCALL Std_SetVolume(long Volume);
	//获取系统音量
	CLIENT_API long STDCALL Std_GetVolume(long *Volume);
	//远程录像回放
	CLIENT_API long STDCALL Std_RecordFind(long LoginHandle,const char *CameraId, const char *StartTime, const char *EndTime,long StorageDev, RecordFindCallBackPF RecordFindCBF,void* UserData);
	CLIENT_API long STDCALL Std_StreamReplayByTime(long LoginHandle,const char *CameraID,const char *BeginTime, const char *EndTime,long StorageDev, void* playWnd ,StreamCallbackPF CBF_Stream , void* UserData);
	CLIENT_API long STDCALL Std_StreamReplayByFile(long LoginHandle,const char *RecordUrl, void* PlayWnd ,StreamCallbackPF CBF_Stream , void* UserData );
	CLIENT_API long STDCALL Std_StopStreamReplay(long ReplayHandle);
	CLIENT_API long STDCALL Std_StreamReplayControl(long ReplayHandle, int ReplayMode);

	//远程录像下载
	CLIENT_API long STDCALL Std_StartDownloadByTime(long LoginHandle, const char *CameraID, const char *BeginTime, const char *EndTime, long StorageDev,long* DownloadSpeed,const char *filename,char *fileExt, long fileExtLen );
	CLIENT_API long STDCALL Std_StartDownloadByFile(long LoginHandle,long* DownloadSpeed, const char *RecordUrl, const char *filename, char *fileExt, long fileExtLen);
	CLIENT_API long STDCALL Std_StopDownload(long DownloadHandle);
	CLIENT_API long STDCALL Std_GetDownloadPos(long DownloadHandle);

	//本地录像回放
	CLIENT_API long STDCALL Std_StartFileReplay(char *filename, void* PlayWnd,StreamCallbackPF CBF_Stream ,void *UserData);
	CLIENT_API long STDCALL Std_StopFileReplay(long ReplayHandle);
	CLIENT_API long STDCALL Std_FileReplayControl(long ReplayHandle, int ReplayMode);
	CLIENT_API long STDCALL Std_GetFileReplayPos(long ReplayHandle, long *PlayTimed);
	CLIENT_API long STDCALL Std_GetFileTotalTime(long ReplayHandle, long *TotalTime);
	CLIENT_API long STDCALL Std_SetStreamReplayPos(long ReplayHandle,long PlayTimed);
	CLIENT_API long STDCALL Std_FileCut(const char *srcFile,const char *DestFile, long BeginTime, long EndTime);

	//获取资源列表
	CLIENT_API long STDCALL Std_GetResList(long LoginHandle, int ResType, const char *ParentOrgCode, OrgInfoCallBackPF OrgInfoCBF,void* UserData);
	//获取摄像机信息
	CLIENT_API long STDCALL Std_GetCameraInfo(long LoginHandle, const char *CameraId, char *CameraInfo);
	//预置点查询
	CLIENT_API long STDCALL Std_PresetQuery(long LoginHandle,const char *CameraId,char *PresetInfo);
	//获取摄像机离线状态
	CLIENT_API long STDCALL Std_GetCameraStatus(long LoginHandle, const char *CameraIds, char *CameraStatus);
	//获取最近错误描述
	CLIENT_API long STDCALL Std_GetLastError(char *ErrorDesc);
	//获取SDK版本号
	CLIENT_API long STDCALL Std_GetSDKVersion(char *SDKVersion);
	//获取平台版本号
	CLIENT_API long STDCALL Std_GetPlatformVersion(char *PlatformVersion);
	//获取平台能力
	CLIENT_API long STDCALL Std_GetPlatformAbility(long LoginHandle, int AbilityType,char *AbilityXML);

        //设置原始流数据回调
	CLIENT_API long STDCALL Std_SetSteamCBF(long RealHandle, StreamCallbackPF CBF_Stream, void *UserData);

	//设置YUV数据回调
	CLIENT_API long STDCALL Std_SetVideoDataCBF(long RealHandle, VideoData_Stream_PF pfnVideoDataOutput, void *UserData);

	//设置消息回调函数
	CLIENT_API long STDCALL Std_SetMessageCBF(long LoginHandle, fMsgCallback CBF_Message, void *UserData);

	//设置状态推送回调
	CLIENT_API long STDCALL Std_SetStatusCBF(long LoginHandle, fStatusCallback CBF_Status, void *UserData );

	//设置叠加画图回调
	CLIENT_API long STDCALL Std_SetDrawCBF(long RealHandle,fDrawCallBack  CBF_Draw, void *UserData);
        //设置下载数据回调
	CLIENT_API long STDCALL Std_SetDownloadDataCBF(long DownloadHandle, StreamCallbackPF CBF_Stream, void *UserData);

	//订阅报警信息
	CLIENT_API long STDCALL Std_SubscribeAlarm(long LoginHandle, fAlarmCallback CBF_Message, void *UserData);
	//取消订阅报警
	CLIENT_API long STDCALL Std_UnSubscribeAlarm(long LoginHandle);

	CLIENT_API long STDCALL Std_RealStreamTypeSwitch(long RealHandle ,int StreamType);

	//开始对讲
	CLIENT_API long STDCALL Std_StartTalk(long LoginHandle, char *CameraId, fVoiceDataCallBack CBF_VoiceStream, void *UserData);
	//结束对讲
	CLIENT_API long STDCALL Std_StopTalk(long TalkHandle);
	//获取对讲音频类型
	CLIENT_API long STDCALL Std_GetTalkAudioType(long LoginHandle, const char *CameraId, int &iAudioType);
	//发送对讲音频数据
	CLIENT_API long STDCALL Std_SendVoiceData(long TalkHandle, char *pData, unsigned int ilength);
}
#endif //_STD_CLIENT_H_


