/*
 * CRegUserStore.cpp
 *
 *  Created on: 2017年4月6日
 *      Author: chuanjiang.zh
 */

#include "CRegUserStore.h"

CRegUserStore::CRegUserStore()
{
}

CRegUserStore::~CRegUserStore()
{
}


bool CRegUserStore::find(const std::string& name, RegUser& user)
{
	if (!findInMap(name, user))
	{
		return findInStore(name, user);
	}
	return true;
}

bool CRegUserStore::findInStore(const std::string& name, RegUser& user)
{
	return false;
}

void CRegUserStore::add(const RegUser& user)
{
	m_map[user.name] = user;
}

void CRegUserStore::remove(const std::string& name)
{
	m_map.erase(name);
}

bool CRegUserStore::findInMap(const std::string& name, RegUser& user)
{
	bool found = false;
	RegUserMap::iterator it = m_map.find(name);
	if (it != m_map.end())
	{
		user = it->second;
		found = true;
	}
	return found;
}
