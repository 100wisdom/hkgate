/*
 * MediaSessionMap.h
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#ifndef MEDIASESSIONMAP_H_
#define MEDIASESSIONMAP_H_


#include "MediaSession.h"
#include "TMap.h"


class MediaSessionMap : public comn::Map< resip::Data, MediaSessionPtr >
{
public:
	MediaSessionMap();
	virtual ~MediaSessionMap();

	MediaSessionPtr findSession(const std::string& id);

	MediaSessionPtr findSession(const resip::Data& id);

	MediaSessionPtr findSession(const resip::NameAddr& addr);

	MediaSessionPtr removeSession(const std::string& id);

	MediaSessionPtr removeSession(const resip::Data& id);

	MediaSessionPtr removeSession(const resip::NameAddr& addr);

	bool removeSession(MediaSessionPtr& session);

};

#endif /* MEDIASESSIONMAP_H_ */
