/*
 * MediaSession.cpp
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#include "MediaSession.h"


MediaSession::MediaSession(int mediaType):
	m_state(kInit),
	m_caller(),
	m_mediaDevice(),
	m_mediaType(mediaType),
	m_channel(),
	m_talker()
{
}

MediaSession::~MediaSession()
{
}

/**
 * 获取会话状态
 * @return
 */
MediaSession::State MediaSession::getState()
{
	comn::AutoCritSec lock(m_cs);
	return m_state;
}

/**
 * 设置会话状态
 * @param state
 */
void MediaSession::setState(MediaSession::State state)
{
	comn::AutoCritSec lock(m_cs);
	m_state = state;
}

/**
 * 判断会话是否就绪, 即状态  = kReady
 * @return
 */
bool MediaSession::isReady()
{
	comn::AutoCritSec lock(m_cs);
	return (m_state == kReady);
}

/**
 * 获取主呼用户
 * @return
 */
resip::Data MediaSession::getCaller()
{
	comn::AutoCritSec lock(m_cs);
	return m_caller;
}

void MediaSession::setCaller(const resip::Data& caller)
{
	comn::AutoCritSec lock(m_cs);
	m_caller = caller;
}

int MediaSession::getMediaType()
{
	comn::AutoCritSec lock(m_cs);
	return m_mediaType;
}

/**
 * 获取媒体设备
 * @return
 */
av::MediaSystem::Device& MediaSession::getMediaDev()
{
	comn::AutoCritSec lock(m_cs);
	return m_mediaDevice;
}

void MediaSession::setMediaDev(const av::MediaSystem::Device& mixer)
{
	comn::AutoCritSec lock(m_cs);
	m_mediaDevice = mixer;
}

/**
 * 获取媒体源
 * @return
 */
av::MediaChannelPtr MediaSession::getMediaChannel()
{
	comn::AutoCritSec lock(m_cs);
	return m_channel;
}

void MediaSession::setMediaChannel(av::MediaChannelPtr& channel)
{
	comn::AutoCritSec lock(m_cs);
	m_channel = channel;
}

/**
 * 添加用户呼叫
 * @param handle
 */
void MediaSession::addUser(resip::ServerInviteSessionHandle& handle)
{
	comn::AutoCritSec lock(m_cs);
	m_users.push_back(handle);
}

/**
 * 删除用户呼叫
 * @param handle
 * @return
 */
bool MediaSession::removeUser(resip::ServerInviteSessionHandle& handle)
{
	bool found = false;
	comn::AutoCritSec lock(m_cs);
	HandleArray::iterator it = std::find(m_users.begin(), m_users.end(), handle);
	if (it != m_users.end())
	{
		m_users.erase(it);
		found = true;
	}
	return found;
}

/**
 * 移除首个用户呼叫
 * @return
 */
resip::ServerInviteSessionHandle MediaSession::removeUser()
{
	resip::ServerInviteSessionHandle handle;
	comn::AutoCritSec lock(m_cs);
	if (m_users.size() > 0)
	{
		handle = m_users.front();
		m_users.pop_front();
	}
	return handle;
}

/**
 * 判断用户呼叫句柄是否存在?
 * @param handle
 * @return
 */
bool MediaSession::existUser(resip::ServerInviteSessionHandle& handle)
{
	comn::AutoCritSec lock(m_cs);
	HandleArray::iterator it = std::find(m_users.begin(), m_users.end(), handle);
	return (it != m_users.end());
}

size_t MediaSession::getUserCount()
{
	comn::AutoCritSec lock(m_cs);
	return m_users.size();
}

/**
 * 获取所有用户呼叫句柄
 * @return
 */
MediaSession::HandleArray MediaSession::getUsers()
{
	comn::AutoCritSec lock(m_cs);
	return m_users;
}


/**
 * 设置设备呼叫会话句柄
 * @param handle
 */
void MediaSession::setInviteSession(resip::ClientInviteSessionHandle handle)
{
	m_invite = handle;
}

/**
 * 获取设备呼叫会话句柄
 * @return
 */
resip::ClientInviteSessionHandle MediaSession::getInviteSession()
{
	return m_invite;
}

std::string MediaSession::getUri()
{
	return m_mediaDevice.url;
}

void MediaSession::setMediaType(int mediaType)
{
	comn::AutoCritSec lock(m_cs);
	m_mediaType = mediaType;
}

bool MediaSession::hasTalker()
{
	comn::AutoCritSec lock(m_cs);
	return m_talker.isValid();
}

bool MediaSession::setTalker(resip::ServerInviteSessionHandle handle)
{
	comn::AutoCritSec lock(m_cs);
	if (m_talker.isValid())
	{
		return false;
	}

	m_talker = handle;
	return true;
}

void MediaSession::clearTalker(resip::ServerInviteSessionHandle handle)
{
	comn::AutoCritSec lock(m_cs);
	if (handle.getId() == m_talker.getId())
	{
		m_talker = resip::ServerInviteSessionHandle();
	}
}

void MediaSession::clearTalker()
{
	comn::AutoCritSec lock(m_cs);
	m_talker = resip::ServerInviteSessionHandle();
}