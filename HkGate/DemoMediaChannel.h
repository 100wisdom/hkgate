/*
 * DemoMediaChannel.h
 *
 *  Created on: 2017年8月20日
 *      Author: terry
 */

#ifndef DEMOMEDIACHANNEL_H_
#define DEMOMEDIACHANNEL_H_

#include "MediaChannel.h"
#include "PcmFileSource.h"
#include "MediaSourceT.h"

namespace av
{

class DemoMediaChannel : public MediaSourceT<MediaChannel>, public PcmFileSourceSink
{
public:
	DemoMediaChannel();
	virtual ~DemoMediaChannel();

	void setDevice(const std::string& id, const std::string& name);
	//void setFilePath(const std::string& filepath);

	virtual int open(const std::string& url, const std::string& params);
	virtual void close();
	virtual bool isOpen();

	virtual std::string getUrl();

	virtual int startStream();

	virtual int pauseStream();

	virtual void stopStream();

	virtual int forceKeyFrame();


	virtual void onMediaFormat(const MediaFormat& fmt);

	virtual void onMediaPacket(MediaPacketPtr& pkt);

	virtual void onMediaEvent(int event);


	virtual std::string getName();
	virtual std::string getId();

protected:
	virtual void onPcmData(uint8_t* data, size_t size, int64_t pts);

protected:
	std::string	m_id;
	std::string	m_name;

	PcmFileSource	m_source;
	DWORD	m_threadId;

};

}

#endif /* DEMOMEDIACHANNEL_H_ */
