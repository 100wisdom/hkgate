﻿/*
 * RtpRecvState.h
 *
 *  Created on: 2012-3-30
 *      Author: terry
 */

#ifndef RTPRECVSTATE_H_
#define RTPRECVSTATE_H_

#include "BasicType.h"

class RtpRecvState
{
public:
    RtpRecvState();
    ~RtpRecvState();

    uint64_t  m_startTime;
    uint64_t  m_extTime;

    uint32_t  m_ts;
    uint32_t  m_packetCount;


    void clear();

    void update(uint32_t ts);

    uint32_t getCurTime() const;

    uint64_t getTimeOffset() const;

    /// 获取以毫秒为单位的时间
    uint64_t getCurRealTime();

    uint64_t getRealTimeOffset() const;

    void setClock(uint32_t clk);

    uint32_t getClock() const;

private:
    uint32_t  m_clock;

};


#endif /* RTPRECVSTATE_H_ */
