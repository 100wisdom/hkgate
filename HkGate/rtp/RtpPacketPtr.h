/*    file: RtpPacketPtr.h
 *    desc:
 *   
 * created: 2013-09-10 10:01:13
 *  author: zhengchuanjiang
 * company: 
 */ 


#if !defined RTPPACKETPTR_ZHENGCHUANJIANG_
#define RTPPACKETPTR_ZHENGCHUANJIANG_


#include <functional>
////////////////////////////////////////////////////////////////////////////
namespace jrtplib
{
    class RTPPacket;
}

typedef jrtplib::RTPPacket* RTPPacketPtr;

struct RtpPacketOrder : public std::binary_function< RTPPacketPtr, RTPPacketPtr, bool >
{
    bool operator ()(const RTPPacketPtr& l, const RTPPacketPtr& r) const;
};

////////////////////////////////////////////////////////////////////////////
#endif //RTPPACKETPTR_ZHENGCHUANJIANG_

