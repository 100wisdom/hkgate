/*
 * JRTPSession.h
 *
 *  Created: 2011-10-19
 *   Author: terry
 */


#if !defined JRTPSESSION_TERRY_
#define JRTPSESSION_TERRY_

#include "Jrtplib.h"

using namespace jrtplib;
////////////////////////////////////////////////////////////////////////////
class JRTPSession : public RTPSession
{
public:
    explicit JRTPSession(RTPMemoryManager *mgr);

    virtual void OnNewSource(RTPSourceData *dat);
    virtual void OnBYEPacket(RTPSourceData *dat);
    virtual void OnRemoveSource(RTPSourceData *dat);
    virtual void OnTimeout(RTPSourceData *srcdat);

    bool isRtcpTimeout() const;

private:
    bool m_isRtcpTimeout;
};

////////////////////////////////////////////////////////////////////////////
#endif //JRTPSESSION_TERRY_
