/*
 * AudioTranscoder.h
 *
 *  Created on: 2017年4月28日
 *      Author: chuanjiang.zh
 */

#ifndef AUDIOTRANSCODER_H_
#define AUDIOTRANSCODER_H_

#include "BasicType.h"
#include "AudioFormat.h"
#include "Ffmpeg.h"
#include "SharedPtr.h"


namespace av
{

class AudioTranscoder
{
public:
	virtual ~AudioTranscoder() {}

	virtual bool open(const AudioFormat& srcFormat, const AudioFormat& outFormat) =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual bool transcode(AVPacket* inPkt, AVPacket* outPkt) =0;

	virtual AudioFormat getOutFormat() =0;

	//virtual AVCodecContext* getCodecContext() =0;

};


typedef std::shared_ptr< AudioTranscoder >	AudioTranscoderPtr;


} /* namespace av */

#endif /* AUDIOTRANSCODER_H_ */
