/*    file: HKG722.h
 *    desc:
 *   
 * created: 2017-05-05
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined HKG722_H_
#define HKG722_H_

#include "BasicType.h"
#include <Windows.h>
#include "HCNetSDK.h"
#include "ByteBuffer.h"


class HKG722Decoder
{
public:
	static void init();
	static void quit();

	static const int PACKET_SIZE = 80;
	static const int PCM_SIZE = 1280;

public:
	HKG722Decoder();
	~HKG722Decoder();

	bool open();
	void close();
	bool isOpen();

	int decode(unsigned char* inbuf, int size, unsigned char* outbuf);

protected:
	int decodeFrame(unsigned char* inbuf, int size, unsigned char* outbuf);

	int flushBuffer(unsigned char*& inbuf, int& size, unsigned char* outbuf);

protected:
	void*	m_handle;
	ByteBuffer	m_buffer;

};

/**
将 16K 16位 单通道音频编码为G722
*/
class HKG722Encoder
{
public:
	HKG722Encoder();
	~HKG722Encoder();

	bool open();
	void close();
	bool isOpen();

	int encode(unsigned char* inbuf, int size, unsigned char* outbuf);

protected:
	int encodeFrame(unsigned char* inbuf, int size, unsigned char* outbuf);

	int flushBuffer(unsigned char*& inbuf, int& size, unsigned char* outbuf);

protected:
	void*	m_handle;
	ByteBuffer	m_buffer;

};


#endif //HKG722_H_

