/*
 * DemoMediaSystem.cpp
 *
 *  Created on: 2017年8月20日
 *      Author: terry
 */

#include "DemoMediaSystem.h"
#include <algorithm>

namespace av
{

DemoMediaSystem::DemoMediaSystem():
	m_sink()
{

	{
		DemoMediaChannelPtr chl(new DemoMediaChannel());
		chl->setDevice("hkgate", "71014200003");
		m_channels.push_back(chl);
	}

	{
		DemoMediaChannelPtr chl(new DemoMediaChannel());
		chl->setDevice("100", "41010901001316158057");
		m_channels.push_back(chl);
	}
}

DemoMediaSystem::~DemoMediaSystem()
{
	close();
}

bool DemoMediaSystem::open()
{
	return true;
}

void DemoMediaSystem::close()
{

}

bool DemoMediaSystem::exist(const std::string& id)
{
	size_t idx = indexOf(id);
	return (idx != std::string::npos);
}

bool DemoMediaSystem::getDevice(const std::string& id, Device& device)
{
	size_t idx = indexOf(id);
	if (idx == std::string::npos)
	{
		return false;
	}

	DemoMediaChannelPtr chl = m_channels[idx];
	device.id = chl->getId();
	device.name = chl->getName();
	device.url = chl->getUrl();
	device.status = true;

	return true;
}

size_t DemoMediaSystem::listDevice(Device* devices, size_t maxSize)
{
	size_t count = std::min(m_channels.size(), maxSize);
	for (size_t i = 0; i < count; ++i)
	{
		DemoMediaChannelPtr chl = m_channels[i];
		Device& device = devices[i];

		device.id = chl->getId();
		device.name = chl->getName();
		device.url = chl->getUrl();
		device.status = true;
	}
	return count;
}

MediaChannelPtr DemoMediaSystem::create(const std::string& id)
{
	size_t idx = indexOf(id);
	if (idx == std::string::npos)
	{
		return MediaChannelPtr();
	}

	DemoMediaChannelPtr chl = m_channels[idx];
	return chl;
}

void DemoMediaSystem::setSink(MediaSystemSink* sink)
{
	m_sink = sink;
}

size_t DemoMediaSystem::indexOf(const std::string& id)
{
	size_t idx = -1;
	for (size_t i = 0; i < m_channels.size(); ++i)
	{
		if (m_channels[i]->getId() == id)
		{
			idx = i;
			break;
		}
	}
	return idx;
}


}
