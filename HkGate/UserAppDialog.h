/*
 * UserAppDialog.h
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#ifndef USERAPPDIALOG_H_
#define USERAPPDIALOG_H_

#include "BasicType.h"

#include <resip/dum/Handles.hxx>
#include <rutil/Data.hxx>
#include <resip/dum/Handles.hxx>
#include <resip/dum/AppDialog.hxx>
#include <resip/dum/DialogUsageManager.hxx>


#include "RtpMedium.h"
#include "RtpMediaChannel.h"
#include "MediaSystem.h"


class UserAppDialog: public resip::AppDialog
{
public:
	UserAppDialog(resip::DialogUsageManager& dum, const resip::SipMessage& msg);
	virtual ~UserAppDialog();

	av::MediaSystem::Device	m_device;

	av::RtpMediaChannelPtr	m_audioChannel;
	av::RtpMediaChannelPtr 	m_videoChannel;

	RtpMedium	m_audioMedium;
	RtpMedium	m_videoMedium;

	void setupAudio(av::MediaChannelPtr& mediaChannel, const std::string& localIP, int localPort);

	void setupVideo(av::MediaChannelPtr& mediaChannel, const std::string& localIP, int localPort);

	bool isMediaReady();

	void startCast();
	void stopCast();

	bool canTalk();
	void canTalk(bool enabled);

protected:
	bool	m_audioReady;
	bool	m_videoReady;
	bool	m_canTalk;

};

#endif /* USERAPPDIALOG_H_ */
