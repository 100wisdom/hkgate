/*
 * SdpHelper.h
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#ifndef SDPHELPER_H_
#define SDPHELPER_H_

#include "BasicType.h"
#include <string>

#include "Resip.h"
#include "RtpMedium.h"


class SdpHelper
{
public:
	SdpHelper();
	virtual ~SdpHelper();

	static bool findVideo(const resip::SdpContents& contents, RtpMedium& rtpMedium);

	static bool findPcm(const resip::SdpContents& contents, RtpMedium& rtpMedium);



};




#endif /* SDPHELPER_H_ */
