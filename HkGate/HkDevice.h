/*
 * HkDevice.h
 *
 *  Created on: 2017年8月20日
 *      Author: terry
 */

#ifndef HKDEVICE_H_
#define HKDEVICE_H_

#include "MediaSystem.h"

namespace av
{

class HkDevice : public av::MediaSystem::Device
{
public:
	HkDevice();
	virtual ~HkDevice();


};

} /* namespace av */

#endif /* HKDEVICE_H_ */
