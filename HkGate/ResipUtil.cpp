/*
 * ResipUtil.cpp
 *
 *  Created on: 2015年6月4日
 *      Author: chuanjiang.zh@qq.com
 */

#include "ResipUtil.h"
#include <iostream>
#include <sstream>
#include "TStringUtil.h"
#include "TStringCast.h"



ResipUtil::ResipUtil()
{
}

ResipUtil::~ResipUtil()
{
}

resip::Mime ResipUtil::makeMime(const std::string& mime)
{
    std::string type;
    std::string subtype;
    comn::StringUtil::split(mime, '/', type, subtype);
    return resip::Mime(type.c_str(), subtype.c_str());
}

void ResipUtil::setMime(resip::SipMessage& msg, const std::string& mime)
{
	resip::Mime& mimeType = msg.header(resip::h_ContentType);
    mimeType = makeMime(mime);
}

std::string ResipUtil::getMime(const resip::SipMessage& msg)
{
    const resip::Mime& mimeType = msg.header(resip::h_ContentType);
    return comn::StringCast::toString(mimeType);
}





