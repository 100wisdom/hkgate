/*    file: PcmFileSource.h
 *    desc:
 *   
 * created: 2017-04-15
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined PCMFILESOURCE_H_
#define PCMFILESOURCE_H_

#include "BasicType.h"
#include "TThread.h"
#include "TEvent.h"
#include <stdio.h>
#include <string>


class PcmFileSourceSink
{
public:
	virtual ~PcmFileSourceSink() {}

	virtual void onPcmData(uint8_t* data, size_t size, int64_t pts) = 0;
};


class PcmFileSource : public comn::Thread
{
public:
	PcmFileSource();
	~PcmFileSource();

	void setSink(PcmFileSourceSink* sink);

	bool open(const char* filename);
	
	void close();
	
	bool isOpen();

	void setPacketSize(int size);

public:

protected:
	virtual int run();
	virtual void doStop();

protected:
	comn::Event	m_event;
	PcmFileSourceSink*	m_sink;
	
	FILE*	m_file;
	std::string	m_filename;
	int64_t	m_pts;
	int	m_packetSize;

};


#endif //PCMFILESOURCE_H_

