/*
 * resip.h
 *
 *  Created on: 2015年7月14日
 *      Author: terry
 */

#ifndef VGS_RESIP_H_
#define VGS_RESIP_H_


#include "resip/stack/SipStack.hxx"
#include "resip/stack/SipMessage.hxx"

#include "resip/dum/DialogUsageManager.hxx"
#include "resip/dum/UserProfile.hxx"

#include "rutil/Log.hxx"
#include "rutil/Logger.hxx"
#include "rutil/Subsystem.hxx"
#include "rutil/Mutex.hxx"


//using namespace resip;



#endif /* VGS_RESIP_H_ */
