/*
 * RtpPackagerFactory.cpp
 *
 *  Created on: 2015年7月19日
 *      Author: terry
 */

#include "H264RtpPackager.h"
#include "RtpPackagerFactory.h"
#include "CRtpPackager.h"
#include "PSRtpPackager.h"


namespace av
{

RtpPackagerFactory::RtpPackagerFactory()
{
}

RtpPackagerFactory::~RtpPackagerFactory()
{
}

RtpPackager* RtpPackagerFactory::create(const std::string& codec)
{
	if (codec == "H264")
	{
		return new H264RtpPackager();
	}
	else if (codec == "PS")
	{
        return new PSRtpPackager();
	}

	return new CRtpPackager();
}


} /* namespace av */
