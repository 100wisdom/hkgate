/*
 * AudioFormatUtil.h
 *
 *  Created on: 2017年4月29日
 *      Author: chuanjiang.zh
 */

#ifndef MEDIA_AUDIOFORMATUTIL_H_
#define MEDIA_AUDIOFORMATUTIL_H_

#include "AudioFormat.h"
#include "MediaFormat.h"


namespace av
{

class AudioFormatUtil
{
public:
	AudioFormatUtil();
	virtual ~AudioFormatUtil();

	static AudioFormat from(const MediaFormat& fmt);

};

} /* namespace av */

#endif /* MEDIA_AUDIOFORMATUTIL_H_ */
