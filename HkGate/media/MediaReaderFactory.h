/*
 * MediaReaderFactory.h
 *
 *  Created on: 2016年9月23日
 *      Author: zhengboyuan
 */

#ifndef MEDIAREADERFACTORY_H_
#define MEDIAREADERFACTORY_H_

#include "MediaReader.h"

namespace av
{

class MediaReaderFactory
{
public:
	virtual ~MediaReaderFactory() {}

	virtual MediaReader* create(const std::string& url, const std::string& params) =0;

	virtual void destroy(MediaReader* reader) =0;
};



typedef std::shared_ptr< MediaReaderFactory >		MediaReaderFactoryPtr;

struct MediaSourceDeleter
{
	explicit MediaSourceDeleter(MediaReaderFactoryPtr& factory):
		m_factory(factory)
	{
	}

	void operator ()(MediaReader* reader)
	{
		if (m_factory)
		{
			m_factory->destroy(reader);
		}
	}

	MediaReaderFactoryPtr	m_factory;

};



} /* namespace av */

#endif /* MEDIAREADERFACTORY_H_ */
