/*
 * CodecName.h
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#ifndef CODECNAME_H_
#define CODECNAME_H_

#include "MediaType.h"
#include <string>

namespace av
{

class CodecName
{
public:
	CodecName();
	virtual ~CodecName();

	static const char* getName(int codec);

	static int parse(const std::string& name);

};


} /* namespace av */

#endif /* CODECNAME_H_ */
