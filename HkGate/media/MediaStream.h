/*
 * MediaStream.h
 *
 *  Created on: 2016年1月12日
 *      Author: terry
 */

#ifndef MEDIASTREAM_H_
#define MEDIASTREAM_H_

#include "MediaFormat.h"
#include "MediaPacket.h"
#include "SharedPtr.h"
#include <string>


namespace av
{


class MediaSink
{
public:
    virtual ~MediaSink() {}

    virtual void onMediaFormat(const MediaFormat& fmt) =0;

    virtual void onMediaPacket(MediaPacketPtr& pkt) =0;

    virtual void onMediaEvent(int event) =0;

};

typedef std::shared_ptr< MediaSink >        MediaSinkPtr;



class MediaSource
{
public:
	virtual ~MediaSource() {}

	virtual int open(const std::string& url, const std::string& params) =0;
	virtual void close() =0;
	virtual bool isOpen() =0;

	virtual std::string getUrl() =0;

	virtual bool getMediaFormat(MediaFormat& fmt) =0;

	virtual int startStream() =0;

	virtual int pauseStream() =0;

	virtual void stopStream() =0;

	virtual int getState() =0;

	virtual int addSink(MediaSinkPtr pSink) =0;

	virtual void removeSink(MediaSinkPtr pSink) =0;

	virtual void removeSinks() =0;

	virtual size_t getSinkCount() =0;

	virtual int forceKeyFrame() =0;

};


typedef std::shared_ptr< MediaSource >		MediaSourcePtr;





} /* namespace av */

#endif /* MEDIASTREAM_H_ */
