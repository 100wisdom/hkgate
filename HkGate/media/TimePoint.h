/*
 * TimePoint.h
 *
 *  Created on: 2016年4月19日
 *      Author: terry
 */

#ifndef TIMEPOINT_H_
#define TIMEPOINT_H_

#include "BasicType.h"

namespace av
{

class TimePoint
{
public:
	TimePoint();
	virtual ~TimePoint();

	TimePoint(int64_t clock, int64_t pts);

	int64_t m_clock;
	int64_t m_pts;

	void reset(int64_t clock, int64_t pts);
	void reset(const TimePoint& tp);

	void reset();

	bool isSet() const;

	int64_t getOffset(int64_t clock, int64_t pts) const;

	int64_t getOffset(const TimePoint& pt) const;

	int64_t getOffset(int64_t clock, int64_t pts, float scale) const;

	int64_t getOffset(const TimePoint& pt, float scale) const;


	int64_t getClockDiff(const TimePoint& pt) const;

	int64_t getClockDiff(int64_t clock) const;

	void rescale(int num, int den);

};


} /* namespace av */

#endif /* TIMEPOINT_H_ */
