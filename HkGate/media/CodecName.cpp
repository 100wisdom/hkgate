/*
 * CodecName.cpp
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#include "CodecName.h"

namespace av
{

struct NameEntry
{
    int codec;
    const char* name;
};

static NameEntry s_nameEntry[] = {
	{ MEDIA_CODEC_H264, "H264"},
	{ MEDIA_CODEC_G711U, "PCMU"},
	{ MEDIA_CODEC_G711A, "PCMA"},
	{ MEDIA_CODEC_G722, "G722" },
	{ MEDIA_CODEC_AAC, "AAC"},
	{ MEDIA_CODEC_RAW, "RAW"},
	{ MEDIA_CODEC_HK, "HK"},
	{ MEDIA_CODEC_DHAV, "DH"}
};



CodecName::CodecName()
{
}

CodecName::~CodecName()
{
}

const char* CodecName::getName(int codec)
{
	size_t count = sizeof(s_nameEntry)/sizeof(s_nameEntry[0]);
	for (size_t i = 0; i < count; i ++)
	{
		if (codec == s_nameEntry[i].codec)
		{
			return s_nameEntry[i].name;
		}
	}
	return "";
}

int CodecName::parse(const std::string& name)
{
	size_t count = sizeof(s_nameEntry)/sizeof(s_nameEntry[0]);
	for (size_t i = 0; i < count; i ++)
	{
		if (name == s_nameEntry[i].name)
		{
			return s_nameEntry[i].codec;
		}
	}
	return MEDIA_CODEC_NONE;
}



} /* namespace av */
