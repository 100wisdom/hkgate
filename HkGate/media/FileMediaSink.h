/*
 * FileMediaSink.h
 *
 *  Created on: 2017年2月2日
 *      Author: zhengboyuan
 */

#ifndef MEDIA_FILEMEDIASINK_H_
#define MEDIA_FILEMEDIASINK_H_

#include "MediaStream.h"


namespace av
{

class FileMediaSink: public MediaSink
{
public:
	virtual ~FileMediaSink() {}

    virtual bool open(const std::string& filepath) =0;
    virtual void close() =0;
    virtual bool isOpen() =0;

	virtual std::string getFile() =0;

	virtual int setFile(const std::string& filepath) =0;

};


typedef std::shared_ptr< FileMediaSink >	FileMediaSinkPtr;


} /* namespace av */

#endif /* MEDIA_FILEMEDIASINK_H_ */
