/*
 * PSAnalyzer.h
 *
 *  Created on: 2015年12月23日
 *      Author: terry
 */

#ifndef PSANALYZER_H_
#define PSANALYZER_H_

#include "RtpPackager.h"
#include "PSType.h"


namespace av
{

/**
 * PS 流编码格式分析器
 * 从PS流中识别出编码格式和封装格式以及厂商
 */
class PSAnalyzer
{
public:
	PSAnalyzer();
	virtual ~PSAnalyzer();

	/**
	 * 分析rtp包
	 * @param pkt
	 * @return true 表示分析成功
	 */
	bool analyze(const RtpPacket& pkt);

	/**
	 * 获取提供商
	 * @return
	 */
	PSType::Provider getProvider() const;

	/**
	 * 获取包格式
	 * @return
	 */
	PSType::PacketFormat getPacketFormat() const;

	/**
	 * 获取编码格式
	 * @return
	 */
	MediaCodec getCodec();

	/**
	 * 是否就绪
	 * 如果就绪, 就可以调用 get系列函数获取流的格式
	 * @return
	 */
	bool isReady();

	/**
	 * 重置
	 */
	void reset();

protected:
	bool parseVideoPES(const RtpPacket& pkt, MediaCodec& codec);
	void setIfNone(PSType::Provider& provider, PSType::Provider value);
	bool parsePSPacket(const RtpPacket& pkt, MediaCodec& codec);
	bool parsePSMPacket(const RtpPacket& pkt, MediaCodec& codec);

protected:
	PSType::Provider	m_provider;
	PSType::PacketFormat m_format;
	MediaCodec	m_codec;

};


} /* namespace av */

#endif /* PSANALYZER_H_ */
