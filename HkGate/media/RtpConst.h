/*
 * RtpConst.h
 *
 *  Created on: 2015年7月7日
 *      Author: terry
 */

#ifndef RTPCONST_H_
#define RTPCONST_H_

#include "BasicType.h"


namespace av
{

static const uint8_t	DYNAMIC_PAYLOAD = 96;

static const size_t     MAX_PACKET_SIZE_RECV = 2048;

}



#endif /* RTPCONST_H_ */
