/*
 * CRtpPackager.h
 *
 *  Created on: 2015年7月5日
 *      Author: terry
 */

#ifndef CRTPPACKAGER_H_
#define CRTPPACKAGER_H_

#include "RtpPackager.h"
#include "DoubleByteBuffer.h"


namespace av
{

/**
 * 一般的rtp打包器
 */
class CRtpPackager: public RtpPackager
{
public:
	CRtpPackager();
	virtual ~CRtpPackager();

	explicit CRtpPackager(int type);

    virtual void slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink);

    virtual bool join(const RtpPacket& pktIn, MediaPacket& pktOut);

    virtual void reset();

    int	m_type;

protected:
    DoubleByteBuffer	m_buffer;

};



} /* namespace av */

#endif /* CRTPPACKAGER_H_ */
