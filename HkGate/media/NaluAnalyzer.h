/*
 * NaluAnalyzer.h
 *
 *  Created: 2012-05-10
 *   Author: terry
 */


#if !defined NALUANALYZER_TERRY_
#define NALUANALYZER_TERRY_

#include "BasicType.h"
#include "TIOBuffer.h"
#include "H264NaluParser.h"
////////////////////////////////////////////////////////////////////////////

typedef unsigned char uint8_t;



class NaluAnalyzerSink
{
public:
    virtual ~NaluAnalyzerSink() {}

    virtual void writePacket(NaluPacket& packet) =0;

};



class NaluAnalyzer
{
public:
    NaluAnalyzer();
    ~NaluAnalyzer();

    bool inputData(const uint8_t* buffer, size_t length);

    void setSink(NaluAnalyzerSink* pSink);

    void clear();

protected:
    void onNewPacket(NaluPacket& packet);
    void writePacket(NaluPacket& packet);
    void flushCompoundBuffer();

    bool findNalu(uint8_t* buffer, size_t length, size_t start, NaluPacket& packet);

private:
    NaluAnalyzerSink*   m_pSink;
    comn::IOBuffer    m_buffer;

    uint8_t m_lastType;
    comn::IOBuffer    m_compoundBuffer;

};

////////////////////////////////////////////////////////////////////////////
#endif //NALUANALYZER_TERRY_
