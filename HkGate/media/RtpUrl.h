/*
 * RtpUrl.h
 *
 *  Created on: 2017年4月6日
 *      Author: chuanjiang.zh
 */

#ifndef RTPURL_H_
#define RTPURL_H_

#include "BasicType.h"
#include <string>

namespace av
{

class RtpUrl
{
public:
	RtpUrl();
	virtual ~RtpUrl();

	static std::string makeUrl(const std::string& ip, int port);

	static bool parse(const std::string& url, std::string& ip, int& port);


};


} /* namespace av */

#endif /* RTPURL_H_ */
