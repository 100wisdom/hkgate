/*
 * TimePoint.cpp
 *
 *  Created on: 2016年4月19日
 *      Author: terry
 */

#include "TimePoint.h"
#include <stdlib.h>
#include "Ffmpeg.h"


namespace av
{



TimePoint::TimePoint():
	m_clock(),
    m_pts()
{
}

TimePoint::~TimePoint()
{
}

TimePoint::TimePoint(int64_t clock, int64_t pts):
		m_clock(clock),
        m_pts(pts)
{

}


void TimePoint::reset(int64_t clock, int64_t pts)
{
    m_pts = pts;
    m_clock = clock;
}

void TimePoint::reset(const TimePoint& tp)
{
    m_pts = tp.m_pts;
    m_clock = tp.m_clock;
}

void TimePoint::reset()
{
    m_pts = 0;
    m_clock = 0;
}

bool TimePoint::isSet() const
{
    return (m_clock != 0) && (m_pts != 0);
}


int64_t TimePoint::getOffset(int64_t clock, int64_t pts) const
{
    return (m_pts - pts) - (m_clock - clock);
}

int64_t TimePoint::getOffset(const TimePoint& pt) const
{
    return getOffset(pt.m_clock, pt.m_pts);
}

int64_t TimePoint::getOffset(int64_t clock, int64_t pts, float scale) const
{
    return (int64_t)((m_pts - pts) / scale) - (m_clock - clock);
}

int64_t TimePoint::getOffset(const TimePoint& pt, float scale) const
{
    return getOffset(pt.m_clock, pt.m_pts, scale);
}


int64_t TimePoint::getClockDiff(const TimePoint& pt) const
{
    return getClockDiff(pt.m_clock);
}

int64_t TimePoint::getClockDiff(int64_t clock) const
{
    return abs((int)(m_clock - clock));
}

void TimePoint::rescale(int num, int den)
{
	m_clock = av_rescale(m_clock, num, den);
	m_pts = av_rescale(m_pts, num, den);
}



} /* namespace av */
