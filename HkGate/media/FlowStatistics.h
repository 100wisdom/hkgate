/*    file: FlowStatistics.h
 *    desc:
 *   
 * created: 2015-11-15
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined FLOWSTATISTICS_H_
#define FLOWSTATISTICS_H_

#include "BasicType.h"

////////////////////////////////////////////////////////////////////////////

/// 流量统计
struct FlowStatistics
{
	/// 带宽
    double  bandwidth;

    /// 字节数
    int64_t bytes;

    /// 包数
    int64_t packetCount;

    /// 丢失的包数
    int64_t lostCount;
};

////////////////////////////////////////////////////////////////////////////
#endif //FLOWSTATISTICS_H_

