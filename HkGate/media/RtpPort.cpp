/*
 * RtpPort.cpp
 *
 *  Created on: 2015年7月6日
 *      Author: terry
 */

#include "RtpPort.h"
#include "NumGenerator.h"
#include "TSingleton.h"


namespace av
{


typedef comn::NumGenerator< unsigned short >    PortGenerator;

typedef comn::Singleton< PortGenerator >    ThePortGenerator;

static void initPortGenerator()
{
    PortGenerator& gen = ThePortGenerator::instance();
    if (gen.getStep() == 1)
    {
        gen.setStep(2);
        gen.setRange(12000, 20000);
    }
}


RtpPort::RtpPort()
{
}

RtpPort::~RtpPort()
{
}

void RtpPort::setRange(uint16_t minPort, uint16_t maxPort)
{
    PortGenerator& gen = ThePortGenerator::instance();
    if (gen.getStep() == 1)
    {
        gen.setStep(2);
    }

	gen.setRange(minPort, maxPort);
}

uint16_t RtpPort::make()
{
	initPortGenerator();

	return ThePortGenerator::instance().next();
}

size_t RtpPort::count()
{
	PortGenerator& gen = ThePortGenerator::instance();
	return gen.max_size() / 2;
}



} /* namespace av */
