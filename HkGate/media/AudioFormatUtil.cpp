/*
 * AudioFormatUtil.cpp
 *
 *  Created on: 2017年4月29日
 *      Author: chuanjiang.zh
 */

#include <AudioFormatUtil.h>

namespace av
{

AudioFormatUtil::AudioFormatUtil()
{
}

AudioFormatUtil::~AudioFormatUtil()
{
}

AudioFormat AudioFormatUtil::from(const MediaFormat& fmt)
{
	AudioFormat audioFormat;
	audioFormat.m_codec = fmt.m_audioCodec;
	audioFormat.m_channels = fmt.m_channels;
	audioFormat.m_samplerate = fmt.m_sampleRate;
	audioFormat.m_config = fmt.m_audioConfig;

	return audioFormat;
}



} /* namespace av */
