/*
 * PcmRtpPackager.h
 *
 *  Created on: 2016年9月24日
 *      Author: zhengboyuan
 */

#ifndef PCMRTPPACKAGER_H_
#define PCMRTPPACKAGER_H_

#include "RtpPackager.h"
#include "DoubleByteBuffer.h"


namespace av
{

class PcmRtpPackager: public RtpPackager
{
public:
	PcmRtpPackager();
	virtual ~PcmRtpPackager();

    virtual void slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink);

    virtual bool join(const RtpPacket& pktIn, MediaPacket& pktOut);

    virtual void reset();

protected:
    DoubleByteBuffer	m_buffer;

};

} /* namespace av */

#endif /* PCMRTPPACKAGER_H_ */
