/*
 * RtpPackagerFactory.h
 *
 *  Created on: 2015年7月19日
 *      Author: terry
 */

#ifndef RTPPACKAGERFACTORY_H_
#define RTPPACKAGERFACTORY_H_

#include "RtpPackager.h"
#include <string>


namespace av
{

/**
 * RTP打包器工厂类
 */
class RtpPackagerFactory
{
public:
	RtpPackagerFactory();
	virtual ~RtpPackagerFactory();

	RtpPackager* create(const std::string& codec);

};


} /* namespace av */

#endif /* RTPPACKAGERFACTORY_H_ */
