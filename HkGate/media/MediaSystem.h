/*
 * MediaSystem.h
 *
 *  Created on: 2017年4月6日
 *      Author: chuanjiang.zh
 */

#ifndef MEDIASYSTEM_H_
#define MEDIASYSTEM_H_

#include "MediaChannel.h"


namespace av
{

class MediaSystemSink;

/**
 * 媒体系统
 */
class MediaSystem
{
public:
	/// 媒体设备
	struct Device
	{
		std::string id;
		std::string name;
		std::string url;
		std::string num;
		int	status;

		Device():
			status()
		{
		}

		bool isEmpty() const
		{
			return id.empty();
		}
	};


public:

	virtual ~MediaSystem() {}

	virtual bool open() =0;

	virtual void close() =0;

	/**
	 * 判断设备是否存在
	 * @param id
	 * @return
	 */
	virtual bool exist(const std::string& id) =0;

	/**
	 * 查找设备
	 * @param id
	 * @param device
	 * @return true 表示成功
	 */
	virtual bool getDevice(const std::string& id, Device& device) =0;

	/**
	 * 枚举设备
	 * @param devices	设备数组
	 * @param maxSize	数组最大长度
	 * @return 数组长度
	 */
	virtual size_t listDevice(Device* devices, size_t maxSize) =0;

	/**
	 * 创建设备
	 * @param id
	 * @return
	 */
	virtual MediaChannelPtr create(const std::string& id) =0;

	/**
	 * 设置回调
	 * @param sink
	 */
	virtual void setSink(MediaSystemSink* sink) =0;

};

typedef std::shared_ptr< MediaSystem >	MediaSystemPtr;


class MediaSystemSink
{
public:
	virtual ~MediaSystemSink() {}

	virtual void onDeviceStatus(const MediaSystem::Device& device) =0;

};



} /* namespace av */

#endif /* MEDIASYSTEM_H_ */
