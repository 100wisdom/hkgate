/*
 * PSType.h
 *
 *  Created on: 2015年12月23日
 *      Author: terry
 */

#ifndef PSTYPE_H_
#define PSTYPE_H_


namespace  av
{

class PSType
{
public:
	/// 提供商
	enum Provider
	{
		kProviderNone = 0,
		kProviderHK,
		kProviderDahua
	};

	/// 包封装格式
	enum PacketFormat
	{
		kUnknownFormat,		/// 未知格式
		kFormalFormat,		/// 合规格式
		kDahuaFormat,		/// 大华ES流格式
        kDhavFormat         /// 大华私有流
	};

	/**
	 * 获取提供商名称
	 * @param provider
	 * @return
	 */
	static const char* toName(Provider provider)
	{
		if (provider == kProviderHK)
		{
			return "hk";
		}
		else if (provider == kProviderDahua)
		{
			return "dahua";
		}
		return "none";
	}

	/**
	 * 获取包封装格式名称
	 * @param fmt
	 * @return
	 */
	static const char* toName(PacketFormat fmt)
	{
		if (fmt == kFormalFormat)
		{
			return "formal";
		}
		else if (fmt == kDahuaFormat)
		{
			return "dahua";
		}
        else if (fmt == kDhavFormat)
        {
            return "dhav";
        }
		return "unknown";
	}
};


}


#endif /* PSTYPE_H_ */
