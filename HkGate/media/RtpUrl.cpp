/*
 * RtpUrl.cpp
 *
 *  Created on: 2017年4月6日
 *      Author: chuanjiang.zh
 */

#include "RtpUrl.h"
#include "TStringUtil.h"

namespace av
{

RtpUrl::RtpUrl()
{
}

RtpUrl::~RtpUrl()
{
}

std::string RtpUrl::makeUrl(const std::string& ip, int port)
{
	return comn::StringUtil::format("%s:%d", ip.c_str(), port);
}

bool RtpUrl::parse(const std::string& url, std::string& ip, int& port)
{
	size_t count = comn::StringUtil::split(url, ':', ip, port);
	return (count > 1);
}

} /* namespace av */
