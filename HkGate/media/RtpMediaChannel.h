/*
 * RtpMediaChannel.h
 *
 *  Created on: 2017年4月6日
 *      Author: chuanjiang.zh
 */

#ifndef RTPMEDIACHANNEL_H_
#define RTPMEDIACHANNEL_H_

#include "MediaChannel.h"
#include "NetAddress.h"

namespace av
{


class RtpMediaChannel: public MediaChannel
{
public:
	RtpMediaChannel();
	virtual ~RtpMediaChannel();

	virtual NetAddress getLocalAddr() =0;

	virtual void setMaxPacetSize(size_t size) =0;

    virtual void setPayload(uint8_t payload) =0;

    virtual uint8_t getPayload() =0;

    virtual void setMediaType(int mediaType) =0;

    virtual int getMediaType() =0;


    virtual void setPeerAddr(const std::string& ip, int port) =0;

	virtual NetAddress getPeerAddr() =0;

	virtual void setPeerMedium(int payload, int codec, int clockrate, const std::string& sprop) =0;


};


typedef std::shared_ptr< RtpMediaChannel >	RtpMediaChannelPtr;


} /* namespace av */

#endif /* RTPMEDIACHANNEL_H_ */
