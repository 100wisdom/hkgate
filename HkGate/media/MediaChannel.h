/*
 * MediaChannel.h
 *
 *  Created on: 2017年4月6日
 *      Author: chuanjiang.zh
 */

#ifndef MEDIACHANNEL_H_
#define MEDIACHANNEL_H_

#include "MediaStream.h"

namespace av
{


class MediaChannel : public MediaSink , public MediaSource
{
public:
	virtual ~MediaChannel() {}

	virtual std::string getName() =0;

	virtual std::string getId()
	{
		return getUrl();
	}

};


typedef std::shared_ptr< MediaChannel >		MediaChannelPtr;


} /* namespace av */

#endif /* MEDIACHANNEL_H_ */
