/*
 * PcmRtpPackager.cpp
 *
 *  Created on: 2016年9月24日
 *      Author: zhengboyuan
 */

#include "PcmRtpPackager.h"

namespace av
{

PcmRtpPackager::PcmRtpPackager()
{
}

PcmRtpPackager::~PcmRtpPackager()
{
}


void PcmRtpPackager::slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink)
{
	if (!pSink)
	{
		return;
	}

	if (pkt.size() <= maxSize)
	{
		RtpPacket pktOut;
		pktOut.data = pkt.data();
		pktOut.size = pkt.size();
		pktOut.ts = (uint32_t)pkt.dts();

		pktOut.mark = false;

		pSink->onSlicePacket(pktOut);
	}
	else
	{
		RtpPacket pktOut;
		pktOut.ts = (uint32_t)pkt.dts();

		uint8_t* p = pkt.data();
		int leftSize = pkt.size();
		while (leftSize > maxSize)
		{
			pktOut.data = p;
			pktOut.size = maxSize;
			pktOut.mark = false;

			p += maxSize;
			leftSize -= maxSize;

			pSink->onSlicePacket(pktOut);
		}

		pktOut.data = p;
		pktOut.size = leftSize;
		pktOut.mark = true;
		pSink->onSlicePacket(pktOut);

	}
}

bool PcmRtpPackager::join(const RtpPacket& pktIn, MediaPacket& pktOut)
{
	bool found = false;

	comn::ByteBuffer& buffer = m_buffer.getCurBuffer();
	buffer.write(pktIn.data, pktIn.size);

	pktOut.set_type(MEDIA_TYPE_AUDIO);
	pktOut.set_dts(pktIn.ts);
	pktOut.set_duration(0);
	pktOut.setData(buffer.data(), buffer.size());

	m_buffer.switchBuffer();
	m_buffer.getCurBuffer().clear();

	if (pktIn.mark)
	{
		pktOut.set_flags(MEDIA_FLAG_KEY);
	}

	found = true;

	return found;
}

void PcmRtpPackager::reset()
{
    m_buffer.clear();
}



} /* namespace av */
