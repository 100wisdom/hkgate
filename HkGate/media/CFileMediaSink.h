/*
 * CFileMediaSink.h
 *
 *  Created on: 2017年2月2日
 *      Author: zhengboyuan
 */

#ifndef MEDIA_CFILEMEDIASINK_H_
#define MEDIA_CFILEMEDIASINK_H_

#include "FileMediaSink.h"
#include <stdio.h>


namespace av
{

class CFileMediaSink: public FileMediaSink
{
public:
	CFileMediaSink();
	virtual ~CFileMediaSink();

    virtual bool open(const std::string& filepath);
    virtual void close();
    virtual bool isOpen();

	virtual std::string getFile();

	virtual int setFile(const std::string& filepath);

protected:
    virtual void onMediaFormat(const MediaFormat& fmt);

    virtual void onMediaPacket(MediaPacketPtr& pkt);

    virtual void onMediaEvent(int event);


    bool isFileOpen();

protected:
    FILE*	m_file;
    std::string m_filepath;

};

} /* namespace av */

#endif /* MEDIA_CFILEMEDIASINK_H_ */
