/*
 * PSRtpPackager.cpp
 *
 *  Created on: 2015年7月5日
 *      Author: terry
 */

#include "PSRtpPackager.h"

namespace av
{




PSRtpPackager::PSRtpPackager():
    m_lastPktTime()
{
}

PSRtpPackager::~PSRtpPackager()
{
}

void PSRtpPackager::slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink)
{
	/// not used
}

bool PSRtpPackager::join(const RtpPacket& pktIn, MediaPacket& pktOut)
{
	//实现PS流到H.264流的解析

    ProgramStreamParser::StreamPacket parserPkt;
	if (!m_parser.input(pktIn.data, pktIn.size, parserPkt))
	{
		return false;
	}

	pktOut.set_type(MEDIA_TYPE_VIDEO);
	pktOut.setData(const_cast< uint8_t*> (parserPkt.data), parserPkt.length);
	pktOut.set_dts(pktIn.ts);
    pktOut.set_duration(0);

    m_lastPktTime = pktIn.ts;

	if (parserPkt.type == NaluPacket::NALU_SPS)
	{
		pktOut.set_flags(pktOut.flags() | MEDIA_FLAG_KEY);
	}
	else if (parserPkt.type == NaluPacket::NALU_PPS)
	{
		pktOut.set_flags(pktOut.flags() | MEDIA_FLAG_KEY);
	}
	else if (parserPkt.type == NaluPacket::NALU_IFRAME)
	{
		pktOut.set_flags(pktOut.flags() | MEDIA_FLAG_KEY);
	}

	return true;
}

void PSRtpPackager::reset()
{
	/// pass
}




} /* namespace av */
