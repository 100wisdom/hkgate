/*
 * MediaPacket.h
 *
 *  Created on: 2016年1月12日
 *      Author: terry
 */

#ifndef MEDIAPACKET_H_
#define MEDIAPACKET_H_

#include "MediaType.h"
#include "SharedPtr.h"

struct AVPacket;

namespace av
{


class MediaPacket
{
public:
	MediaPacket();
	virtual ~MediaPacket();

	MediaPacket(const MediaPacket& obj);
	MediaPacket& operator = (const MediaPacket& obj);

	void reset();

	MediaPacket clone();

	MediaPacket& clone(const MediaPacket& obj);

	AVPacket* get();

	explicit MediaPacket(AVPacket* avpkt);
	MediaPacket& operator = (AVPacket* avpkt);
	MediaPacket& ref(AVPacket* avpkt);
	MediaPacket& clone(AVPacket* avpkt);

	uint8_t * data() const;
	int size() const;

	int64_t pts() const;
	int64_t dts() const;
	int stream_index() const;
	int flags() const;
	int duration() const;
	int64_t pos() const;

	void set_pts(int64_t val);
	void set_dts(int64_t val);
	void set_stream_index(int val);
	void set_flags(int val);
	void set_duration(int val);
	void set_pos(int64_t val);

	int copy_props(AVPacket* src);
	int copy_props(MediaPacket& src);

	void setData(uint8_t* data, int size);

	int copy(const AVPacket *src);

	int grow(int grow_by);

	void shrink(int size);

	int new_packet(int size);

	void free_packet();

	int type() const;

	void set_type(int val);


	bool isVideo() const;

    bool isKey() const;
    
    bool empty() const;

    bool ensure(size_t size);

    bool copy(uint8_t* data, int size);

protected:
	void cleanup();

protected:
	AVPacket*	m_avpkt;
	int	m_type;

};


typedef 	std::shared_ptr< MediaPacket >  	MediaPacketPtr;



} /* namespace av */

#endif /* MEDIAPACKET_H_ */
