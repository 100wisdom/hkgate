/*
 * NetAddress.h
 *
 *  Created on: 2015年6月27日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef NETADDRESS_H_
#define NETADDRESS_H_

#include "BasicType.h"
#include <string>


namespace av
{

class NetAddress
{
public:
    NetAddress();
    virtual ~NetAddress();

    NetAddress(const std::string& ip, uint16_t port);

    NetAddress(const NetAddress& obj);

    NetAddress& operator = (const NetAddress& obj);

    bool operator == (const NetAddress& obj);

    void set(const std::string& ip, uint16_t port);

	const std::string& ip();

	uint16_t port();

	bool isEmpty() const;

	void clear();

    std::string	m_ip;
    uint16_t	m_port;

};


} /* namespace av */

#endif /* NETADDRESS_H_ */
