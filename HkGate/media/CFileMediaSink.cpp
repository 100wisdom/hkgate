/*
 * CFileMediaSink.cpp
 *
 *  Created on: 2017年2月2日
 *      Author: zhengboyuan
 */

#include "CFileMediaSink.h"

namespace av
{


CFileMediaSink::CFileMediaSink():
		m_file()
{
}

CFileMediaSink::~CFileMediaSink()
{
	close();
}

bool CFileMediaSink::open(const std::string& filepath)
{
	m_filepath = filepath;
	m_file = fopen(filepath.c_str(), "wb");
	return (m_file != NULL);
}

void CFileMediaSink::close()
{
	if (m_file)
	{
		fclose(m_file);
		m_file = NULL;
	}
}

bool CFileMediaSink::isOpen()
{
	return isFileOpen();
}

std::string CFileMediaSink::getFile()
{
	return m_filepath;
}

int CFileMediaSink::setFile(const std::string& filepath)
{
	return 0;
}

void CFileMediaSink::onMediaFormat(const MediaFormat& fmt)
{
	// pass
}

void CFileMediaSink::onMediaPacket(MediaPacketPtr& pkt)
{
	if (!isFileOpen())
	{
		return;
	}

	fwrite(pkt->data(), 1, pkt->size(), m_file);
	fflush(m_file);
}

void CFileMediaSink::onMediaEvent(int event)
{
	// pass
}

bool CFileMediaSink::isFileOpen()
{
	return (m_file != NULL);
}



} /* namespace av */
