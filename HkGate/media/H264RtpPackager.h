/*
 * H264RtpPackager.h
 *
 *  Created on: 2015年6月2日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef H264RTPPACKAGER_H_
#define H264RTPPACKAGER_H_

#include "RtpPackager.h"
#include "TByteBuffer.h"

namespace av
{

/**
 * H264 ES流打包器, 即FU-A拆包与解包
 */
class H264RtpPackager: public RtpPackager
{
public:
    H264RtpPackager();
    virtual ~H264RtpPackager();

    virtual void slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink);

    virtual bool join(const RtpPacket& pktIn, MediaPacket& pktOut);

    virtual void reset();

protected:
    void sliceNaluPtr(uint8_t* data, uint8_t* end, uint32_t timestamp, bool mark,
            int maxSize, RtpPackagerSink* pSink);

    void sliceNalu(uint8_t* data, int size, uint32_t timestamp, bool mark,
        int maxSize, RtpPackagerSink* pSink);

    comn::ByteBuffer   m_sliceBuffer;
    comn::ByteBuffer   m_joinBuffer;
    uint32_t    m_lastTime;

};


}

#endif /* H264RTPPACKAGER_H_ */
