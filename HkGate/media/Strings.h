/*
 * Strings.h
 *
 *  Created on: 2017年3月17日
 *      Author: chuanjiang.zh
 */

#ifndef STRINGS_H_
#define STRINGS_H_

#include <string>
#include <vector>
#include <map>


typedef std::vector< std::string >	StringArray;

typedef std::map< std::string, std::string >	StringMap;



#endif /* STRINGS_H_ */
