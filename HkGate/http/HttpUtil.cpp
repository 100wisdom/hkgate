
#include "HttpUtil.h"


bool HttpUtil::startsWith(const mg_str& src, const char* substr)
{
	return startsWith(src, substr, strlen(substr));
}

bool HttpUtil::startsWith(const mg_str& src, const char* substr, size_t len)
{
	if (src.len < len)
	{
		return false;
	}

	int ret = memcmp(src.p, substr, len);
	return ret == 0;
}

bool HttpUtil::getVar(const struct mg_str *buf, const char *name, std::string& value)
{
	bool found = false;
	char text[1024] = {0};
	int ret = mg_get_http_var(buf, name, text, sizeof(text));
	if (ret > 0)
	{
		value = text;
		found = true;
	}
	return found;
}

bool HttpUtil::getVar(const struct mg_str *buf, const char *name, int& value)
{
	bool found = false;
	char text[1024] = {0};
	int ret = mg_get_http_var(buf, name, text, sizeof(text));
	if (ret > 0)
	{
		value = strtol(text, NULL, 10);
		found = true;
	}
	return found;
}


bool HttpUtil::getVar(const struct mg_str *buf, const char *name, double& value)
{
	bool found = false;
	char text[1024] = {0};
	int ret = mg_get_http_var(buf, name, text, sizeof(text));
	if (ret > 0)
	{
		value = strtod(text, NULL);
		found = true;
	}
	return found;
}

bool HttpUtil::getVar(const struct mg_str *buf, const char *name, int64_t& value)
{
	bool found = false;
	char text[1024] = {0};
	int ret = mg_get_http_var(buf, name, text, sizeof(text));
	if (ret > 0)
	{
#if defined(_MSC_VER) && (_MSC_VER < 1900 )
		value = _strtoi64(text, NULL, 10);
#else
		value = strtoll(text, NULL, 10);
#endif //
		found = true;
	}
	return found;
}

bool HttpUtil::getVar(const struct mg_str *buf, const char *name, bool& value)
{
	bool found = false;
	char text[1024] = {0};
	int ret = mg_get_http_var(buf, name, text, sizeof(text));
	if (ret > 0)
	{
		if (mg_ncasecmp(text, "false", strlen("false")) == 0)
		{
			value = false;
		}
		else
		{
			if (isDigit(text, ret))
			{
				int num = strtol(text, NULL, 10);
				value = (num > 0);
			}
			else
			{
				value = false;
			}
		}

		found = true;
	}
	return found;
}

bool HttpUtil::isDigit(const char* str, size_t len)
{
	if ((str == NULL) || (len == 0))
	{
		return false;
	}

	bool result = true;
	for (size_t i = 0; i < len; i ++)
	{
		if (str[0] == '0')
		{
			break;
		}

		if (!::isdigit(str[i]))
		{
			result = false;
			break;
		}
	}
	return result;
}

std::string HttpUtil::optVar(const struct mg_str *buf, const char *name, const char* defValue)
{
	std::string value(defValue);
	getVar(buf, name, value);
	return value;
}

std::string HttpUtil::optVar(const struct mg_str *buf, const char *name, const std::string& defValue)
{
	std::string value(defValue);
	getVar(buf, name, value);
	return value;
}

double HttpUtil::optVar(const struct mg_str *buf, const char *name, double defValue)
{
	double value(defValue);
	getVar(buf, name, value);
	return value;
}

int HttpUtil::optVar(const struct mg_str *buf, const char *name, int defValue)
{
	int value(defValue);
	getVar(buf, name, value);
	return value;
}

bool HttpUtil::optVar(const struct mg_str *buf, const char *name, bool defValue)
{
	bool value(defValue);
	getVar(buf, name, value);
	return value;
}

int64_t HttpUtil::optVar(const struct mg_str *buf, const char *name, int64_t defValue)
{
	int64_t value(defValue);
	getVar(buf, name, value);
	return value;
}

