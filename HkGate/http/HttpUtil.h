
#ifndef HTTPUTIL_H_
#define HTTPUTIL_H_

#include "mongoose.h"
#include <string>


class HttpUtil
{
public:

	static bool startsWith(const mg_str& src, const char* substr);

	static bool startsWith(const mg_str& src, const char* substr, size_t len);


	static bool getVar(const struct mg_str *buf, const char *name, std::string& value);

	static bool getVar(const struct mg_str *buf, const char *name, double& value);

	static bool getVar(const struct mg_str *buf, const char *name, int& value);

	static bool getVar(const struct mg_str *buf, const char *name, bool& value);

	static bool getVar(const struct mg_str *buf, const char *name, int64_t& value);

	static bool isDigit(const char* str, size_t len);


	static std::string optVar(const struct mg_str *buf, const char *name, const char* defValue);

	static std::string optVar(const struct mg_str *buf, const char *name, const std::string& defValue);

	static double optVar(const struct mg_str *buf, const char *name, double defValue);

	static int optVar(const struct mg_str *buf, const char *name, int defValue);

	static bool optVar(const struct mg_str *buf, const char *name, bool defValue);

	static int64_t optVar(const struct mg_str *buf, const char *name, int64_t defValue);

};



#endif //HTTPUTIL_H_
