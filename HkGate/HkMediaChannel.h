/*
 * HkMediaChannel.h
 *
 *  Created on: 2017年8月20日
 *      Author: terry
 */

#ifndef HKMEDIACHANNEL_H_
#define HKMEDIACHANNEL_H_

#include "MediaChannel.h"
#include "MediaSourceT.h"
#include "StdClient.h"
#include "G722Transcoder.h"
#include "G722ToG711Encoder.h"


namespace av
{

class HkMediaChannel : public MediaSourceT< MediaChannel >
{
public:
	HkMediaChannel();
	virtual ~HkMediaChannel();

	void setDevice(long loginId, const std::string& id, const std::string& name);

	virtual int open(const std::string& url, const std::string& params);
	virtual void close();
	virtual bool isOpen();

	virtual std::string getUrl();

	virtual int startStream();

	virtual int pauseStream();

	virtual void stopStream();

	virtual int forceKeyFrame();


	virtual void onMediaFormat(const MediaFormat& fmt);

	virtual void onMediaPacket(MediaPacketPtr& pkt);

	virtual void onMediaEvent(int event);


	virtual std::string getName();
	virtual std::string getId();

	void enableDump(bool enabled);

public:
	void onTalkCallback(int iDataType, void* pData, int iDataSize);

protected:
	bool startAudioData();
	void stopAudioData();

	av::MediaCodec makeOutCodec(int audioType);
	av::MediaCodec toMediaCodec(int audioType);
	
	bool isG711(int audioType);

	void sendVoiceData(MediaPacketPtr& pkt);

protected:
	long	m_loginId;
	std::string	m_id;
	std::string	m_name;

	long	m_speakHandle;
	int64_t	m_audioPts;

	G722Transcoder		m_g722Encoder;
	G722ToG711Encoder	m_g722Decoder;

	bool	m_dumpEnabled;
	int		m_audioType;

	comn::CriticalSection	m_csHandle;

};

}

#endif /* HKMEDIACHANNEL_H_ */
