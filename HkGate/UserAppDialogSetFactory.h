/*
 * UserAppDialogSetFactory.h
 *
 *  Created on: 2016年9月21日
 *      Author: zhengboyuan
 */

#ifndef USERAPPDIALOGSETFACTORY_H_
#define USERAPPDIALOGSETFACTORY_H_

#include <resip/dum/DialogUsageManager.hxx>
#include <resip/dum/AppDialogSetFactory.hxx>

class UserAppDialogSetFactory: public resip::AppDialogSetFactory
{
public:
	UserAppDialogSetFactory();
	virtual ~UserAppDialogSetFactory();

	virtual resip::AppDialogSet* createAppDialogSet(resip::DialogUsageManager& dum,
		            const resip::SipMessage& msg);

};

#endif /* USERAPPDIALOGSETFACTORY_H_ */
