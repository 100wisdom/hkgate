/*
 * AudioFormat.h
 *
 *  Created on: 2017年4月28日
 *      Author: chuanjiang.zh
 */

#ifndef AUDIOFORMAT_H_
#define AUDIOFORMAT_H_

#include "BasicType.h"
#include <string>


namespace av
{

class AudioFormat
{
public:
	AudioFormat():
		m_codec(),
		m_channels(1),
		m_samplerate(8000),
		m_bitrate()
	{
	}

	virtual ~AudioFormat()
	{

	}

	int m_codec;
	int	m_channels;
	int m_samplerate;
	int	m_bitrate;
	std::string m_config;

	void reset()
	{
		m_codec = 0;
		m_channels = 1;
		m_samplerate = 8000;
		m_config.clear();
	}

	bool check() const
	{
		return m_channels > 0 && m_samplerate > 0;
	}

};


} /* namespace av */

#endif /* AUDIOFORMAT_H_ */
