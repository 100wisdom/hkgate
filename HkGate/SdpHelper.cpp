/*
 * SdpHelper.cpp
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#include "SdpHelper.h"
#include "ResipSdp.h"

using namespace resip;

SdpHelper::SdpHelper()
{
}

SdpHelper::~SdpHelper()
{
}

bool SdpHelper::findVideo(const resip::SdpContents& contents, RtpMedium& rtpMedium)
{
	const SdpContents::Session& session = contents.session();
	const SdpContents::Session::Medium* medium = ResipSdp::findMedium(session, "video");
	if (!medium)
	{
		return false;
	}

	const SdpContents::Session::Codec* codec = ResipSdp::findCodec(*medium, "H264");
	if (!codec)
	{
		codec = ResipSdp::findCodec(*medium, "PS");
	}

	return ResipSdp::parse(*medium, codec, rtpMedium);
}

bool SdpHelper::findPcm(const resip::SdpContents& contents, RtpMedium& rtpMedium)
{
	const SdpContents::Session& session = contents.session();
	const SdpContents::Session::Medium* medium = ResipSdp::findMedium(session, "audio");
	if (!medium)
	{
		return false;
	}

	const SdpContents::Session::Codec* codec = ResipSdp::findCodec(*medium, "PCMU");
	if (!codec)
	{
		codec = ResipSdp::findCodec(*medium, "PCMA");
	}

	return ResipSdp::parse(*medium, codec, rtpMedium);
}
