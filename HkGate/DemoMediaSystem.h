/*
 * DemoMediaSystem.h
 *
 *  Created on: 2017年8月20日
 *      Author: terry
 */

#ifndef DEMOMEDIASYSTEM_H_
#define DEMOMEDIASYSTEM_H_

#include "DemoMediaChannel.h"
#include "MediaSystem.h"
#include <vector>


namespace av
{

class DemoMediaSystem : public av::MediaSystem
{
public:
	DemoMediaSystem();
	virtual ~DemoMediaSystem();

	virtual bool open();

	virtual void close();

	/**
	* 判断设备是否存在
	* @param id
	* @return
	*/
	virtual bool exist(const std::string& id);

	/**
	* 查找设备
	* @param id
	* @param device
	* @return true 表示成功
	*/
	virtual bool getDevice(const std::string& id, Device& device);

	/**
	* 枚举设备
	* @param devices	设备数组
	* @param maxSize	数组最大长度
	* @return 数组长度
	*/
	virtual size_t listDevice(Device* devices, size_t maxSize);

	/**
	* 创建设备
	* @param id
	* @return
	*/
	virtual MediaChannelPtr create(const std::string& id);

	/**
	* 设置回调
	* @param sink
	*/
	virtual void setSink(MediaSystemSink* sink);


protected:
	typedef std::shared_ptr< DemoMediaChannel >		DemoMediaChannelPtr;

protected:
	size_t indexOf(const std::string& id);

protected:

	MediaSystemSink*	m_sink;

	std::vector<DemoMediaChannelPtr>	m_channels;

};

}

#endif /* DEMOMEDIASYSTEM_H_ */
