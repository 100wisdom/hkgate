/*
 * MediaSessionMap.cpp
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#include "MediaSessionMap.h"

MediaSessionMap::MediaSessionMap()
{
}

MediaSessionMap::~MediaSessionMap()
{
}

MediaSessionPtr MediaSessionMap::findSession(const std::string& id)
{
	MediaSessionPtr session;
	find(id.c_str(), session);
	return session;
}

MediaSessionPtr MediaSessionMap::findSession(const resip::Data& id)
{
	MediaSessionPtr session;
	find(id, session);
	return session;
}

MediaSessionPtr MediaSessionMap::findSession(const resip::NameAddr& addr)
{
	return findSession(addr.uri().user());
}

MediaSessionPtr MediaSessionMap::removeSession(const std::string& id)
{
	MediaSessionPtr session;
	remove(id.c_str(), session);
	return session;
}

MediaSessionPtr MediaSessionMap::removeSession(const resip::Data& id)
{
	MediaSessionPtr session;
	remove(id, session);
	return session;
}

MediaSessionPtr MediaSessionMap::removeSession(const resip::NameAddr& addr)
{
	return removeSession(addr.uri().user());
}

bool MediaSessionMap::removeSession(MediaSessionPtr& session)
{
	resip::Data key;
	return removeValue(session, key);
}
