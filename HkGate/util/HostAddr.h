/*
 * HostAddr.h
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#ifndef HOSTADDR_H_
#define HOSTADDR_H_

#include "BasicType.h"
#include <string>
#include <vector>


namespace util
{

class HostAddr
{
public:
	typedef std::vector< std::string >	StringArray;

public:
	HostAddr();
	virtual ~HostAddr();

	static std::string getHostAddr();

	static StringArray getHostAddrs();


};



} /* namespace util */

#endif /* HOSTADDR_H_ */
