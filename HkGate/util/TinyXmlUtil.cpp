/*
 * TinyXmlUtil.cpp
 *
 *  Created on: 2015年6月11日
 *      Author: chuanjiang.zh@qq.com
 */

#include "TinyXmlUtil.h"
#include "TStringUtil.h"



TinyXmlUtil::TinyXmlUtil()
{
}

TinyXmlUtil::~TinyXmlUtil()
{
}
namespace xml
{


std::string getRootName(const std::string& xml)
{
    TiXmlDocument doc;
    doc.Parse(xml.c_str());
    TiXmlElement* element = doc.RootElement();
    if (element)
    {
        return element->Value();
    }
    return std::string();
}

TiXmlNode* findByTag(TiXmlNode* node, const std::string& tag)
{
    TiXmlNode* childnode = node->FirstChild();
    while (childnode)
    {
        if (tag == childnode->Value())
        {
            return childnode;
        }

        TiXmlNode* subnode = findByTag(childnode, tag);
        if (subnode)
        {
            return subnode;
        }

        childnode = childnode->NextSibling();
    }
    return NULL;
}

TiXmlNode* ifindByTag(TiXmlNode* node, const std::string& tag)
{
    TiXmlNode* childnode = node->FirstChild();
    while (childnode)
    {
        if (comn::StringUtil::icompare(tag, childnode->Value()) == 0)
        {
            return childnode;
        }

        TiXmlNode* subnode = findByTag(childnode, tag);
        if (subnode)
        {
            return subnode;
        }

        childnode = childnode->NextSibling();
    }
    return NULL;
}

bool findTextByTag(const std::string& xml, const std::string& tag, std::string& text)
{
    bool found = false;
    TiXmlDocument doc;
    doc.Parse(xml.c_str());
    TiXmlNode* childNode = findByTag(&doc, tag);
    if (childNode)
    {
        TiXmlElement* element = childNode->ToElement();
        if (element)
        {
            text = element->GetText();
            found = true;
        }
    }
    return found;
}

std::string findTextByTag(const std::string& xml, const std::string& tag)
{
    std::string value;
    findTextByTag(xml, tag, value);
    return value;
}

bool ifindTextByTag(const std::string& xml, const std::string& tag, std::string& text)
{
    bool found = false;
    TiXmlDocument doc;
    doc.Parse(xml.c_str());
    TiXmlNode* childNode = ifindByTag(&doc, tag);
    if (childNode)
    {
        TiXmlElement* element = childNode->ToElement();
        if (element)
        {
            text = element->GetText();
            found = true;
        }
    }
    return found;
}

std::string ifindTextByTag(const std::string& xml, const std::string& tag)
{
    std::string value;
    ifindTextByTag(xml, tag, value);
    return value;
}

std::string toXml(TiXmlNode* node)
{
    TiXmlPrinter printer;
    node->Accept(&printer);
    return printer.CStr();
}

bool check(TiXmlNode* node, const char* value)
{
    return (node->ValueTStr() == value);
}

bool icheck(TiXmlNode* node, const char* value)
{
    std::string vstr(node->Value());
    return comn::StringUtil::icompare(vstr, value) == 0;
}

bool getValue(TiXmlNode* node, const char* name, std::string& value)
{
    TiXmlNode* subnode = node->FirstChild(name);
    if (!subnode)
    {
        return false;
    }
    TiXmlElement* element = subnode->FirstChildElement();
    if (!element)
    {
        return false;
    }

    if (element->GetText())
    {
        value = element->GetText();
    }
    return true;
}

bool igetValue(TiXmlNode* node, const char* name, std::string& value)
{
    TiXmlNode* subnode = ifindByTag(node, name);
    return getValue(subnode, value);
}

bool getValue(TiXmlNode* node, std::string& value)
{
    TiXmlNode* subNode = node->FirstChild();
    if (!subNode)
    {
        return false;
    }

    value = subNode->Value();
    return true;
}

bool setValue(TiXmlNode* node, const std::string& value)
{
    if (!node)
    {
        return false;
    }

    TiXmlNode* subNode = node->FirstChild();
    if (!subNode)
    {
        return false;
    }

    subNode->SetValue(value.c_str());
    return true;
}

bool setValue(TiXmlNode* node, const char* value)
{
    if (!node)
    {
        return false;
    }

    TiXmlNode* subNode = node->FirstChild();
    if (!subNode)
    {
        return false;
    }

    subNode->SetValue(value);
    return true;
}

bool setValue(TiXmlNode* node, const char* name, const std::string& value)
{
    TiXmlNode* subNode = findByTag(node, name);
    return setValue(subNode, value);
}

bool setValue(TiXmlNode* node, const char* name, const char* value)
{
    TiXmlNode* subNode = findByTag(node, name);
    return setValue(subNode, value);
}




}
