/*
 * TinyXmlUtil.h
 *
 *  Created on: 2015年6月11日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef VGS_TINYXMLUTIL_H_
#define VGS_TINYXMLUTIL_H_

#include "tinyxml.h"
#include <string>
#include <sstream>


class TinyXmlUtil
{
public:
    TinyXmlUtil();
    virtual ~TinyXmlUtil();
};

/**
 * tinyxml 库的辅助函数
 *
 *
 */
namespace xml
{

	/**
	 * 获取跟元素名称
	 * @param xml
	 * @return
	 */
    std::string getRootName(const std::string& xml);

    /**
     * 根据tag查找子节点
     * @param node
     * @param tag
     * @return
     */
    TiXmlNode* findByTag(TiXmlNode* node, const std::string& tag);

    /**
     * 根据tag名称查找子节点, 比较时无关字符大小写
     * @param node
     * @param tag
     * @return
     */
    TiXmlNode* ifindByTag(TiXmlNode* node, const std::string& tag);

    /**
     * 查找指定tag的文本值
     * @param xml
     * @param tag
     * @param text
     * @return
     */
    bool findTextByTag(const std::string& xml, const std::string& tag, std::string& text);

    std::string findTextByTag(const std::string& xml, const std::string& tag);

    /**
     * 查找指定tag的文本值, 比较时无关字符大小写
     * @param xml
     * @param tag
     * @param text
     * @return
     */
    bool ifindTextByTag(const std::string& xml, const std::string& tag, std::string& text);

    std::string ifindTextByTag(const std::string& xml, const std::string& tag);

    /**
     * 节点转换为字符串
     * @param node
     * @return
     */
    std::string toXml(TiXmlNode* node);

    /**
     * 校验节点的值与指定值相等
     * @param node
     * @param value
     * @return
     */
    bool check(TiXmlNode* node, const char* value);

    bool icheck(TiXmlNode* node, const char* value);

    /**
     * 获取指定节点下指定名称的子节点的文本
     * @param node
     * @param name
     * @param value
     * @return
     */
    bool getValue(TiXmlNode* node, const char* name, std::string& value);

    bool igetValue(TiXmlNode* node, const char* name, std::string& value);

    template < class T >
    bool getValue(TiXmlNode* node, const char* name, T& value)
    {
        std::string str;
        if (!getValue(node, name, str))
        {
            return false;
        }
        std::istringstream iss(str);
        iss >> value;
        return true;
    }

    template < class T >
    bool igetValue(TiXmlNode* node, const char* name, T& value)
    {
        std::string str;
        if (!igetValue(node, name, str))
        {
            return false;
        }
        std::istringstream iss(str);
        iss >> value;
        return true;
    }

    bool getValue(TiXmlNode* node, std::string& value);

    template < class T >
    bool getValue(TiXmlNode* node, T& value)
    {
        std::string str;
        if (!getValue(node, str))
        {
            return false;
        }
        std::istringstream iss(str);
        iss >> value;
        return true;
    }

    /**
     * 设置指定节点的文本值
     * @param node
     * @param value
     * @return
     */
    bool setValue(TiXmlNode* node, const std::string& value);
    bool setValue(TiXmlNode* node, const char* value);

    /**
     * 设置指定节点下指定子节点名称的文本值
     * @param node
     * @param name
     * @param value
     * @return
     */
    bool setValue(TiXmlNode* node, const char* name, const std::string& value);
    bool setValue(TiXmlNode* node, const char* name, const char* value);

}


#endif /* VGS_TINYXMLUTIL_H_ */
