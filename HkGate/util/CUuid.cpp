/*
 * CUuid.cpp
 *
 *  Created on: 2017年1月31日
 *      Author: zhengboyuan
 */

#include "CUuid.h"
#include "TStringCast.h"

#ifdef WIN32
	#include <objbase.h>
#else
	#include <uuid/uuid.h>
#endif //WIN32


CUuid::CUuid()
{
}

CUuid::~CUuid()
{
}

#define GUID_LEN 64

std::string CUuid::make()
{

#ifdef WIN32

	char buffer[GUID_LEN] = { 0 };
	GUID guid;
	CoCreateGuid(&guid);

	_snprintf(buffer, sizeof(buffer),
		"%08X%04X%04x%02X%02X%02X%02X%02X%02X%02X%02X",
		guid.Data1, guid.Data2, guid.Data3,
		guid.Data4[0], guid.Data4[1], guid.Data4[2],
		guid.Data4[3], guid.Data4[4], guid.Data4[5],
		guid.Data4[6], guid.Data4[7]);
	return buffer;


#else

    uuid_t uuid;
    uuid_generate(uuid);

    comn::StringCast::toHexString((const char*)&uuid, sizeof(uuid));

#endif //WIN32
}
