/*
 * ByteHelper.cpp
 *
 *  Created on: 2015年12月23日
 *      Author: terry
 */

#include <ByteHelper.h>

namespace av
{

ByteHelper::ByteHelper()
{
}

ByteHelper::~ByteHelper()
{
}

bool ByteHelper::equal(const uint8_t* buf, size_t size, const uint8_t* pat)
{
	for (size_t i = 0; i < size; ++ i)
	{
		if (buf[i] != pat[i])
		{
			return false;
		}
	}
	return true;
}

size_t ByteHelper::find(const uint8_t* buf, size_t size,
		const uint8_t* pat, size_t length)
{
	return find(buf, size, 0, pat, length);
}

size_t ByteHelper::find(const uint8_t* buf, size_t size, size_t start,
		const uint8_t* pat, size_t length)
{
	if (start >= size)
	{
		return -1;
	}

	const uint8_t* data = buf + start;
	size -= start;
	if (size < length)
	{
		return false;
	}

	for (size_t i = 0; i < (size - length); ++ i)
	{
		if (equal(data + i, length, pat))
		{
			return i + start;
		}
	}
	return -1;
}

bool ByteHelper::startsWith(const uint8_t* buf, size_t size,
		const uint8_t* pat, size_t length)
{
	if (size < length)
	{
		return false;
	}

	for (size_t i = 0; i < length; ++ i)
	{
		if (buf[i] != pat[i])
		{
			return false;
		}
	}
	return true;
}





} /* namespace av */
