/*    file: SpsParser.h
 *    desc:
 *   
 * created: 2013-10-27 22:50:43
 *  author: zhengchuanjiang
 * version: 1.0
 * company: 
 */ 


#if !defined SPSPARSER_H_
#define SPSPARSER_H_

#include "BasicType.h"

////////////////////////////////////////////////////////////////////////////

/**
 * SPS解析器, 从SPS中提取分辨率,profile,level
 */
class SpsParser
{
public:
	struct h264_sps_t 
	{
		uint16_t width;
		uint16_t height;
		uint8_t  profile;
		uint8_t  level;
	};

	static bool parse(const uint8_t *buf, int len, h264_sps_t& param);


};

////////////////////////////////////////////////////////////////////////////
#endif //SPSPARSER_H_

