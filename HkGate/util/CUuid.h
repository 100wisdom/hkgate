/*
 * CUuid.h
 *
 *  Created on: 2017年1月31日
 *      Author: zhengboyuan
 */

#ifndef UTIL_CUUID_H_
#define UTIL_CUUID_H_

#include "BasicType.h"
#include <string>



class CUuid
{
public:
	CUuid();
	virtual ~CUuid();

	static std::string make();

};

#endif /* UTIL_CUUID_H_ */
