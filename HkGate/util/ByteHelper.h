/*
 * ByteHelper.h
 *
 *  Created on: 2015年12月23日
 *      Author: terry
 */

#ifndef BYTEHELPER_H_
#define BYTEHELPER_H_

#include "BasicType.h"


namespace av
{

class ByteHelper
{
public:
	ByteHelper();
	virtual ~ByteHelper();

	/**
	 * 判断字节数组是否相等
	 * @param buf
	 * @param size
	 * @param pat
	 * @return
	 */
	static bool equal(const uint8_t* buf, size_t size, const uint8_t* pat);

	/**
	 * 在字节数组中查找子串
	 * @param buf	字节指针
	 * @param size	字节长度
	 * @param pat	子串
	 * @param length	子串长度
	 * @return
	 */
	static size_t find(const uint8_t* buf, size_t size,
			const uint8_t* pat, size_t length);

	/**
	 * 在字节数组中查找子串
	 * @param buf
	 * @param size
	 * @param start
	 * @param pat
	 * @param length
	 * @return
	 */
	static size_t find(const uint8_t* buf, size_t size, size_t start,
			const uint8_t* pat, size_t length);

	/**
	 * 判断字节数组是否以指定的子串开头
	 * @param buf
	 * @param size
	 * @param pat
	 * @param length
	 * @return
	 */
	static bool startsWith(const uint8_t* buf, size_t size,
			const uint8_t* pat, size_t length);


};


} /* namespace av */

#endif /* BYTEHELPER_H_ */
