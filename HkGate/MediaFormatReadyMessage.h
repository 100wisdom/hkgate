/*
 * MediaFormatReadyMessage.h
 *
 *  Created on: 2015年10月27日
 *      Author: terry
 */

#ifndef MEDIAFORMATREADYMESSAGE_H_
#define MEDIAFORMATREADYMESSAGE_H_

#include <resip/dum/DialogUsageManager.hxx>
#include <resip/dum/ExternalMessageBase.hxx>
#include "MediaStream.h"

/**
 * 媒体格式就绪通知消息
 */
class MediaFormatReadyMessage: public resip::ExternalMessageBase
{
public:
	MediaFormatReadyMessage(const resip::Data& uri, const av::MediaFormat& fmt);
	virtual ~MediaFormatReadyMessage();

    virtual resip::Message* clone() const;
    virtual std::ostream& encode(std::ostream& strm) const;
    virtual std::ostream& encodeBrief(std::ostream& strm) const;

    resip::Data m_uri;
    av::MediaFormat m_fmt;

};

#endif /* MEDIAFORMATREADYMESSAGE_H_ */
