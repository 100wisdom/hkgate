/*
 * ResipSdp.h
 *
 *  Created on: 2015年9月9日
 *      Author: terry
 */

#ifndef RESIPSDP_H_
#define RESIPSDP_H_

#include "BasicType.h"
#include <string>
#include "resip/stack/SdpContents.hxx"
#include "RtpMedium.h"


typedef resip::SdpContents::Session 	SdpSession;

/**
 * resip SDP 辅助工具类
 */
class ResipSdp
{
public:
	static const char* 	SENDONLY;
	static const char* 	RECVONLY;
	static const char* 	SENDRECV;
	static const char* 	INACTIVE;

public:
	ResipSdp();
	virtual ~ResipSdp();

	/**
	 * 将sdp对象转化为SDP文本
	 * @param sdp
	 * @return
	 */
	static std::string toString(const resip::SdpContents& sdp);

	/**
	 * 获取媒体流方向
	 * @param medium
	 * @return
	 */
	static std::string getStreamModeText(const SdpSession::Medium& medium);

	/**
	 * 获取媒体流方向
	 * @param medium
	 * @return
	 */
	static int getStreamMode(const SdpSession::Medium& medium);

	/**
	 * 获取SDP媒体的连接
	 * @param medium
	 * @return
	 */
	static SdpSession::Connection getConnection(const SdpSession::Medium& medium);

	/**
	 * 在SDP媒体中查找指定名称的编码器
	 * @param medium
	 * @param name
	 * @return
	 */
	static const SdpSession::Codec* findCodec(const SdpSession::Medium& medium, const char* name);

	static const SdpSession::Codec* findCodec(const SdpSession::Medium& medium, const char* name, const char* fmtp);

	static bool parse(const SdpSession::Medium& medium, const char* codecName, RtpMedium& rtpMedium);

	static bool parse(const SdpSession::Medium& medium, const SdpSession::Codec* codec, RtpMedium& rtpMedium);


	static const SdpSession::Medium* findMedium(const SdpSession& session, const char* name);

	/**
	 * 从SDP媒体中获取连接的地址
	 * @param medium
	 * @return
	 */
	static resip::Data getMediumIP(const SdpSession::Medium& medium);

};


#endif /* RESIPSDP_H_ */
