/*
 * AppConst.cpp
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#include "AppConst.h"


#ifdef WIN32
	const char* AppConst::LOG_FILE     = "hkgate.log";
	const char* AppConst::SIP_LOG_FILE = "sip.log";
#else
	const char* AppConst::LOG_FILE     = "/var/log/hkgate.log";
	const char* AppConst::SIP_LOG_FILE = "/var/log/sip.log";
#endif //


bool AppConst::discard = false;
bool AppConst::TALK_RECV_ENABLED = false;

AppConst::AppConst()
{
}

AppConst::~AppConst()
{
}


