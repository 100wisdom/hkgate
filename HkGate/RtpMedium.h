/*
 * RtpMedium.h
 *
 *  Created on: 2015年7月18日
 *      Author: terry
 */

#ifndef RTPMEDIUM_H_
#define RTPMEDIUM_H_

#include "BasicType.h"
#include <string>


class RtpMedium
{
public:

	enum StreamMode
	{
	    kNone = 0,
	    kRecvOnly = 0x01,
	    kSendonly = 0x02,
	    kSendRecv = 0x04,
	    kInactive
	};

	static const char*	VIDEO;
	static const char*	AUDIO;

	static const char*	H264;
	static const char*	PCMA;
	static const char*	PCMU;


public:
	std::string name;	/// 媒体名称

	std::string ip;		/// 连接地址
	int	port;			/// 连接端口

	int payload;		/// 载体类型
	std::string codec;	/// 编码名称

	int clockRate;		/// 时钟
	int mode;			/// 媒体流方向

	std::string fmtp;	/// H.264 fmtp属性
    int width;			/// 宽
    int height;			/// 高

	RtpMedium():
		port(),
		payload(),
		clockRate(90000),
		mode(),
        width(),
        height()
	{
	}

	bool empty() const
	{
		return (port == 0);
	}

	void setToH264(const std::string& ip, int port, int payload)
	{
		name = VIDEO;
		this->ip = ip;
		this->port = port;
		this->payload = payload;
		this->codec = H264;
		this->clockRate = 90000;
		this->mode = kSendonly;
	}


};


#endif /* RTPMEDIUM_H_ */
