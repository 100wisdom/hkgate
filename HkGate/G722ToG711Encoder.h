/*
 * G722ToG711Encoder.h
 *
 *  Created on: 2017年5月6日
 *      Author: chuanjiang.zh
 */

#ifndef G722TOG711ENCODER_H_
#define G722TOG711ENCODER_H_

#include "AudioTranscoder.h"
#include "AVFramePtr.h"
#include "FfmpegUtil.h"
#include "MediaType.h"
#include "HKG722.h"
#include "ByteBuffer.h"
#include "speex/speex_resampler.h"

namespace av
{

class G722ToG711Encoder: public AudioTranscoder
{
public:
	G722ToG711Encoder();
	virtual ~G722ToG711Encoder();

	virtual bool open(const AudioFormat& srcFormat, const AudioFormat& outFormat);

	virtual void close();

	virtual bool isOpen();

	virtual bool transcode(AVPacket* inPkt, AVPacket* outPkt);

	virtual AudioFormat getOutFormat();

protected:
	int openEncoder(AudioFormat& outFormat);
	void closeEncoder();
	bool isEncoderOpen();

	bool isSameFormat(int fmt, int channels, int sampleRate);

	int resample(AVFrame* inFrame, AVFrame* outFrame);

	int doEncode(AVFrame* frame, AVPacket* pkt);

protected:
	AudioFormat	m_srcFormat;
	AudioFormat	m_outFormat;

	AVCodecContext*	m_context;
	SwrContext*		m_swrContext;

	HKG722Decoder	m_decoder;
	ByteBuffer		m_buffer;

	SpeexResamplerState*	m_resample;

};


} /* namespace av */

#endif /* G722TOG711ENCODER_H_ */
