/*
 * MediaFormatReadySink.cpp
 *
 *  Created on: 2015年10月27日
 *      Author: terry
 */

#include "MediaFormatReadySink.h"
#include "MediaFormatReadyMessage.h"


MediaFormatReadySink::MediaFormatReadySink(resip::DialogUsageManager& dum, const resip::Data& devID):
	m_dum(dum),
	m_devID(devID)
{
}

MediaFormatReadySink::~MediaFormatReadySink()
{
}

void MediaFormatReadySink::onMediaFormat(const av::MediaFormat& fmt)
{
    if (fmt.m_audioCodec != av::MEDIA_CODEC_NONE)
	{
		MediaFormatReadyMessage* msg = new MediaFormatReadyMessage(m_devID, fmt);

		m_dum.post(msg);
	}
}

void MediaFormatReadySink::onMediaPacket(av::MediaPacketPtr& pkt)
{
	/// pass
}

void MediaFormatReadySink::onMediaEvent(int event)
{
	/// pass
}
