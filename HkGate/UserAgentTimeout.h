/*
 * UserAgentTimeout.h
 *
 *  Created on: 2016年1月12日
 *      Author: terry
 */

#ifndef USERAGENTTIMEOUT_H_
#define USERAGENTTIMEOUT_H_

#include "Resip.h"

class SipServer;

class UserAgentTimeout : public resip::DumCommand
{
public:
	UserAgentTimeout(SipServer& userAgent, unsigned int timerId, unsigned int duration, unsigned int seqNumber) :
		mUserAgent(userAgent), mTimerId(timerId), mDuration(duration), mSeqNumber(seqNumber)
	{
	}

	UserAgentTimeout(const UserAgentTimeout& rhs) :
		mUserAgent(rhs.mUserAgent), mTimerId(rhs.mTimerId), mDuration(rhs.mDuration), mSeqNumber(rhs.mSeqNumber),
		mName(rhs.mName)
	{
	}

	~UserAgentTimeout()
	{
	}

	virtual void executeCommand();

	resip::Message* clone() const
	{
		return new UserAgentTimeout(*this);
	}

    EncodeStream& encode(EncodeStream& strm) const
	{
		strm << "UserAgentTimeout: id=" << mTimerId << ", duration=" << mDuration << ", seq=" << mSeqNumber;
		return strm;
	}

	EncodeStream& encodeBrief(EncodeStream& strm) const
	{
		return encode(strm);
	}

	unsigned int id() const
	{
		return mTimerId;
	}

	unsigned int seqNumber() const
	{
		return mSeqNumber;
	}

	unsigned int duration() const
	{
		return mDuration;
	}

	const resip::Data& getName() const
	{
		return mName;
	}

	void setName(const resip::Data& name)
	{
		mName = name;
	}

private:
	SipServer& mUserAgent;
	unsigned int mTimerId;
	unsigned int mDuration;
	unsigned int mSeqNumber;
	resip::Data	mName;

};



#endif /* USERAGENTTIMEOUT_H_ */
