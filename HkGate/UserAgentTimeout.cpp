/*
 * UserAgentTimeout.cpp
 *
 *  Created on: 2016年1月12日
 *      Author: terry
 */

#include "UserAgentTimeout.h"

#include "SipServer.h"


void UserAgentTimeout::executeCommand()
{
	mUserAgent.onApplicationTimer(mTimerId, mDuration, mSeqNumber, mName);
}

