/*
 * MediaFormatReadyMessage.cpp
 *
 *  Created on: 2015年10月27日
 *      Author: terry
 */

#include "MediaFormatReadyMessage.h"

MediaFormatReadyMessage::MediaFormatReadyMessage(const resip::Data& uri, const av::MediaFormat& fmt):
    m_uri(uri),
    m_fmt(fmt)
{
}

MediaFormatReadyMessage::~MediaFormatReadyMessage()
{
}

resip::Message* MediaFormatReadyMessage::clone() const
{
    return new MediaFormatReadyMessage(m_uri, m_fmt);
}

std::ostream& MediaFormatReadyMessage::encode(std::ostream& strm) const
{
    return strm;
}

std::ostream& MediaFormatReadyMessage::encodeBrief(std::ostream& strm) const
{
    return strm;
}
