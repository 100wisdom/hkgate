/*
 * UserAppDialog.cpp
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#include "UserAppDialog.h"
#include "CodecName.h"
#include "RtpUrl.h"


UserAppDialog::UserAppDialog(resip::DialogUsageManager& dum, const resip::SipMessage& msg):
	resip::AppDialog(dum),
	m_audioReady(),
	m_videoReady(),
	m_canTalk()
{
}

UserAppDialog::~UserAppDialog()
{
}

void UserAppDialog::setupAudio(av::MediaChannelPtr& mediaChannel, const std::string& localIP, int localPort)
{
	m_audioChannel->setMediaType(av::MEDIA_TYPE_AUDIO);

	int codec = av::CodecName::parse(m_audioMedium.codec.c_str());
	m_audioChannel->setPeerMedium(m_audioMedium.payload, codec, m_audioMedium.clockRate, m_audioMedium.fmtp);

	m_audioChannel->setPayload(m_audioMedium.payload);

	std::string url = av::RtpUrl::makeUrl(localIP, localPort);
	m_audioChannel->open(url, std::string());

	mediaChannel->addSink(m_audioChannel);
	m_audioReady = true;

	m_audioChannel->addSink(mediaChannel);
}

void UserAppDialog::setupVideo(av::MediaChannelPtr& mediaChannel, const std::string& localIP, int localPort)
{
	m_videoChannel->setMediaType(av::MEDIA_TYPE_VIDEO);

	m_videoChannel->setPeerMedium(m_videoMedium.payload, av::MEDIA_CODEC_H264, m_videoMedium.clockRate, m_videoMedium.fmtp);

	m_videoChannel->setPayload(m_videoMedium.payload);

	std::string url = av::RtpUrl::makeUrl(localIP, localPort);
	m_videoChannel->open(url, std::string());

	mediaChannel->addSink(m_videoChannel);
	m_videoReady = true;
}

bool UserAppDialog::isMediaReady()
{
	return m_audioReady;
}

void UserAppDialog::startCast()
{
	if (m_audioChannel)
	{
		m_audioChannel->setPeerAddr(m_audioMedium.ip, m_audioMedium.port);
	}

	if (m_videoChannel)
	{
		m_videoChannel->setPeerAddr(m_videoMedium.ip, m_videoMedium.port);
	}
}

void UserAppDialog::stopCast()
{
	if (m_audioChannel)
	{
		m_audioChannel->setPeerAddr(std::string(), 0);
	}

	if (m_videoChannel)
	{
		m_videoChannel->setPeerAddr(std::string(), 0);
	}

}

bool UserAppDialog::canTalk()
{
	return m_canTalk;
}

void UserAppDialog::canTalk(bool enabled)
{
	m_canTalk = enabled;
}