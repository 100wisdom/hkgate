/*
 * MediaFormatReadySink.h
 *
 *  Created on: 2015年10月27日
 *      Author: terry
 */

#ifndef MEDIAFORMATREADYSINK_H_
#define MEDIAFORMATREADYSINK_H_

#include "MediaStream.h"
#include <resip/dum/DialogUsageManager.hxx>

/**
 * 媒体格式就绪接收器
 * 仅仅处理 onMediaFormat
 */
class MediaFormatReadySink : public av::MediaSink
{
public:
    MediaFormatReadySink(resip::DialogUsageManager& dum, const resip::Data& devID);
	virtual ~MediaFormatReadySink();

    virtual void onMediaFormat(const av::MediaFormat& fmt);

    virtual void onMediaPacket(av::MediaPacketPtr& pkt);

    virtual void onMediaEvent(int event);

protected:
    resip::DialogUsageManager& m_dum;
    resip::Data	m_devID;

};


#endif /* MEDIAFORMATREADYSINK_H_ */
