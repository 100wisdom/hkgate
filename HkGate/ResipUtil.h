/*
 * ResipUtil.h
 *
 *  Created on: 2015年6月4日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef RESIPUTIL_H_
#define RESIPUTIL_H_

#include "BasicType.h"
#include "Resip.h"
#include "resip/stack/SdpContents.hxx"
#include <string>
#include <sstream>

/**
 * resip 工具类
 */
class ResipUtil
{
public:
    ResipUtil();
    virtual ~ResipUtil();

    /**
     * 从字符串构成Mime对象
     * @param mime
     * @return
     */
    static resip::Mime makeMime(const std::string& mime);

    /**
     * 设置消息的mime
     * @param msg
     * @param mime
     */
    static void setMime(resip::SipMessage& msg, const std::string& mime);

    /**
     * 从消息中获取mime
     * @param msg
     * @return
     */
    static std::string getMime(const resip::SipMessage& msg);

    template <class T>
    static std::string toString(const T& t)
    {
        std::ostringstream oss;
        t.encode(oss);
        return oss.str();
    }


};

#endif /* RESIPUTIL_H_ */
