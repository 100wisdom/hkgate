/*
 * HkMediaSystem.h
 *
 *  Created on: 2017年8月20日
 *      Author: terry
 */

#ifndef HKMEDIASYSTEM_H_
#define HKMEDIASYSTEM_H_

#include "MediaSystem.h"
#include "HkMediaChannel.h"
#include "HkDevice.h"
#include "TMap.h"


namespace av
{


class HkMediaSystem : public MediaSystem
{
public:
	HkMediaSystem();
	virtual ~HkMediaSystem();

	void setAddress(const std::string& ip, int port);

	void setAccount(const std::string& username, const std::string& password);

	virtual bool open();

	virtual void close();

	virtual bool isOpen();

	/**
	* 判断设备是否存在
	* @param id
	* @return
	*/
	virtual bool exist(const std::string& id);

	/**
	* 查找设备
	* @param id
	* @param device
	* @return true 表示成功
	*/
	virtual bool getDevice(const std::string& id, Device& device);

	/**
	* 枚举设备
	* @param devices	设备数组
	* @param maxSize	数组最大长度
	* @return 数组长度
	*/
	virtual size_t listDevice(Device* devices, size_t maxSize);

	/**
	* 创建设备
	* @param id
	* @return
	*/
	virtual MediaChannelPtr create(const std::string& id);

	/**
	* 设置回调
	* @param sink
	*/
	virtual void setSink(MediaSystemSink* sink);


public:
	void onOrgInfo(long getResHandle, int *iContinue, int iFinish, const char *FileListXML);


protected:
	typedef comn::Map< std::string, HkDevice >	DeviceMap;

protected:
	std::string	m_ip;
	int	m_port;
	std::string	m_username;
	std::string	m_password;

	long	m_loginId;
	MediaSystemSink*	m_sink;

	DeviceMap	m_deviceMap;

};

}

#endif /* HKMEDIASYSTEM_H_ */
