/*    file: G722Transcoder.h
 *    desc:
 *   
 * created: 2017-05-05
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined G722TRANSCODER_H_
#define G722TRANSCODER_H_

#include "AudioTranscoder.h"
#include "AVFramePtr.h"
#include "FfmpegUtil.h"
#include "MediaType.h"
#include "HKG722.h"
#include "ByteBuffer.h"
#include "speex/speex_resampler.h"

namespace av
{


class G722Transcoder : public AudioTranscoder
{
public:
	G722Transcoder();
	~G722Transcoder();

	virtual bool open(const AudioFormat& srcFormat, const AudioFormat& outFormat);

	virtual void close();

	virtual bool isOpen();

	virtual bool transcode(AVPacket* inPkt, AVPacket* outPkt);

	virtual AudioFormat getOutFormat();

protected:
	int openDecoder();
	void closeDecoder();

	int resample(AVFrame* inFrame, AVFrame* outFrame);

protected:
	AudioFormat	m_srcFormat;
	AudioFormat	m_outFormat;

	AVCodecContext*	m_decContext;
	SwrContext*		m_swrContext;

	HKG722Encoder	m_encoder;
	ByteBuffer		m_outBuffer;

	SpeexResamplerState*	m_resample;

};


}
#endif //G722TRANSCODER_H_

