/*
 * AppConst.h
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */

#ifndef APPCONST_H_
#define APPCONST_H_


#include "BasicType.h"
#include <string>


class AppConst
{
public:
	AppConst();
	virtual ~AppConst();

	static const char* LOG_FILE;

	static const char* SIP_LOG_FILE;

	static const int	MAX_DURATION = 60 * 8;	/// minutes

	static const int 	DEFAULT_RESERVED_SPACE = 2000;

	static bool discard;

	static bool TALK_RECV_ENABLED;

};

#endif /* APPCONST_H_ */
