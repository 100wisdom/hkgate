/*
 * RegUserStore.h
 *
 *  Created on: 2015年7月16日
 *      Author: terry
 */

#ifndef REGUSERSTORE_H_
#define REGUSERSTORE_H_

#include "BasicType.h"
#include <string>


/**
 * 注册用户
 */
struct RegUser
{
	std::string	name;	/// id
	std::string	alias;	/// 别名
	std::string password;	/// 密码
	std::string passwordSha;	/// 密码sha值
	std::string type;		/// 用户类型
};


/**
 * 注册用户表接口
 */
class RegUserStore
{
public:
	virtual ~RegUserStore() {}

	/**
	 * 根据用户ID查找注册用户
	 * @param name
	 * @param user
	 * @return
	 */
	virtual bool find(const std::string& name, RegUser& user) =0;

};



#endif /* REGUSERSTORE_H_ */
