/*
 * CRegUserStore.h
 *
 *  Created on: 2017年4月6日
 *      Author: chuanjiang.zh
 */

#ifndef CREGUSERSTORE_H_
#define CREGUSERSTORE_H_

#include "RegUserStore.h"
#include <map>

class CRegUserStore: public RegUserStore
{
public:
	CRegUserStore();
	virtual ~CRegUserStore();

	virtual bool find(const std::string& name, RegUser& user);

	/**
	 * 添加注册用户
	 * @param user
	 */
	void add(const RegUser& user);

	/**
	 * 删除注册用户
	 * @param name
	 */
	void remove(const std::string& name);

protected:
	virtual bool findInStore(const std::string& name, RegUser& user);

protected:
	typedef std::map< std::string, RegUser >	RegUserMap;

	bool findInMap(const std::string& name, RegUser& user);



protected:
	RegUserMap	m_map;

};

#endif /* CREGUSERSTORE_H_ */
