/*
 * TestG722.h
 *
 *  Created on: 2017年8月20日
 *      Author: terry
 */

#ifndef TESTG722_H_
#define TESTG722_H_

#include "HKG722.h"
#include "G722Transcoder.h"
#include "G722ToG711Encoder.h"
#include "MediaPacket.h"


class TestG722
{
public:
	TestG722();
	virtual ~TestG722();

	void testDecodeG722();

	void test();

protected:
	void onData(uint8_t* data, int size);

	void onPcmData(av::MediaPacketPtr& pkt);

protected:
	av::G722Transcoder		m_g722Encoder;
	av::G722ToG711Encoder	m_g722Decoder;

};

#endif /* TESTG722_H_ */
