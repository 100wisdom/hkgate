/*
 * RegUserAuthManager.h
 *
 *  Created on: 2015年7月16日
 *      Author: terry
 */

#ifndef REGUSERAUTHMANAGER_H_
#define REGUSERAUTHMANAGER_H_

#include "Resip.h"
#include <resip/dum/ServerAuthManager.hxx>
#include <resip/dum/UserAuthInfo.hxx>
#include "RegUserStore.h"

using namespace resip;

/**
 * resip 服务认证接口实现类
 */
class RegUserAuthManager: public resip::ServerAuthManager
{
public:
	RegUserAuthManager(DialogUsageManager& dum, RegUserStore& userStore,
            const resip::Data& staticRealm = resip::Data::Empty);
	virtual ~RegUserAuthManager();

	virtual void requestCredential(const Data& user,
	                                   const Data& realm,
	                                   const SipMessage& msg,
	                                   const Auth& auth,
	                                   const Data& transactionToken);
    virtual bool proxyAuthenticationMode() const;

    virtual bool authorizedForThisIdentity(const resip::Data &user, 
        const resip::Data &realm, 
        resip::Uri &fromUri);

    virtual Result handle(SipMessage* sipMsg);

protected:
	RegUserStore&	m_userStore;

};

#endif /* REGUSERAUTHMANAGER_H_ */
