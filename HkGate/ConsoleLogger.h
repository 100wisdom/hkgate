/*
 * ConsoleLogger.h
 *
 *  Created on: 2015年5月14日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef VGS_CONSOLELOGGER_H_
#define VGS_CONSOLELOGGER_H_

#include "rutil/Log.hxx"

#include <stdio.h>
#include <stdlib.h>

using namespace resip;


class ConsoleLogger : public resip::ExternalLogger
{
public:
    virtual bool operator()(Log::Level level,
       const Subsystem& subsystem,
       const Data& appName,
       const char* file,
       int line,
       const Data& message,
       const Data& messageWithHeaders)
    {


#ifdef WIN32XXX
        Data msg(message);
        msg += "\n";
        OutputDebugStringA(msg.c_str());
#else
        printf("%s\n", message.c_str());

#endif //WIN32

        return true;
    }

};

#endif /* VGS_CONSOLELOGGER_H_ */
