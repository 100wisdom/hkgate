/*
 * CAudioTranscoder.h
 *
 *  Created on: 2017年4月28日
 *      Author: chuanjiang.zh
 */

#ifndef CAUDIOTRANSCODER_H_
#define CAUDIOTRANSCODER_H_

#include "AudioTranscoder.h"
#include "AVFramePtr.h"
#include "FfmpegUtil.h"

namespace av
{

class CAudioTranscoder: public AudioTranscoder
{
public:
	CAudioTranscoder();
	virtual ~CAudioTranscoder();

	virtual bool open(const AudioFormat& srcFormat, const AudioFormat& outFormat);

	virtual void close();

	virtual bool isOpen();

	virtual bool transcode(AVPacket* inPkt, AVPacket* outPkt);

	virtual AudioFormat getOutFormat();

	virtual AVCodecContext* getCodecContext();

protected:
	int openEncoder(AudioFormat& outFormat);
	void closeEncoder();
	bool isEncoderOpen();

	bool isSameFormat(int fmt, int channels, int sampleRate);

	bool getAudioConfig(std::string& config);

	void convertAndStore(AVFramePtr& frame);

	int resample(AVFrame* inFrame, AVFrame* outFrame);

	void storeFrame(AVFramePtr& frame);
	bool loadFrame(AVFrame* frame, int nb_samples);
	void closeFifo();
	int getFifoSize();

	int doEncode(AVPacket* pkt);

	int openDecoder();
	void closeDecoder();

protected:
	AudioFormat	m_srcFormat;
	AudioFormat	m_outFormat;

	AVCodecContext*	m_decContext;
	AVCodecContext* m_context;

	SwrContext*		m_swrContext;

	AVAudioFifo*	m_fifo;
	int64_t         m_sampleCount;


};



} /* namespace av */

#endif /* CAUDIOTRANSCODER_H_ */
