/*
 * UserAppDialogSetFactory.cpp
 *
 *  Created on: 2016年9月21日
 *      Author: zhengboyuan
 */

#include "UserAppDialogSetFactory.h"

#include <resip/dum/DialogSet.hxx>
#include <resip/dum/AppDialogSet.hxx>

#include "UserAppDialog.h"


/**
 * 用户呼叫对话集
 */
class UserAppDialogSet: public resip::AppDialogSet
{
public:
    UserAppDialogSet(resip::DialogUsageManager& dum, const resip::SipMessage& msg):
    	AppDialogSet(dum)
	{

	}

	virtual ~UserAppDialogSet()
	{

	}

    virtual resip::AppDialog* createAppDialog(const resip::SipMessage& msg)
    {
    	UserAppDialog* dlg = new UserAppDialog(mDum, msg);
    	return dlg;
    }

};


UserAppDialogSetFactory::UserAppDialogSetFactory()
{
}

UserAppDialogSetFactory::~UserAppDialogSetFactory()
{
}

resip::AppDialogSet* UserAppDialogSetFactory::createAppDialogSet(resip::DialogUsageManager& dum,
            const resip::SipMessage& msg)
{
    return new UserAppDialogSet(dum, msg);
}
