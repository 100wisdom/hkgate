/*
 * RtpMedium.cpp
 *
 *  Created on: 2016年9月22日
 *      Author: zhengboyuan
 */


#include "RtpMedium.h"


const char*	RtpMedium::VIDEO = "video";
const char*	RtpMedium::AUDIO = "audio";

const char*	RtpMedium::H264 = "H264";
const char*	RtpMedium::PCMA = "PCMA";
const char*	RtpMedium::PCMU = "PCMU";



